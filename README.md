# Apertis Website

This is the source for the main Apertis website. It is generated using
[Hugo](https://gohugo.io/) with a modified
[Beautiful Hugo theme](https://github.com/halogenica/beautifulhugo), changing
the look, implementing search and allowing for the generation of PDFs. The page
is served from GitLab pages.

---

## Document layout

When adding documents to the Apertis Website, please adhere to the following
layout guidelines:

- Policies:
  - Documenting procedures and rules
  - Minimal requirements for project involvement
- Concepts:
  - Topics that have been researched and/or planned but which haven't yet been
    implemented
- Architecture:
  - Description of project infrastructure
  - Details of technologies and software used by Apertis
- Guides:
  - Guidance on using specific software features
  - Worked examples of expected project workflows
- QA:
  - Test reports
  - Test procedures (realistically, a description of the testing performed and
    a pointer to qa.apertis.org)
- Releases:
  - Release notes
  - Release schedules

## Spelling

In order to provide some consistency and quality to the website, we would like
to ensure that all documents have been spellchecked. In the mid-term we would
like checks to be performed as part of the websites CI/CD, using Aspell to
check the spellings. As it is likely that many of the documents use words not
in Aspell's dictionaries, we are starting with a manual approach to start
building up a personal dictionary of additional words that the Apertis project
is happy with.

When making changes or adding documents, please run:

    aspell --personal="./dictionary.aspell" --lang="en_us" --mode=markdown check <document>

Any issues caught should be rectified or added to the `dictionary.aspell` file
(maintaining the alphabetical order being used).

## Use of Hugo `ref` Shortcode

Hugo provides the `ref` shortcode to aid with
[creating links between documents](https://gohugo.io/content-management/cross-references/).
This should be used for all references internal to the website. The generation
process will fail when the target page of the reference can not be found,
causing the CI to fail and thus aids with reducing the risk broken links being
provided on the website.

## Generating PDFs

In order to generate PDFs, we are getting Hugo to create a simplified HTML
pages. So as not to have every page generating a PDF, to get a page to be
generated as a PDF (and HTML at the same time) add the following to the front
matter of the page:

```
outputs = ["html", "pdf-in"]
```

This will result in the simplified HTML being produced in a file called
`index.pdf-in` in the relevant directory. The CI is configured to look for
these files once Hugo has generated the site and create PDFs of them. For the
page `www.apertis.org/concepts/foo/`, a PDF will be available as
`www.apertis.org/concepts/foo/foo.pdf`.

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml).

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install][https://gohugo.io/overview/installing/] Hugo
1. Preview your project: `hugo server`
1. Add content
1. Generate the website: `hugo` (optional)

Read more at Hugo's [documentation][https://gohugo.io/overview/introduction/].

### Preview your site

If you clone or download this project to your local computer and run `hugo
server`, your site can be accessed under `localhost:1313/hugo/`.

