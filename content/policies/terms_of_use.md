+++
date = "2021-04-05"
weight = 100

title = "Terms of Use"

aliases = [
    "/old-wiki/Apertis:Copyrights",
    "/old-wiki/Apertis:General_disclaimer",
    "/policies/general_disclaimer",
    "/policies/copyrights",
]
+++

{{% notice info %}}
This page contains the new Terms of Use that will become effective as of
**6 May 2021**. Please see the
[Apertis Privacy Policy & Terms of Use Update page]({{< ref "conditions_change.md" >}})
for more information.
{{% /notice %}}

**PLEASE READ THESE TERMS AND CONDITIONS CAREFULLY BEFORE USING THIS SITE**

# WHAT'S IN THESE TERMS?

These terms tell you the rules for using our website "apertis.org".  Click on
the links below to go straight to more information on each area:

- [WHO WE ARE AND HOW TO CONTACT US]({{% ref "#who-we-are-and-how-to-contact-us" %}})
- [BY USING OUR SITE YOU ACCEPT THESE TERMS]({{% ref "#by-using-our-site-you-accept-these-terms" %}})
- [THERE ARE OTHER TERMS THAT MAY APPLY TO YOU]({{% ref "#there-are-other-terms-that-may-apply-to-you" %}})
- [WE MAY MAKE CHANGES TO THESE TERMS]({{% ref "#we-may-make-changes-to-these-terms" %}})
- [WE MAY MAKE CHANGES TO OUR SITE]({{% ref "#we-may-make-changes-to-our-site" %}})
- [WE MAY SUSPEND OR WITHDRAW OUR SITE]({{% ref "#we-may-suspend-or-withdraw-our-site" %}})
- [WE MAY TRANSFER THIS AGREEMENT TO SOMEONE ELSE]({{% ref "#we-may-transfer-this-agreement-to-someone-else" %}})
- [YOU MUST KEEP YOUR ACCOUNT DETAILS SAFE]({{% ref "#you-must-keep-your-account-details-safe" %}})
- [HOW YOU MAY USE MATERIAL ON OUR SITE]({{% ref "#how-you-may-use-material-on-our-site" %}})
- [INTERACTIVE SERVICES]({{% ref "#interactive-services" %}})
- [PROHIBITED USES]({{% ref "#prohibited-uses" %}})
- [DO NOT RELY ON INFORMATION ON THIS SITE]({{% ref "#do-not-rely-on-information-on-this-site" %}})
- [WE ARE NOT RESPONSIBLE FOR WEBSITES WE LINK TO]({{% ref "#we-are-not-responsible-for-websites-we-link-to" %}})
- [USER-GENERATED CONTENT IS NOT APPROVED BY US]({{% ref "#user-generated-content-is-not-approved-by-us" %}})
- [HOW TO COMPLAIN ABOUT CONTENT UPLOADED BY OTHER USERS]({{% ref "#how-to-complain-about-content-uploaded-by-other-users" %}})
- [OUR RESPONSIBILITY FOR LOSS OR DAMAGE SUFFERED BY YOU]({{% ref "#our-responsibility-for-loss-or-damage-suffered-by-you" %}})
- [HOW WE MAY USE YOUR PERSONAL INFORMATION]({{% ref "#how-we-may-use-your-personal-information" %}})
- [UPLOADING CONTENT TO OUR SITE]({{% ref "#uploading-content-to-our-site" %}})
- [CONTENT STANDARDS]({{% ref "#content-standards" %}})
- [WE ARE NOT RESPONSIBLE FOR VIRUSES AND YOU MUST NOT INTRODUCE THEM]({{% ref "#we-are-not-responsible-for-viruses-and-you-must-not-introduce-them" %}})
- [RULES ABOUT LINKING TO OUR SITE]({{% ref "#rules-about-linking-to-our-site" %}})
- [WHICH COUNTRY'S LAWS APPLY TO ANY DISPUTES?]({{% ref "#which-countrys-laws-apply-to-any-disputes" %}})
- [TRADEMARKS]({{% ref "#trademarks" %}})
- [OUR TRADEMARKS ARE REGISTERED]({{% ref "#our-trademarks-are-registered" %}})


# WHO WE ARE AND HOW TO CONTACT US

"apertis.org" is a site operated by Collabora Limited ("We"). We are registered
in England and Wales as a limited company with registered number 05513718 and
our registered office and main  trading address is Platinum Building, Cowley
Road, Cambridge, England, CB4 0DS. Our VAT number is GB874163019.

To contact us, please email [contact@apertis.org](mailto: contact@apertis.org)
or phoning our customer service line on +44 1223 362967.

# BY USING OUR SITE YOU ACCEPT THESE TERMS

By using our site, you confirm that you accept these terms of use and that you
agree to comply with them. If you do not agree to these terms, you must not use
our site. We recommend that you print a copy of these terms for future
reference.

# THERE ARE OTHER TERMS THAT MAY APPLY TO YOU

These terms of use refer to the following additional terms, which also apply to
your use of our site:

- Our [Privacy Policy]({{% ref "new_privacy_policy.md" %}}), which also sets
  out information about the cookies on our site.

# WE MAY MAKE CHANGES TO THESE TERMS

We amend these terms from time to time. Every time you wish to use our site,
please check these terms to ensure you understand the terms that apply at that
time. These terms were most recently updated on 5 April 2021.

# WE MAY MAKE CHANGES TO OUR SITE

We may update and change our site from time to time to reflect changes to the
focus of the project. We will try to give you reasonable notice of any major
changes.

# WE MAY SUSPEND OR WITHDRAW OUR SITE

Our site is made available free of charge.

We do not guarantee that our site, or any content on it, will always be
available or be uninterrupted. We may suspend or withdraw or restrict the
availability of all or any part of our site for business and operational
reasons. We will try to give you reasonable notice of any suspension or
withdrawal.

You are also responsible for ensuring that all persons who access our site
through your internet connection are aware of these terms of use and other
applicable terms and conditions, and that they comply with them.

When we consider that a breach of the terms of use has occurred, we may take
such action as we deem appropriate.

Failure to comply with these terms of use upon which you are permitted to use
our site may result in our taking all or any of the following actions:

- Immediate, temporary or permanent withdrawal of your right to use our site.
- Immediate, temporary or permanent removal of any Contribution uploaded by you
  to our site.
- Issue of a warning to you.
- Legal proceedings against you for reimbursement of all costs on an indemnity
  basis (including, but not limited to, reasonable administrative and legal
  costs) resulting from the breach.
- Further legal action against you.
- Disclosure of such information to law enforcement authorities as we
  reasonably feel is necessary or as required by law.

We exclude our liability for all action we may take in response to breaches of
these terms of use. The actions we may take are not limited to those described
above, and we may take any other action we reasonably deem appropriate.

# WE MAY TRANSFER THIS AGREEMENT TO SOMEONE ELSE

We can transfer our rights and obligations under these terms to any third
party, provided this does not adversely affect your rights under these terms.

# YOU MUST KEEP YOUR ACCOUNT DETAILS SAFE

If you choose, or you are provided with, a user identification code, password
or any other piece of information as part of our security procedures, you must
treat such information as confidential. You must not disclose it to any third
party.

We have the right to disable any user identification code or password, whether
chosen by you or allocated by us, at any time, if in our reasonable opinion you
have failed to comply with any of the provisions of these terms of use.

If you know or suspect that anyone other than you knows your user
identification code or password, you must promptly notify us at
[contact@apertis.org](mailto:contact@apertis.org).

# HOW YOU MAY USE MATERIAL ON OUR SITE

We are the owner or the licensee of all intellectual property rights in our
site, and in the material published on it.  Those works are protected by
copyright laws and treaties around the world.

The different elements of the content provided on "apertis.org" are licensed
under different licensing terms:

- Apertis website (www.apertis.org): Unless explicitly stated otherwise, the
  text of the Apertis website is copyrighted by the Apertis contributors and is
formally licensed under Creative Commons Attribution-ShareAlike 4.0
International (CC BY-SA 4.0) license. For more information, please see:
https://www.apertis.org/policies/copyrights/

- Apertis GitLab instance (gitlab.apertis.org): The GitLab site is copyrighted
  by GitLab B.V. and licensed as laid out in the GitLab LICENSE file:
https://gitlab.com/gitlab-org/gitlab/-/blob/master/LICENSE

- Apertis Code: The Apertis GitLab instance contains many Git repositories
  housing source code and other documents. Code created for Apertis is
primarily licensed under the Mozilla Public License Version 2.0. Apertis also
makes use of other projects which may have other licenses, such as the GPL and
LGPL. For example, this includes projects such as the Linux kernel, WebKit and
GLib. You can find more information about the licensing for each component in
the COPYING or LICENSE files in each components Git repository. For more
information regarding the licensing of Apertis code see:

	https://www.apertis.org/policies/license-applying/

# INTERACTIVE SERVICES

We may from time to time provide interactive services on our site, including,
without limitation:

- Comments in merge requests.
- Comments in issues.

We are under no obligation to oversee, monitor or moderate any interactive
service we provide on our site, and we expressly exclude our liability for any
loss or damage arising from the use of any interactive service by a user in
contravention of our content standards, whether the service is moderated or
not.

The use of any of our interactive services by a minor is subject to the consent
of their parent or guardian. We advise parents to supervise their children
whilst using any interactive services due to the lack of moderation.

# PROHIBITED USES

You may use our site only for lawful purposes.  You may not use our site:

- In any way that breaches any applicable local, national or international law
  or regulation.
- In any way that is unlawful or fraudulent or has any unlawful or fraudulent
  purpose or effect.
- For the purpose of harming or attempting to harm minors in any way.
- To bully, insult, intimidate or humiliate any person.
- To send, knowingly receive, upload, download, use or re-use any material
  which does not comply with our
  [content standards]({{% ref "#content-standards" %}}).
- To transmit, or procure the sending of, any unsolicited or unauthorised
  advertising or promotional material or any other form of similar solicitation
  (spam).
- To knowingly transmit any data, send or upload any material that contains
  viruses, Trojan horses, worms, time-bombs, keystroke loggers, spyware, adware
  or any other harmful programs or similar computer code designed to adversely
  affect the operation of any computer software or hardware.

You also agree:

- Not to reproduce, duplicate, copy or re-sell any part of our site in
  contravention of the provisions in these terms of use and the relevant
  materials licensing.
- Not to access without authority, interfere with, damage or disrupt:
   - any part of our site;
   - any equipment or network on which our site is stored;
   - any software used in the provision of our site; or
   - any equipment or network or software owned or used by any third party.

# DO NOT RELY ON INFORMATION ON THIS SITE

The content on our site is provided for general information only. It is not
intended to amount to advice on which you should rely. You must obtain
professional or specialist advice before taking, or refraining from, any action
on the basis of the content on our site.

Although we make reasonable efforts to update the information on our site, we
make no representations, warranties or guarantees, whether express or implied,
that the content on our site is accurate, complete or up to date.

# WE ARE NOT RESPONSIBLE FOR WEBSITES WE LINK TO

Where our site contains links to other sites and resources provided by third
parties, these links are provided for your information only. Such links should
not be interpreted as approval by us of those linked websites or information
you may obtain from them.

We have no control over the contents of those sites or resources.

# USER-GENERATED CONTENT IS NOT APPROVED BY US

This website may include information and materials uploaded by other users of the site, including in merge requests and issues. This information and these materials have not been verified or approved by us. The views expressed by other users on our site do not represent our views or values.

# HOW TO COMPLAIN ABOUT CONTENT UPLOADED BY OTHER USERS

If you wish to complain about content uploaded by other users, please contact
us at [contact@apertis.org](mailto:contact@apertis.org).

# OUR RESPONSIBILITY FOR LOSS OR DAMAGE SUFFERED BY YOU

- We do not exclude or limit in any way our liability to you where it would be unlawful to do so. This includes liability for death or personal injury caused by our negligence or the negligence of our employees, agents or subcontractors and for fraud or fraudulent misrepresentation.
- We exclude all implied conditions, warranties, representations or other terms that may apply to our site or any content on it.
- We will not be liable to you for any loss or damage, whether in contract, tort (including negligence), breach of statutory duty, or otherwise, even if foreseeable, arising under or in connection with:
   - use of, or inability to use, our site; or
   - use of or reliance on any content displayed on our site.
- In particular, we will not be liable for:
   - loss of profits, sales, business, or revenue;
   - business interruption;
   - loss of anticipated savings;
   - loss of business opportunity, goodwill or reputation; or
   - any indirect or consequential loss or damage.

# HOW WE MAY USE YOUR PERSONAL INFORMATION

We will only use your personal information as set out in our
[Privacy Policy]({{% ref "new_privacy_policy.md" %}}).

# UPLOADING CONTENT TO OUR SITE

Whenever you make use of a feature that allows you to upload content to our
site, or to make contact with other users of our site, you must comply with the
content standards set out in these terms of use.

You warrant that any such contribution does comply with those standards, and
you will be liable to us and indemnify us for any breach of that warranty. This
means you will be responsible for any loss or damage we suffer as a result of
your breach of warranty.

Any content you upload to our site will be considered non-confidential and
non-proprietary. You retain all of your ownership rights in your content, but
by uploading the content you agree to that content if uploaded to a Git
repository being licensed as spelled out in that Git repositories COPYING or
LICENSE file. Content added as issues and merge requests in GitLab or any other
way that is not submitting content to a Git repository are to be licensed under
Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA) license.

We also have the right to disclose your identity to any third party who is
claiming that any content posted or uploaded by you to our site constitutes a
violation of their intellectual property rights, or of their right to privacy.

We have the right to remove any content you upload to our site if, in our
opinion, the content does not comply with the content standards set out here.

You are solely responsible for securing and backing up your content.

# CONTENT STANDARDS

These content standards apply to any and all material which you contribute to our site ("Contribution"), and to any interactive services associated with it.

The Content Standards must be complied with in spirit as well as to the letter. The standards apply to each part of any Contribution as well as to its whole.

Apertis.org will determine, in its discretion, whether a Contribution breaches the Content Standards.

A Contribution must:

- Be accurate (where it states facts).
- Be genuinely held (where it states opinions).
- Comply with the law applicable in England and Wales and in any country from which it is posted.

A Contribution must not:

- Be defamatory of any person.
- Be obscene, offensive, hateful or inflammatory.
- Bully, insult, intimidate or humiliate.
- Promote sexually explicit material.
- Include child sexual abuse material.
- Promote violence.
- Promote discrimination based on race, sex, religion, nationality, disability,
  sexual orientation or age.
- Infringe any copyright, database right or trade mark of any other person.
- Be likely to deceive any person.
- Breach any legal duty owed to a third party, such as a contractual duty or a
  duty of confidence.
- Promote any illegal content or activity.
- Be in contempt of court.
- Be threatening, abuse or invade another's privacy, or cause annoyance,
  inconvenience or needless anxiety.
- Be likely to harass, upset, embarrass, alarm or annoy any other person.
- Impersonate any person or misrepresent your identity or affiliation with any
  person.
- Give the impression that the Contribution emanates from a company, if this is
  not the case.
- Advocate, promote, incite any party to commit, or assist any unlawful or
  criminal act such as (by way of example only) copyright infringement or
  computer misuse.
- Contain a statement which you know or believe, or have reasonable grounds for
  believing, that members of the public to whom the statement is, or is to be,
  published are likely to understand as a direct or indirect encouragement or
  other inducement to the commission, preparation or instigation of acts of
  terrorism.
- Contain any advertising or promote any services or web links to other sites.

# WE ARE NOT RESPONSIBLE FOR VIRUSES AND YOU MUST NOT INTRODUCE THEM

We do not guarantee that our site will be secure or free from bugs or viruses.

You are responsible for configuring your information technology, computer
programmes and platform to access our site. You should use your own virus
protection software.

You must not misuse our site by knowingly introducing viruses, trojans, worms,
logic bombs or other material that is malicious or technologically harmful. You
must not attempt to gain unauthorised access to our site, the server on which
our site is stored or any server, computer or database connected to our site.
You must not attack our site via a denial-of-service attack or a distributed
denial-of service attack. By breaching this provision, you would commit a
criminal offence under the Computer Misuse Act 1990. We will report any such
breach to the relevant law enforcement authorities and we will co-operate with
those authorities by disclosing your identity to them. In the event of such a
breach, your right to use our site will cease immediately.

# RULES ABOUT LINKING TO OUR SITE

You may link to our home page, provided you do so in a way that is fair and
legal and does not damage our reputation or take advantage of it.

You must not establish a link in such a way as to suggest any form of
association, approval or endorsement on our part where none exists.

We reserve the right to withdraw linking permission without notice.

The website in which you are linking must comply in all respects with the
content standards set out in our [Content Standards]({{% ref "#content-standards" %}}).

If you wish to link to or make any use of content on our site other than that
set out above, please contact
[contact@apertis.org](mailto:contact@apertis.org).

# WHICH COUNTRY'S LAWS APPLY TO ANY DISPUTES?

If you are a consumer, please note that these terms of use, their subject
matter and their formation, are governed by English law. You and we both agree
that the courts of England and Wales will have exclusive jurisdiction except
that if you are a resident of Northern Ireland you may also bring proceedings
in Northern Ireland, and if you are resident of Scotland, you may also bring
proceedings in Scotland.

If you are a business, these terms of use, their subject matter and their
formation (and any non-contractual disputes or claims) are governed by English
law. We both agree to the exclusive jurisdiction of the courts of England and
Wales.

# TRADEMARKS

Any of the trademarks, service marks, collective marks, design rights or
similar rights that are mentioned, used or cited "apertis.org" are the property
of their respective owners. Their use here does not imply that you may use them
for any purpose other than for the same or a similar informational use as
contemplated by the original authors of these documents. Unless otherwise
stated Apertis is neither endorsed by nor affiliated with any of the holders of
any such rights and as such Apertis cannot grant any rights to use any
otherwise protected materials. Your use of any such trademarks, service marks,
collective marks, design rights or similar rights is at your own risk.

# OUR TRADEMARKS ARE REGISTERED

“Apertis” is a registered trademark of Robert Bosch Car Multimedia GmbH.
“Collabora” is a registered trademark of Collabora Ltd. You are not permitted
to use them without our approval, unless they are part of material you are
using as permitted under
[How you may use material on our site]({{% ref "marketing_resources.md" %}}).
