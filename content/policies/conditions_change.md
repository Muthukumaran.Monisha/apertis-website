+++
date = "2021-04-05"
weight = 100

title = "Apertis Privacy Policy & Terms of Use Update"
+++

The Apertis project is opening up!

We are planning to open registration on the Apertis
[GitLab Repository](https://gitlab.apertis.org), as part of this we need to
update our [privacy policy]({{< ref "privacy_policy.md" >}}) and other
conditions.

As a result of these changes, and inline with our policies regarding changes to
the privacy policy, we are highlighting the following changes to the privacy
policy that we intend to implement on **6 May 2021**.

## Highlevel Changes:

- We will be replacing the [privacy policy]({{< ref "privacy_policy.md" >}})
  with a [new privacy policy]({{< ref "new_privacy_policy.md" >}}).
- We will be adding a new set of [Terms of Use]({{< ref "terms_of_use.md" >}})
  that will replace the current
  [general disclaimer]({{< ref "general_disclaimer.md" >}}) and
  [copyrights]({{< ref "copyrights.md" >}}).

## Privacy Policy

The existing [privacy policy]({{< ref "privacy_policy.md" >}}) was written
when Apertis provided a Mediawiki based website. This was removed some time
ago and whilst small changes were made to make it more relevant to the
Apertis services provided today, it does not meet our requirements,
specifically around topics like GDPR compliance. As a result, the document is
being replaced with the
[new privacy policy]({{< ref "new_privacy_policy.md" >}}).

## Terms of use

The current website contains a
[general disclaimer]({{< ref "general_disclaimer.md" >}})  and a
[copyright]({{< ref "copyrights.md" >}}) document. These cover some of the
topics that typically get included in Terms of Use, however the Apertis project
currently does not have a set of Terms of Use that are suitable to cover our
planned opening of registration for GitLab and also suitable for the current
services provided by Apertis. To remedy this, the above two documents will be
removed and replaced by a single set of
[Terms of Use]({{< ref "terms_of_use.md" >}}) to cover all Apertis services.

