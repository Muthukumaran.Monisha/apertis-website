+++
date = "2015-11-05"
lastmod= "2021-04-05"
weight = 100

title = "Privacy Policy"

aliases = [
    "/old-wiki/Apertis:Privacy_policy"
]
+++

{{% notice info %}}
This page contains the new Privacy Policy that will become effective as of
**6 May 2021**. Please see the
[Apertis Privacy Policy & Terms of Use Update page]({{< ref "conditions_change.md" >}})
for more information.
{{% /notice %}}

# Who are we?

"apertis.org" is managed and maintained by Collabora Limited. We are an IT
global consultancy company, specialized in open source software.

Our main office is in The Platinum Building, St John's Innovation Park,
Cambridge, United Kingdom. Our registered company number is 05513718 and our
ICO registration number is ZA229843.

If you have any queries in connection with how we use your data, you can
contact us by emailing [data@collabora.com](mailto:data@collabora.com) or
phoning one of our offices. You can also contact our EU Representative, Olivier
Potin, at [olivier.potin@collabora.com](mailto:olivier.potin@collabora.com).

# What data do we collect about you?

1. We collect and process the following data when you sign up to GitLab, in
order to create your basic profile with GitLab, and provide the core
functionality of our GitLab platform to you:

   - Username
   - First Name
   - Last Name
   - Email Address
   - Date you joined
   - Commit history on the platform
   - Commit history from other other projects

   We handle this data (which we refer to from now on as your **Core Account
Data**) to perform the contract in place between us. Your Core Account Data is
critical to us being able to manage the history of, and develop our open source
software.

   If you update your Core Account Data with us, we do not apply those changes
retrospectively (meaning commits you make to Gitlab under an old email address
will remain posted under that email address).

   You are free to delete your GitLab account at any time, but please keep in
mind that the right to erasure does not apply to your Core Account Data. This
means that your history on our platform (such as your history of working on the
GitLab software) will remain publically visible even after you delete your
account.

2. You can choose to enhance your GitLab profile and experience by:

   - Following others on GitLab, and engaging with others who follow you
   - Adding detail to your profile such as your job title, where you work, and
     an avatar

   We handle this data (which we refer to from now on as your **Additional
Account Data**) because of our legitimate interest to build a community of
developers who work on our platform. If you choose to delete your GitLab
account, which you can do at any time, we will also erase your Additional
Account Data.

3. When you contribute to the GitLab project, in addition to your Core Account
Data and any Additional Account Data you choose to provide, we collect and
process the following data created by you, which we refer to from now on as
your Account Activity Data:

   - Issues with the software
   - Merge requests
   - Notes
   - Abuse reports
   - Award emojis

   We handle this data about you under our legitimate interests to ensure the
service we provide to you is as expected, and that we are able to take steps to
solve any issues/ fix any bugs you highlight to us and respond to any of your
queries.

# What happens when I delete my account?

When you delete your user account:

- We delete your Additional Account Data.
- Your Core Account Data is not deleted, which means any commits made by a
  deleted user will still display your username at the time the commit was
  made.

- Your Account Activity Data is unlinked from your username, and moved to a
  system-wide user called “Ghost User”, which acts as a container for deleted
  records. We do this because we want to respect our users’ privacy and ensure
  the development history of our software remains intact.

# Where is your data stored?

Our servers are located in the UK.

# Transfer of data to third parties:

No data is transferred to 3rd parties for further processing, and we do not use
any non-essential cookies on GitLab. We use the following, essential cookies:

| Cookie Name | Retention Period | Description |
| ----------- | ---------------- | ----------- |
| \_gitlab\_session | End of session | Tracking the user session to provide core functionality such as logging in |
| experimentation\_subject\_id | 20 years | Utilised to perform A/B testing by the gitlab developers on the gitlab.com instance (it is not used on the Apertis instance). |

Under no circumstances, does Collabora sell customer data of any kind.

Please keep in mind that information you actively contribute to our GitLab
project, including personal data, is publicly visible and can be found by
search engines. It is also possible that a third party could decide to create a
fork (a spin off project) that involves copying our GitLab, along with its
development history and publically available data, and creating their own
version of our software. With this in mind, please do not contribute any
information that you are uncomfortable making permanently public, such as
information revealing your real name or location in your contributions.

# Retention Period:

We will only retain your personal information for as long as we need it unless
we are required to keep it for longer to comply with our legal, accounting or
regulatory requirements. We will usually delete your personal data when you
delete your account as described above, or 6 years after the GitLab project
shuts down. Any data we have on any backup systems will usually be deleted
within 6 months.

In some circumstances we may carefully anonymise your personal data so that it
can no longer be associated with you, and we may use this anonymised
information indefinitely without notifying you. We use this anonymised
information to analyse our programmes and support other similar programmes
around the world.

# Your Rights

You have various other rights under applicable data protection laws, including
the right to:

- **access** your personal data (also known as a “subject access request”);
- **correct** incomplete or inaccurate data we hold about you;
- ask us to **erase** the personal data we hold about you;
- ask us to **restrict** our handling of your personal data;
- ask us to **transfer** your personal data to a third party; 
- **object** to how we are using your personal data; and
- **withdraw your consent** to us handling your personal data.

You also have the right to lodge a complaint with your relevant supervisory
authority, you can find which one applies to youi
[here](https://ec.europa.eu/justice/article-29/structure/data-protection-authorities/index_en.htm).

To exercise any of the above rights, please contact us using
[data@collabora.com](mailto:data@collabora.com). Please keep in mind that
privacy law is often complicated, and these rights will not be available to you
all of the time. And in order to process your request, we may need to verify
your identity for your security.  In such cases, we may need you to respond
with proof of your identity.

This privacy policy was last updated on 23 March 2021.
