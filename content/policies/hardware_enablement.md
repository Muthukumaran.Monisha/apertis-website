+++
date = "2019-12-03"
weight = 100

title = "Hardware Enablement"

aliases = [
    "/hardware_interface.md",
    "/old-wiki/Hardware_Interface",
    "/old-wiki/Hardware_Interfaces"
]
+++

This page documents what is required from hardware platform (and
specifically the software interfaces used) to integrate well into
Apertis.

For Apertis to easily support a variety of hardware platforms, it is
important that a consistent set of user space components can be used
across the range of supported devices.

To achieve this (in-line with its [Upstream
First]( {{< ref "contributions.md#upstream-first-policy" >}} )
strategy), Apertis relies on upstream interfaces and frameworks to
access the hardware. Interfaces that aren't supported upstream (or
cannot go upstream for whatever reason) require extra effort to support,
test and maintain. This makes such bespoke interfaces a very bad choice
in both the short and the long-term.

Furthermore, to enable Apertis to meaningfully debug, fix and improve
the hardware enablement if and when needed, the hardware support should
be open source. Unfortunately on most ARM platforms this is not yet
achievable, for example the EGL/GL stack for the 3D GPU on ARM platforms
is typically not available as open source (although this seems to be
improving). However even in that case, the components should be freely
distributable (such that Apertis images can be distributed with them)
and use standard APIs.

The following sections highlight for different types of hardware what
software interface should be provided to enable it. As the list is not
exhaustive with respect to all available types of hardware (e.g. new
types of hardware that can not currently support by Apertis may not be
listed here), please ask on the [Apertis developers
list]( {{< ref "/community.md" >}} ) for guidance when in doubt.

In general the overall guidance is simply if there are well-established
kernel interfaces for a type of device, those should be used without
further question and won't be included in this list (e.g. Ethernet
drivers, USB stack, block drivers etc).

## Bootloader

The very first bit of hardware enablement required when booting a
device, is the bootloader. In Apertis the bootloader doesn't only play
the role of doing the basic hardware setup but is also a fundamental
part of the rollback design as it has to decide when to rollback/boot a
previous version of the system. As such Apertis currently only supports
two bootloaders:

  - U-Boot: Typically used on ARM. Hardware support should be integrated
    in the U-Boot version used by the [version of Apertis being
    used](https://gitlab.apertis.org/pkg/target/u-boot) or newer.

<!-- end list -->

  - systemd-boot: Used for Intel EFI systems. Work for supporting
    rollback with this bootloader is pending. (on the hardware side this
    mostly requires a valid EFI implementation)

## Kernel

Apertis uses a Linux kernel that is as close to upstream as possible and
regularily updates its base kernel version. Therefore it's recommended
that any hardware enablement done for Apertis is also upstream, to both
minimize the delta with upstream and make it easier to update to newer
versions. The specific version of the Linux kernel currently being
targetted by the Apertis release being used can be determined for the
[Apertis kernel](https://gitlab.apertis.org/pkg/target/linux) git
repository.

### Specific Kernel Areas

The following guidance is provided for specific functionality that may
need to be provided by the kernel.

#### Graphics

The graphical subsystem on modern systems tends to be divided in various
specific IP blocks. Typically there is a separate display subsystem
(controller for the screen, overlays etc) and at least a separate 3D
GPU. All these blocks should be exposed by the kernel to user space via
the DRM and KMS interfaces.

Furthermore it is recommended that the Display subsystem support Atomic
modesetting.

#### Directly Attached Cameras

Cameras directly attached to the system (e.g. via a MIPI-CSI interface)
should be exposed via the standard Video for Linux 2 APIs (V4L2). To
ensure efficient sharing of the captured frames with other hardware
components (e.g. for displaying), the driver should support exporting
the frame via dma buffers
[1](https://www.kernel.org/doc/html/latest/media/uapi/v4l/vidioc-expbuf.html).

#### Bluetooth

Apertis uses the standard Linux bluetooth stack
([BlueZ](http://www.bluez.org/)). Any bluetooth device should support
this stack.

#### Wireless LAN

Apertis uses the standard Linux interface for configuring wireless
devices. On the driver side that means it should use the kernels
cfg80211 framework (either directly or indirectly via mac80211 depending
on whether it's a full mac or a soft mac device).

#### Audio

Apertis uses
[PulseAudio](https://www.freedesktop.org/wiki/Software/PulseAudio/) on
top of the standard ALSA framework. This should be supported by audio
input and output devices. As Audio routing on a SOC is often quite
complicated, it is recommended that a UCM policy is provided if the
standard PulseAudio paths aren't enough (this is typically not needed
for Intel HDA or very simple codecs).

#### Input device

Apertis uses the standard Linux input event subsystem (evdev). For
multitouch screens protocol type B should be used (which allows
fingertracking[2](https://www.kernel.org/doc/Documentation/input/multi-touch-protocol.txt)).

#### Hardware Video Decoding

Video decoding support in hardware exists in a few different forms, but
at the two extremes there are:

  - Fix function decoders: These devices are specifically and only for
    decoding video, their input is a full or slightly pre-processed
    video frame.
  - Dynamic decoders: These device are typically part of the GPU and are
    mostly (like a GPU) programmed by user space for the decoding. They
    may either accelerate parts of the decoding or do the full decoding.

For the former category the standard Linux Video for Linux 2 (V4L2)
decoder interface should be exposed
[3](https://www.kernel.org/doc/html/latest/media/uapi/v4l/dev-mem2mem.html) which is
directly supported by GStreamer.

The latter category typically needs more support on the user space side,
in which case a GStreamer plugin should be provided. E.g. GStreamer
VAAPI as available for Intel systems.

Note that it's typical for ARM platforms to implement the OpenMAX
specification for video playback (as used by Android). However while
there exists GStreamer plugins for using OpenMAX IL, the integration is
generally poor as OpenMAX is a bad fit for GStreamer as it is intended
more for application rather then media framework usage (and needs
hardware specific extensions to integrate well).

#### Sensors

Generic sensors should be exposed through the [Linux Industrial I/O
subsystem](https://www.kernel.org/doc/html/latest/driver-api/iio/index.html)
or [Hardware Monitoring
subsystem](https://www.kernel.org/doc/Documentation/hwmon/hwmon-kernel-api.txt)
as appropriate.

## User Space Components

### Multimedia

Apertis uses Gstreamer throughout as it's multimedia framework. As such,
all platform Multimedia components (e.g. audio decoders, video capture
device, etc), when not exposed via common kernel interfaces directly
supported by GStreamer, should be supported in the form of Gstreamer
plugins. These plugins should support at least GStreamer version 1.8.

### Graphics

Apart from the fore mentioned kernel support for graphics there are big
hardware specific parts required to be provided in user space,
specifically an EGL/GLES2 stack for the 3D GPU. The EGL/GLES2 stack
should support the GBM platform (`KHR_platform_gbm`) for usage by
Wayland compositors, the WL EGL platform and extensions for Wayland
clients. Furthermore dmabuf importing (`EXT_image_dma_buf_import`)
should be supported to allow for efficient importing of output from
either the camera subsystems or the hardware video decoder.

### Voice/3G Modems

Any modem hardware should be support via ofono plugins.
