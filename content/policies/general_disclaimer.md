+++
date = "2015-11-02"
weight = 100

title = "General disclaimer"

aliases = [
    "/old-wiki/Apertis:General_disclaimer"
]
+++

The Apertis website has recently transitioned from a wiki (an online
open-content collaborative resource) to a statically generated site. A lot of
the content provided by this site was generated whilst it was a wiki, this
means:

## No formal peer review

Our active community of editors used tools such as the `Recentchanges` and
`Newpages` feeds to monitor new and changing content. However, Apertis is not
uniformly peer reviewed; while readers may correct errors or engage in casual
peer review, they have no legal duty to do so and thus all information read
here is without any implied warranty of fitness for any purpose or use
whatsoever. Even articles that have been vetted by informal peer review
processes may later have been edited inappropriately.

**None of the contributors, sponsors, administrators or anyone else
connected with Apertis in any way whatsoever can be responsible for the
appearance of any inaccurate or libelous information or for your use of
the information contained in or linked from these web pages.**

## No contract; limited license

Please make sure that you understand that the information provided here
is being provided freely, and that no kind of agreement or contract is
created between you and the owners or users of this site, the owners of
the servers upon which it is housed, the individual Apetis contributors,
any project administrators, sysops or anyone else who is in *any way
connected* with this project or partner projects subject to your claims
against them directly. You are being granted a limited license to copy
anything from this site; it does not create or imply any contractual or
extracontractual liability on the part of Apertis or any of its agents,
members, organizers or other users.

There is **no agreement or understanding between you and Apertis**
regarding your use or modification of this information beyond the
[Creative Commons Attribution-Sharealike 3.0 Unported
License](https://creativecommons.org/licenses/by-sa/3.0/) (CC-BY-SA);
neither is anyone at Apertis responsible should someone change, edit,
modify or remove any information that you may post on any Apertis
websites.

## Trademarks

Any of the trademarks, service marks, collective marks, design rights or
similar rights that are mentioned, used or cited in the articles of the
Apertis wiki are the property of their respective owners. Their use here
does not imply that you may use them for any purpose other than for the
same or a similar informational use as contemplated by the original
authors of these Apertis articles under the CC-BY-SA licensing scheme.
Unless otherwise stated Apertis and Apertis sites are neither endorsed
by nor affiliated with any of the holders of any such rights and as such
Apertis cannot grant any rights to use any otherwise protected
materials. Your use of any such or similar incorporeal property is at
your own risk.

## Credits

*The first version of this page was based on
<https://en.wikipedia.org/wiki/Wikipedia:General_disclaimer>*
