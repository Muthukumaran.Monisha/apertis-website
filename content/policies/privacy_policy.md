+++
date = "2015-11-05"
lastmod= "2020-05-29"
weight = 100

title = "Apertis Privacy policy"

aliases = [
    "/old-wiki/Apertis:Privacy_policy"
]
+++

This Policy explains how we collect, use, and share your personal
information.

  - We collect very little personal information about you.
  - We do not rent or sell your information to third parties.

By using Apertis Websites, you consent to this Policy.

We believe that information-gathering and use should go hand-in-hand
with transparency. This Privacy Policy explains how information is
collected from Apertis Websites, used and shared information we receive
from you through your use of the Apertis Websites. It is essential to
understand that, by using any of the Apertis Websites, you consent to
the collection, transfer, processing, storage, disclosure, and use of
your information as described in this Privacy Policy. That means that
reading this Policy carefully is important.

We do not sell or rent your nonpublic information, nor do we give it to
others to sell you anything. We use it to figure out how to make the
Apertis Websites more engaging and accessible, to see which ideas work,
and to make learning and contributing more fun. Put simply: we use this
information to make the Apertis Websites better for you.

### Definitions

Because everyone (not just lawyers) should be able to easily understand
how and why their information is collected and used, we use common
language instead of more formal terms throughout this Policy. To help
ensure your understanding of some particular key terms, here is a table
of translations:

<table>
<thead>
<tr class="header">
<th><p>When we say…</p></th>
<th><p>…we mean:</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>"Collabora Ltd." / "we" / "us" / "our"</p></td>
<td><p>Collabora Ltd., a company registered in the UK that operates the Apertis websites.</p></td>
</tr>
<tr class="even">
<td><p>"Apertis Websites" / "our services"</p></td>
<td><p>Apertis Websites and services, including our main projects as well as mobile applications, APIs, emails, and notifications.</p></td>
</tr>
<tr class="odd">
<td><p>"you" / "your"</p></td>
<td><p>You, regardless of whether you are an individual, group, or organization, and regardless of whether you are using the Apertis Websites or our services on behalf of yourself or someone else.</p></td>
</tr>
<tr class="even">
<td><p>"this Policy" / "this Privacy Policy"</p></td>
<td><p>This document, entitled the "Apertis Privacy Policy".</p></td>
</tr>
<tr class="odd">
<td><p>"contributions"</p></td>
<td><p>Content you add or changes you make to any Apertis Websites.</p></td>
</tr>
<tr class="even">
<td><p>"personal information"</p></td>
<td><p>Information you provide us or information we collect from you that could be used to personally identify you. To be clear, while we do not necessarily collect all of the following types of information, we consider at least the following to be “personal information” if it is otherwise nonpublic and can be used to identify you:</p>
<dl>
<dt></dt>
<dd>(a) your real name, address, phone number, email address, password, identification number on government-issued ID, IP address, user-agent information, credit card number;
</dd>
<dd>(b) when associated with one of the items in subsection (a), any sensitive data such as date of birth, gender, sexual orientation, racial or ethnic origins, marital or familial status, medical conditions or disabilities, political affiliation, and religion; and
</dd>
<dd>(c) any of the items in subsections (a) or (b) when associated with your user account.
</dd>
</dl></td>
</tr>
<tr class="odd">
<td><p>"third party" / "third parties"</p></td>
<td><p>Individuals, entities, websites, services, products, and applications that are not controlled, managed, or operated by Collabora Ltd.. This includes other Apertis users and independent organizations or groups who help promote the Apertis project such as user groups, as well as volunteers, employees, directors, officers, grant recipients, and contractors of those organizations or groups.</p></td>
</tr>
</tbody>
</table>

### What this privacy policy does & does not cover

Except as explained below, this Privacy Policy applies to our collection
and handling of information about you that we receive as a result of
your use of any of the Apertis Websites. This Policy also applies to
information that we receive from our partners or other third parties. To
understand more about what this Privacy Policy covers, please see below.

For the sake of clarity, this Privacy Policy covers, regardless of
language:

  - All of our major sites, such as the Apertis website, Apertis gitlab
    instance, including user pages, discussion pages, and noticeboards.
  - Our blogs and APIs (unless we have provided a separate policy for
    those services).
  - Our mobile sites and applications, which can be found on our
    official list.
  - Emails, SMS, and notifications from us or sent to us from you.

## Use of information

### Types of information we receive from you, how we get it, & how we use it

#### Your public contributions

Whatever you post on Apertis Websites can be seen and used by everyone.

When you make a contribution to any Apertis Website, including on
Phabricator or the GitLab instance, you are creating a permanent, public
record of every piece of content added, removed, or altered by you. We may
use your public contributions, either aggregated with the public
contributions of others or individually, to create new features or to
learn more about how the Apertis Websites are used.

Unless this Policy says otherwise, you should assume that information
that you actively contribute to the Apertis Websites, including personal
information, is publicly visible and can be found by search engines.
Like most things on the Internet, anything you share may be copied and
redistributed throughout the Internet by other people. Please do not
contribute any information that you are uncomfortable making permanently
public, like revealing your real name or location in your contributions.

You should be aware that specific data made public by you or aggregated
data that is made public by us can be used by anyone for analysis and to
infer information about users, such as which country a user is from,
political affiliation, and gender.

#### Account information & registration

You need to create an account to contribute to most Apertis Websites.
You are not required to create an account to read most Apertis Websites.

If you want to create accounts, in most cases we require only a username,
email address and a password. Your username will be publicly visible, so
please be careful about using your real name as your username. Your password is
only used to verify that the account is yours. Your IP address is also
automatically submitted to us, and we record it temporarily to help
prevent abuse.

Once created, user accounts cannot be removed entirely (although you can
usually hide the information on your user page if you choose to). This
is because your public contributions must be associated with their
author (you\!). So make sure you pick a name that you will be
comfortable with for years to come.

To gain a better understanding of the demographics of our users, to
localize our services, and to learn how we can improve our services, we
may ask you for more demographic information, such as gender or age,
about yourself. We will tell you if such information is intended to be
public or private, so that you can make an informed decision about
whether you want to provide us with that information. Providing such
information is always completely optional. If you don't want to, you
don't have to — it's as simple as that.

#### Information we receive automatically

Like other websites, we receive some information about you automatically
when you visit the Apertis Websites. This information helps us
administer the Apertis websites and enhance your user experience.

Because of how browsers work and similar to other major websites, we
receive some information automatically when you visit the Apertis
Websites. This information includes the type of device you are using
(possibly including unique device identification numbers), the type and
version of your browser, your browser's language preference, the type
and version of your device's operating system, in some cases the name of
your internet service provider or mobile carrier, the website that
referred you to the Apertis websites, which pages you request and visit,
and the date and time of each request you make to the Apertis Websites.

Put simply, we may use this information to enhance your experience with
Apertis Websites. For example, we may use this information to administer
the sites, provide greater security, and fight vandalism; optimize
mobile applications, customize content and set language preferences,
test features to see what works, and improve performance; understand how
users interact with the Apertis Websites, track and study use of various
features, gain understanding about the demographics of the different
Apertis Websites, and analyze trends.

#### Information we collect

##### Emails

If you provide your email address, we will keep it confidential, except as
provided in this Policy.

We may occasionally send you emails about important information.

You may choose to opt out of certain kinds of notifications.

You will need to provide an email address at the time of registration
to be able to access certain portions of some of the Apertis Websites. If
you do so, your email address is kept confidential, except as provided in
this Policy. We do not sell, rent, or use your email address to
advertise third-party products or services to you.

We use your email address to let you know about things that are
happening with Apertis or the Apertis Websites, such as telling you
important information about your account, letting you know if something
is changing about the Apertis Websites or policies, and alerting you
when there has been a change to a page or project that you have decided
to follow. Please note that if you email us, we may keep your message,
email address, and any other information you provide us, so that we can
process and respond to your request.

You can choose to limit some of these kinds of notifications, like those
alerting you if an page changes. Others, such as those containing
critical information that all users need to know to participate
successfully in the Apertis Websites, you may not be able to opt out of.
You can manage what kinds of notifications you receive and how often you
receive them by going to your Notifications Preferences.

We will never ask for your password by email (but may send you a
temporary password via email if you have requested a password reset).

##### Location information

When you visit any Apertis Website, we automatically receive the IP address
of the device (or your proxy server) you are using to access the Internet,
which could be used to infer your geographical location. We keep IP
addresses confidential, except as provided in this Policy.

## Sharing

### When may we share your information?

We may share your information when you give us specific permission to do
so.

#### With your permission

We may share your information for a particular purpose, if you agree.

#### For legal reasons

We will disclose your information in response to an official legal
process only if we believe it to be legally valid. We will notify you of
such requests when possible.

We may access, preserve, or disclose your personal information if we
reasonably believe it necessary to satisfy a valid and legally
enforceable warrant, subpoena, court order, law or regulation, or other
judicial or administrative order. However, if we believe that a
particular request for disclosure of a user's information is legally
invalid or an abuse of the legal system and the affected user does not
intend to oppose the disclosure themselves, we will try our best to
fight it. We are committed to notifying you via email at least ten (10)
calendar days, when possible, before we disclose your personal
information in response to a legal demand. However, we may only provide
notice if we are not legally restrained from contacting you, there is no
credible threat to life or limb that is created or increased by
disclosing the request, and you have provided us with an email address.

Nothing in this Privacy Policy is intended to limit any legal objections
or defenses you may have to a third party's request (whether it be
civil, criminal, or governmental) to disclose your information. We
recommend seeking the advice of legal counsel immediately if such a
request is made involving you.

#### If the Organization is Transferred (Really Unlikely\!)

In the unlikely event that the ownership of Apertis changes, we will
provide you 30 days notice before any personal information is
transferred to the new owners or becomes subject to a different privacy
policy.

In the extremely unlikely event that ownership of all or substantially
all of the Apertis changes, or we go through a reorganization (such as a
merger, consolidation, or acquisition), we will continue to keep your
personal information confidential, except as provided in this Policy,
and provide notice to you via the Apertis Websites at least thirty (30)
calendar days before any personal information is transferred or becomes
subject to a different privacy policy.

#### To protect you, ourselves & others

We, or users with certain administrative rights, may disclose
information that is reasonably necessary to:

  - enforce or investigate potential violations of Apertis or
    community-based policies;
  - protect our organization, infrastructure, employees, contractors, or
    the public; or
  - prevent imminent or serious bodily harm or death to a person.

We, or particular users with certain administrative rights as described
below, may need to share your personal information if it is reasonably
believed to be necessary to enforce or investigate potential violations
of this Privacy Policy, or any Apertis or user community-based policies.
We may also need to access and share information to investigate and
defend ourselves against legal threats or actions.

Apertis Websites are collaborative, with users writing most of the
policies and selecting from amongst themselves people to hold certain
administrative rights. These rights may include access to limited
amounts of otherwise nonpublic information about contributions and
activity by other users. They use this access to help protect against
vandalism and abuse, fight harassment of other users, and generally try
to minimize disruptive behavior on the Apertis Websites. These various
user-selected administrative groups that have their own privacy and
confidentiality guidelines. These user-selected administrative groups
are accountable to other users through checks and balances: users are
selected through a community-driven process and overseen by their peers
through a logged history of their actions. However, the legal names of
these users are not known to Collabora Ltd.

We hope that this never comes up, but we may disclose your personal
information if we believe that it's reasonably necessary to prevent
imminent and serious bodily harm or death to a person, or to protect our
organization, employees, contractors, users, or the public. We may also
disclose your personal information if we reasonably believe it necessary
to detect, prevent, or otherwise assess and address potential spam,
malware, fraud, abuse, unlawful activity, and security or technical
concerns.

#### To our service providers

We may disclose personal information to our third-party service
providers or contractors to help run or improve the Apertis Websites.

As hard as we may try, we can't do it all. So sometimes we use
third-party service providers or contractors who help run or improve the
Apertis Websites for you and other users. We may give access to your
personal information to these providers or contractors as needed to
perform their services for us or to use their tools and services. We put
requirements, such as confidentiality agreements, in place to help
ensure that these service providers treat your information consistently
with, and no less protective of your privacy than, the principles of
this Policy.

#### To Understand & Experiment

We may give volunteer developers and researchers access to systems that
contain your information to allow them to protect, develop, and
contribute to the Apertis Websites.

We may also share non-personal or aggregated information with third
parties interested in studying the Apertis Websites.

When we share information with third parties for these purposes, we put
reasonable technical and contractual protections in place to protect
your information consistent with this Policy.

The open-source software that powers the Apertis Websites depends on the
contributions of volunteer software developers, who spend time writing
and testing code to help it improve and evolve with our users' needs. To
facilitate their work, we may give some developers limited access to
systems that contain your personal information, but only as reasonably
necessary for them to develop and contribute to the Apertis Websites.

When we give access to personal information to third-party developers or
researchers, we put requirements, such as reasonable technical and
contractual protections, in place to help ensure that these service
providers treat your information consistently with the principles of
this Policy and in accordance with our instructions. If these developers
or researchers later publish their work or findings, we ask that they
not disclose your personal information. Please note that, despite the
obligations we impose on developers and researchers, we cannot guarantee
that they will abide by our agreement, nor do we guarantee that we will
regularly screen or audit their projects.

#### Because you made it public

Information that you post is public and can been seen and used by
everyone.

Any information you post publicly on the Apertis Websites is just that –
public. For example, if you put your phone number in a merge request
comment, that is public, and not protected by this Policy. Please think
carefully about your desired level of anonymity before you disclose
personal information.

## Protection

### How do we protect your data?

We use a variety of physical and technical measures, policies, and
procedures to help protect your information from unauthorized access,
use, or disclosure.

We strive to protect your information from unauthorized access, use, or
disclosure. We use a variety of physical and technical measures,
policies, and procedures (such as access control procedures, network
firewalls, and physical security) designed to protect our systems and
your personal information. Unfortunately, there's no such thing as
completely secure data transmission or storage, so we can't guarantee
that our security will not be breached (by technical measures or through
violation of our policies and procedures).

### How Long Do We Keep Your Data?

We only keep your personal information as long as necessary to maintain,
understand, and improve the Apertis Websites or to comply with UK law.

Once we receive personal information from you, we keep it for the
shortest possible time that is consistent with the maintenance,
understanding, and improvement of the Apertis Websites, and our
obligations under applicable UK law. Non-personal information may be
retained indefinitely.

Please remember that certain information is retained and displayed
indefinitely, such as any public contributions to the Apertis Websites.

## Important info

**For the protection of the Apertis community and Collabora Ltd., if you
do not agree with this Privacy Policy, you may not use the Apertis
Websites.**

### Where is Collabora Ltd. & what does that mean for you?

You are consenting to the use of your information in Europe and to the
transfer of that information to other countries in connection to providing
our services to you and others.

Collabora Ltd. is a registered company based in Cambridge, UK with
servers and data centers located in the UK. If you decide to use Apertis
Websites, whether from inside or outside of the UK, you consent to the
collection, transfer, storage, processing, disclosure, and other uses of
your information in the UK as described in this Privacy Policy. You also
consent to the transfer of your information by Collabora Ltd. from the
UK to other countries, which may have different or less stringent data
protection laws than your country, in connection with providing services
to you.

### Our response to Do Not Track (DNT) signals

We do not allow tracking by third-party websites you have not visited.

We do not share your data with third parties for marketing purposes.

We are strongly committed to not sharing nonpublic information with
third parties. In particular, we do not allow tracking by third-party
websites you have not visited (including analytics services, advertising
networks, and social platforms), nor do we share your information with
any third parties for marketing purposes. Under this Policy, we may
share your information only under particular situations, which you can
learn more about in the "When May We Share Your Information" section of
this Privacy Policy.

Because we protect all users in this manner, we do not change our
behavior in response to a web browser's "do not track" signal.

### Changes to this Privacy Policy

Because things naturally change over time and we want to ensure our
Privacy Policy accurately reflects our practices and the law, it may be
necessary to modify this Privacy Policy from time to time. We reserve
the right to do so in the following manner:

  - For minor changes, such as grammatical fixes, administrative or
    legal changes, or corrections of inaccurate statements, we will post
    the changes and, when possible, provide at least three (3) calendar
    days' prior notice.

We ask that you please review the most up-to-date version of our
[Privacy Policy]( {{< ref "privacy_policy.md" >}} ). Your continued use
of the Apertis Websites after this Privacy Policy becomes effective
constitutes acceptance of this Privacy Policy on your part. Your
continued use of the Apertis Websites after any subsequent version of
this Privacy Policy becomes effective, following notice as outlined
above, constitutes acceptance of that version of the Privacy Policy on
your part.

### Thank you\!

Thank you for reading our Privacy Policy.

**This privacy policy went into effect on November 1st 2015.**

**This privacy policy was last updated on July 1st 2020.**

## Credits

*The first version of this page was based on
<https://wikimediafoundation.org/wiki/Privacy_policy>*
