+++
date = "2020-09-02"
weight = 100

title = "Image Download"
+++

The Apertis project provides a number of images, designed for evaluation and
development purposes. The Apertis project recommends to use the current stable
release images presented below for evaluation.

- **SDK**: The SDK is a development environment primarily designed to be run in
  [VirtualBox]({{< ref "virtualbox.md" >}}).

  | Image | Optional Downloads |
  | ----- | ------------------ |
  | [Intel 64-bit (amd64)]({{< image-url Stable amd64 sdk >}}) | |

- **Minimal**: A compact image, utilizing
  [OSTree]({{< ref "guides/ostree.md" >}}) for updates, designed to be run as a
  headless system on [target hardware]({{< ref "reference_hardware" >}}).

  | Image | Optional Downloads |
  | ----- | ------------------ |
  | [Intel 64-bit (amd64)]({{< image-url Stable amd64 minimal ostree >}}) | [bmap file]({{< image-url Stable amd64 minimal ostree bmap >}}) |
  | [ARM 32-bit (armhf)]({{< image-url Stable armhf minimal ostree >}}) | [bmap file]({{< image-url Stable armhf minimal ostree bmap >}}) |
  | [ARM 64-bit (arm64)]({{< image-url Stable arm64 minimal ostree >}}) | [bmap file]({{< image-url Stable arm64 minimal ostree bmap >}}) |

- **Target**: A more featureful image, utilizing
  [OSTree]({{< ref "guides/ostree.md" >}}) for updates, providing an example
  graphical environment and designed to be run on
  [target hardware]({{< ref "reference_hardware" >}}).

  | Image | Optional Downloads |
  | ----- | ------------------ |
  | [Intel 64-bit (amd64)]({{< image-url Stable amd64 target ostree >}}) | [bmap file]({{< image-url Stable amd64 target ostree bmap >}}) |
  | [ARM 32-bit (armhf)]({{< image-url Stable armhf target ostree >}}) | [bmap file]({{< image-url Stable armhf target ostree bmap >}}) |
  | [ARM 32-bit (arm64)]({{< image-url Stable arm64 target ostree >}}) | [bmap file]({{< image-url Stable arm64 target ostree bmap >}}) |

  {{% notice note %}}
  No open graphical stack is available for the current stable release on the
  arm64 reference hardware and thus target images aren't available.
  {{% /notice %}}

{{% notice info %}}
Apertis provides a wider selection of [image types]({{< ref "images.md" >}})
designed for other specific purposes and [versions]({{< ref "releases.md" >}})
at differing points in the
[release flow]({{< ref "release-flow.md#apertis-release-flow" >}}). These can
be found on the [Apertis image site](https://images.apertis.org).
{{% /notice %}}
