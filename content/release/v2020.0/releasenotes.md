+++
date = "2020-03-11"
weight = 100

title = "V2020.0 ReleaseNotes"
+++

# Apertis v2020.0 Release

Apertis is a Debian derivative distribution geared towards the creation
of product-specific images for ARM (both the 32bit ARMv7 and 64-bit
ARMv8 versions using the hardfloat ABI) and Intel x86-64 (64-bit)
systems.

**Apertis v2020.0** is the first **stable** release of the LTS Apertis
v2020 [release
flow]( {{< ref "release-flow.md#apertis-release-flow" >}} ).
Apertis is committed to maintaining the v2020 release stream until September 2021.

This Apertis release is built on top of Debian Buster with several
customisations.

Test results for the v2020.0 release are available in the following test
reports:

  - [APT     images](https://lavaphabbridge.apertis.org/report/v2020/20200310.0)
  - [OSTree  images](https://lavaphabbridge.apertis.org/report/v2020/20200310.0/ostree)
  - [NFS  artifacts](https://lavaphabbridge.apertis.org/report/v2020/20200310.0/nfs)
  - [LXC containers](https://lavaphabbridge.apertis.org/report/v2020/20200310.0/lxc)

Point releases including all the security fixes accumulated will be
published quarterly, up to v2020.7.

## Release flow

  - 2019 Q3: v2020dev0
  - 2019 Q4: v2020pre
  - 2020 Q1: **v2020.0**
  - 2020 Q2: v2020.1
  - 2020 Q3: v2020.2
  - 2020 Q4: v2020.3
  - 2021 Q1: v2020.4
  - 2021 Q2: v2020.5
  - 2021 Q3: v2020.6
  - 2021 Q4: v2020.7

### Release downloads

| [Apertis v2020.0 images](https://images.apertis.org/release/v2020/v2020.0/) | | | | |
| --------------------------------------------------------------------------- |-|-|-|-|
| Intel 64-bit        | [minimal](https://images.apertis.org/release/v2020/v2020.0/amd64/minimal/apertis_v2020-minimal-amd64-uefi_v2020.0.img.gz) | [target](https://images.apertis.org/release/v2020/v2020.0/amd64/target/apertis_v2020-target-amd64-uefi_v2020.0.img.gz) | [base SDK](https://images.apertis.org/release/v2020/v2020.0/amd64/basesdk/apertis_v2020-basesdk-amd64-sdk_v2020.0.vdi.gz) | [SDK](https://images.apertis.org/release/v2020/v2020.0/amd64/sdk/apertis_v2020-sdk-amd64-sdk_v2020.0.vdi.gz)
| ARM 32-bit (U-Boot) | [minimal](https://images.apertis.org/release/v2020/v2020.0/armhf/minimal/apertis_v2020-minimal-armhf-uboot_v2020.0.img.gz) | [target](https://images.apertis.org/release/v2020/v2020.0/armhf/target/apertis_v2020-target-armhf-uboot_v2020.0.img.gz)
| ARM 64-bit (U-Boot) | [minimal](https://images.apertis.org/release/v2020/v2020.0/arm64/minimal/apertis_v2020-minimal-arm64-uboot_v2020.0.img.gz)


The Intel `minimal` and `target` images are tested on the
[reference hardware (MinnowBoard MAX)]( {{< ref "/reference_hardware/_index.md" >}} ),
but they can run on any UEFI-based x86-64 system. The `basesdk` and `sdk` images are
[tested under VirtualBox]( {{< ref "/virtualbox.md" >}} ).

#### Apertis v2020.0 repositories

    deb https://repositories.apertis.org/apertis/ v2020 target development sdk hmi
    deb https://repositories.apertis.org/apertis/ v2020-security target development sdk hmi

## New features

### Mainline graphics stack support on ARM target images

Target images for the i.MX6 SabreLite boards with graphics support using the
mainline stack and the open source etnaviv driver [are now
available](https://images.apertis.org/release/v2020/v2020.0/armhf/target/).

Just like the amd64 `target` images, the `armhf` ones for the SabreLite board
now boot into the full HMI out-of-the-box.

### System update authentication checks using Ed25519 signatures

The OSTree updates in this release are now signed to prove their trusted
provenance. The updater on the devices now checks for the signature
against its configurable set of trusted keys before processing the
updates, refusing to install anything that comes from untrusted sources.

While this is usually done with OpenPGP keys and signatures, such
approach is not suitable for Apertis since modern versions of GnuPG
[switched to the GPL-3
license](https://lists.gnupg.org/pipermail/gnupg-announce/2007q3/000255.html)
which conflicts with the [Apertis Open Source license
expectations]( {{< ref "license-expectations.md" >}} ).

In addition, the use of OpenPGP is suboptimal for OSTree: the OpenPGP
standard is large and leads to complex implementations, covering
features that address use-cases like the web of trust that are not
relevant for the system update verification use-case.

To address that, the upstream OSTree code has now been [reworked to
optionally drop its reliance on
GnuPG](https://github.com/ostreedev/ostree/pull/1889), with work being
in progress to land the modularization of the signature verification
mechanism with an alternative backend based on the [Ed25519
cryptosystem](https://ed25519.cr.yp.to/) with the [libsodium
implementation](https://libsodium.org/), which brings the following
benefits:

  - State-of-the-art security
  - Much simpler implementation than GnuPG
  - Small signatures
  - Small keys
  - Fast signature verification
  - Very fast signing

As a technology preview, the Ed25519 backend is already available and
enabled in Apertis showcasing the whole workflow by signing updates
during the image building pipeline on the Apertis server and verifying
those signatures on target device.

### Linux kernel 5.4

Released upstream on the 2019-11-24, the latest LTS version of the Linux kernel
is now included in this release.  With a minimum projected end-of-life in
December 2021, the 5.4 series better suits the v2020 release cycle, which would
reach its own end-of-life at roughly the same time (2021 Q3).

### Persistent kernel panic storage

This release introduces initial support for
[pstore](https://www.kernel.org/doc/Documentation/ABI/testing/pstore) in
the U-Boot bootloader and the Linux kernel to debug hard crashes by
providing a way to store logs for postmortem analysis (specifically next
boot) even if the crash makes the main persistent storage no longer
available or unsafe to access.

A new document [guides developers through the steps needed to use pstore
on the
i.MX6]( {{< ref "pstore.md" >}} )
Sabrelite boards.

### hawkBit integration proof-of-concept

[Eclipse hawkBit™](https://www.eclipse.org/hawkbit/) is a back-end
framework for deploying software updates over the Internet, providing
advanced phased rollout management.

A proof-of-concept hawkBit server instance has been deployed for Apertis
and the image building pipelines now [automatically push updates to
it](https://gitlab.apertis.org/infrastructure/apertis-image-recipes/merge_requests/187)
as OSTree static bundles.

The Apertis v2020 OSTree images now ship the [Apertis hawkBit
agent](https://gitlab.apertis.org/appfw/apertis-hawkbit-agent/)
component by default, which can be enabled to let developers control
updates using the hawkBit server instance.

The [Deployment management
document]( {{< ref "deployment-management.md" >}} )
describes how the updates can be modeled on the hawkBit server and how
to set up agents to connect to it and deploy the updates on the device.

## Build and integration

### GitLab-to-OBS workflow enhancements

The tools around the workflow used to manage package sources in GitLab and push
them to OBS saw several improvements during the v2020 release cycle.

For instance, the GitLab pipeline can now easily target `-security` and
`-updates` repositories and prevents MRs from targeting the main branches on
stable releases.

### Automatic license scans on upstream pull

When pulling updates from upstream distributions like Debian, the package
sources are now automatically scanned to identify code released under
problematic licenses as early as possible.

More details on how the scan works and how developers can control it are
available in the [GitLab-to-OBS pipeline
documentation](https://gitlab.apertis.org/infrastructure/ci-package-builder/-/blob/master/README.md#license-scans).

## Deprecations and ABI/API breaks

During this release cycle no new deprecations have been issued. Obsolete
or problematic APIs are marked with the [ABI
break](https://phabricator.apertis.org/tag/abi_break/) tag as a way to
clear technical debt in future.

### Removal of packages with problematic licenses

A few package got moved away from the `target` component of the distribution
since they didn't meet the [Apertis license
expectations]( {{< ref "license-expectations.md" >}} )
and thus were not suitable for installation in final products.

Downstreams will need to check the following packages:

* dictionaries-common ([T6711](https://phabricator.apertis.org/T6711))
* iptables-persistent ([T6712](https://phabricator.apertis.org/T6712))
* btrfs-progs ([T6710](https://phabricator.apertis.org/T6710))
* libpipeline ([T6713](https://phabricator.apertis.org/T6713))
* quvi and libquvi-scripts ([T6714](https://phabricator.apertis.org/T6714))
* libunistring ([T6715](https://phabricator.apertis.org/T6715))

## Infrastructure

### Apertis Docker registry

The Apertis Docker registry stores Docker images in order to provide a
unified and easily reproducible build environment for developers and
services.

As of today, this includes the `apertis-image-builder`,
`apertis-package-builder`, `apertis-package-source-builder`,
`apertis-testcases-builder` and `apertis-documentation-builder` Docker
images.

### Apertis infrastructure tools

The [Apertis v2020 infrastructure
repository](https://build.collabora.co.uk/project/users/apertis:infrastructure:v2020)
provides packages for the required versions of `ostree-push` and
`ostree` for Debian Buster:

    deb https://repositories.apertis.org/infrastructure-v2020/ buster infrastructure

### Images

Image daily builds, as well as release builds can be found at:

  https://images.apertis.org/

Image build tools can be found in the Apertis tools repositories.

### Infrastructure overview

The [Image build infrastructure
document]( {{< ref "image-build-infrastructure.md" >}} )
provides an overview of the image building process and the involved
services.

## Known issues


