+++
date = "2015-12-18"
weight = 100

title = "15.12 ReleaseNotes"

aliases = [
    "/old-wiki/15.12/ReleaseNotes"
]
+++

# Apertis 15.12 Release

**15.12** is the current stable development distribution of **Apertis**,
a Debian/Ubuntu derivative distribution geared towards the creation of
product-specific images for ARM (ARMv7 using the hardfloat ABI) and
Intel x86 (64/32-bit Intel) systems. Features which are planned for
Apertis can be found on the [Roadmap]() page.

### What's available in the distribution?

The software stack in **15.12** is comprised of the following
technologies:

  - Linux 4.2
  - Graphical subsystem based on X.org X server 1.17.2 and Clutter
    1.22.4 with full Multi-Touch support
  - Network management subsytem provided by ConnMan 1.27, BlueZ 4.101
    and Ofono 1.14
  - Multimedia support provided by GStreamer 1.0
  - The Telepathy framework, with XMPP and SIP support
  - The Folks contact management framework
  - The Clutter port of the WebKit browser engine with support for WebGL
  - The GTK+ port of WebKit with features from WebKit Clutter ported to
    it and a ClutterActor wrapper
  - Size scaled down Apertis image
  - Graphical subsystem image based on Wayland protocol 1.8.1

### What's new in the distribution?

  - Apertis module code reviews
  - Preferences and Persistence design
  - Geolocation and Navigation design
  - Sensors and Actuators design
  - Application Entry Points design
  - Application Layout design
  - Security design
  - Design reviews
  - Initial release of Tartan static analyser
  - JSON testing with Walbottle
  - Containerisation design
  - Text to Speech design
  - hotdoc: new API documentation tool
  - UI Customisation design
  - Example SDK application testing
  - systemd activation for all D-Bus services
  - AppArmor denials cleaned up on boot
  - Application manager's AppArmor profile is enforced
  - Application manager high-level library
  - Improved media management tests
  - DLT support
  - GIR support in Apertis libraries
  - Preliminary BlueZ 5 support

### Release downloads

| [Apertis 15.12 images](https://images.apertis.org/release/15.12/) |
| ----------------------------------------------------------------- |
| Intel 32-bit                                                      |
| Intel 64-bit / Minnowboard MAX                                    |

#### Apertis 15.12 repositories

` deb `<https://repositories.apertis.org/apertis/>` 15.12 target helper-libs development sdk hmi`

#### Apertis 15.12 infrastructure tools

For Debian Jessie based systems:

` deb `<https://repositories.apertis.org/debian/>` jessie tools`

For Ubuntu Trusty based systems:

` deb `<https://repositories.apertis.org/ubuntu/>` trusty tools`

# Apertis 15.12 Release

This release builds on the Ubuntu 15.10 (Wily) release as its base
distribution. The 15.12 release has been verified in the [15.12 test
report]().

## Apertis module code reviews

The code for various Apertis modules has been reviewed for the 15.12
release. As a result of these reviews, various bug fixes and API
improvements may be made in future.

In some cases, more review and improvement is anticipated to be needed
in subsequent quarters.

  - Breamore
  - Canterbury
  - Eye
  - Frampton
  - Frome
  - libclapton
  - libgrassmoor
  - liblightwood
  - libthornbury
  - Rhayader
  - Ribchester
  - Upchurch

## Preferences and Persistence design

The [Preferences and Persistence design]( {{< ref "preferences-and-persistence.md" >}} ) has
been expanded to give more user experience suggestions and a more
thorough treatment of background research.

## Geolocation and Navigation design

An initial version of a [design for handling
navigation]( {{< ref "geolocation-and-navigation.md" >}} ), geolocation, geocoding,
geofencing and other position-based services in the SDK has been
produced and reviewed. Further minor changes are needed before the
design is finished, but the overall architecture has been laid out.

## Sensors and Actuators design

The [Sensors and Actuators design]( {{< ref "sensors-and-actuators.md" >}} ) has been
expanded to cover the use case of multiple and changing sets of
vehicles, a more rigorous design for the security of the system, and
other minor changes as a result of review. Further minor changes may be
made before it is considered final, but the design is quite complete
now.

## Application Entry Points design

The [Application Entry Points]( {{< ref "application-entry-points.md" >}} )
design provides recommendations for how to install and enumerate
application *entry points*, which can be used to represent graphical
menu entries, [content-type handlers]( {{< ref "/concepts/content_hand-over.md" >}} ),
and other applications and agents that implement some
[interface]( {{< ref "/concepts/interface_discovery.md" >}} ). It also provides
recommendations for a transitional path from the historical
GSchema-based approach to the recommended `.desktop`-file-based
approach.

The majority of the built-in applications in the Apertis image have
already been converted to this scheme. The Eye video player and the
various non-app-based services will be converted during the next
quarter, at which point backwards compatibility with the GSchema-based
approach can be removed from the Canterbury application manager.

## Application Layout design

The [Application Layout]( {{< ref "application-layout.md" >}} ) design updates
and clarifies the [Applications design
document]( {{< ref "applications.md" >}} ) to provide concrete
recommendations for how the files in application bundles should be laid
out, including support for multi-user, upgrade, rollback and data reset
use cases.

Some parts of this design have already been implemented: in particular,
several built-in applications have been updated to use the recommended
naming scheme based on [reversed domain
names]( {{< ref "/glossary.md#reversed-domain-name" >}} ).

## Security design

The [Security design document]( {{< ref "security.md" >}} ) has been
updated and expanded. It now describes the general security model and
threat model for the platform, including the trust boundaries between
users and between application bundles, and the need to mediate
communication between application bundles. External references to
Android, iOS and Linux LSMs have been updated. New chapters describe
polkit (formerly PolicyKit) and the xdg-app containerisation framework
(see [Containerisation design](#containerisation-design)),
and provide recommendations for how we can use them to improve Apertis
functionality and security.

## Design reviews

The designs for Breamore (system update server) and Curridge (app store
server) have been reviewed, and some improvements to the implementations
of these components planned as a result.

## Initial release of Tartan static analyser

[Tartan](http://www.freedesktop.org/software/tartan/), the [Clang static
analyser](http://clang-analyzer.llvm.org/) plugin for adding GLib- and
GObject-specific analyses, has been packaged as an initial release for
Apertis. This includes support for `GVariant`, `g_object_new()` and
`GSignal` type checking, plus a variety of nullability and `GError`
correctness checks. It can be run from the SDK or the target, on any
project which can be built using Clang.

## JSON testing with Walbottle

[Walbottle](https://people.collabora.com/~pwith/walbottle/) has been
improved to give better coverage of JSON schemas which use multiple
levels of nested subschemas, and has been integrated with libthornbury,
finding several bugs in its widget parser, widget styler and view parser
implementations, which have all been fixed.

## Containerisation design

Initial research on the requirements and use cases for an implementation
of containerisation (sandboxing) of applications running on Apertis has
been done, and once reviewed will form the basis for a design proposal
for containerisation. Extensive background research has been done on
existing containerisation solutions for Android, iOS and Linux,
including a detailed look at [iOS’ application
sandbox](https://developer.apple.com/app-sandboxing/) (based on
TrustedBSD) and [xdg-app](https://wiki.gnome.org/Projects/SandboxedApps)
for Linux.

One aspect of contained applications is that they should have access to
only a subset of all user settings. After discussion and collaboration
with upstream developers, work has started to write the AppService
project. It is a D-Bus service that will expose to each application only
the subset of settings which its AppArmor profile allows it to read and
write. This change is transparent to applications which already use the
GSettings API. A new `GSettingsBackend` has been implemented to deal
with all the details. Current work-in-progress code is available at
<https://git.collabora.com/cgit/user/xclaesse/appservice.git>

## Text to Speech design

An initial design for a text-to-speech (TTS) API for the Apertis SDK has
been produced, including background research on various existing
solutions for Android, iOS and Linux, and an initial proposal for the
structure and API of the service. The overall architecture for this
service is mostly settled, but minor updates are expected to the
document in the new year before it is implemented.

## hotdoc: new API documentation tool

The 0.6.3 version of [hotdoc](https://github.com/hotdoc/hotdoc) is
packaged for use in the SDK.

Hotdoc has been designed as a documentation micro-framework. It provides
an interface for extensions to plug upon, along with some base objects
(formatters, ...)

It is also designed as an incremental documentation system. Along with
its final output, it produces a database of symbols and a dependency
graph, which can be reused at the next run, or queried upon externally.

For example, [the 'hotdoc
server'](https://github.com/hotdoc/hotdoc_server) uses hotdoc to provide
wiki-like functionalities: users can edit and preview the documentation,
and the server patches the sources it was extracted from.

The extensions currently implemented are listed at
<https://github.com/hotdoc>.

The hotdoc HTML formatter also supports theming. Its base output has no
stylesheets or scripts applied to it whatsoever — this is handled by
themes, which can override the base templates. By default, hotdoc will
use a theme based on bootstrap and isotope, see
[1](https://github.com/hotdoc/hotdoc_bootstrap_theme) for its sources.

Hotdoc also now supports the definition of custom tags to apply to the
documented symbols. The default theme uses isotope to allow the users to
filter the documentation client-side (for example, to only see symbols
in a given version of the documented library), and the ‘tag’ extension
allows the generated documentation, ‘server-side’ to be pre-filtered at
build time.

The hotdoc server is still in a phase of testing, but the initial
prototype is ready, and implements important required functionality:
users can edit the documentation after authenticating themselves with a
common authentication provider (support is implemented for GitHub,
Google and Facebook accounts). While editing the source code comments,
the user can see a live preview being updated, and after having provided
an edit message, they can publish their changes. They then get
redirected to the updated page, and a git commit is made to keep track
of their changes. A demonstration of this for the libgrassmoor
documentation is available at <https://docs.apertis.org/>.

## UI Customisation design

The UI Customisation Design document has had its first draft released.
It aims to cover the architectural design of Apertis widgets and how
they should be used. It contains lots of use cases and strives to cover
these cases with requirements and recommendations.

It still contains some open questions, relating to technologies and use
cases. We are hoping that with a review some of these questions can be
answered and addressed.

## Example SDK application testing

The running environment of an application bundle changes as Apertis is
developed. The instructions for writing an application bundle on the SDK
have been checked and updated at
[HelloWorldBundle](). The process of walking
through these steps to create an example application revealed some bugs
in the SDK, which have been fixed.

## systemd activation for all D-Bus services

All D-Bus *session services* in the target image, and the majority in
the SDK image, are now launched as systemd *user services*, which means
that each service runs in its own cgroup which can be subjected to
resource limits, configured for automatic restart and so on. As well as
integrating the resulting changes into Apertis, we have submitted them
as enhancements for the upstream projects, and in many cases the
enhancement has already been merged. This is a continuation of similar
work in 15.09 which configured every D-Bus *system service* to be
launched as a systemd *system service*.

This involved changes to these third-party projects: at-spi2-core,
blueman colord, dconf, evince, evolution-data-server, gconf, geoclue,
glib-networking, gvfs, obexd, syncevolution, telepathy-gabble,
telepathy-mission-control-5, thunar, tracker, tumbler, xfce4-notifyd,
xfconf, zeitgeist. Additionally, it uncovered a bug in dbus-daemon's
activation code, which was fixed upstream and in Apertis.

The next steps for this topic are:

  - do the same for the remaining non-systemd-activated services in the
    SDK image (which are related to gnome-keyring, and were added after
    this work was completed)
  - modify dbus-daemon to disable its traditional service launching
    mechanism, ensuring that every activated service added in future
    will be a systemd service

## AppArmor denials cleaned up on boot

The AppArmor profiles for many components have been improved so that
they do not log denials during a reboot.

  - Apertis components: Beckfoot, Canterbury, Chalgrove, Corbridge,
    Frampton, mildenhall-launcher, mildenhall-mutter-plugin,
    mildenhall-statusbar, Newport, Ribchester, Shapwick, Tinwell
  - Apertis profiles for third-party components: Avahi, ConnMan

Bugs and tasks have been opened for some issues which need additional
investigation or code changes: , , , , , ,

We plan to continue this cleanup in subsequent releases, and already
have work-in-progress profiles for Barkway, Didcot and Prestwood.

## Application manager’s AppArmor profile is enforced

Canterbury, the application manager service, now has its AppArmor
profile applied in enforcing mode. This means that instead of merely
providing informational ‘complaint’ messages if it takes an unauthorized
action, unauthorized actions are now denied.

All of the applications and services that are run by Canterbury must now
have a matching AppArmor profile: it is not yet required to be in
enforcing mode, but it must exist. Applications and services not meeting
that requirement will not be launched.

We plan to place additional AppArmor profiles in enforcing mode in
subsequent releases.

## Application manager high-level library

Canterbury, the application manager service, now provides a high-level
library, libcanterbury-0, for use by applications and services.

This library currently provides APIs with which a process can query its
own identity, and the identities of processes with which it communicates
via D-Bus. In the latter case, it is intended to guide service and agent
authors into safe usage patterns that avoid time-of-check/time-of-use
vulnerabilities. It should be preferred over more general but
lower-level AppArmor interfaces.

Additionally, it provides an API call that processes can use to
determine a correct persistence directory, using a simple subset of the
[Application Layout design](#application-layout-design).

Additional APIs will be added to this library where needed for future
work. In particular, it is an appropriate place to encapsulate the
implementation of those parts of the [Application Layout
design](#application-layout-design) and [Application Entry
Point design](#application-entry-point-design) that do not
need to cross privilege boundaries.

## Improved media management tests

Tracker and Grilo tests have been completely rewritten in a much more
manageable way. These tests are now more reliable and no longer subject
to race conditions which caused them to spuriously fail before.
Additional unit tests have been added to them to increase coverage of
thumbnailing and media art behaviour on removable devices (i.e. USB
drives).

## DLT support

Support for [GENIVI’s diagnostic log and trace (DLT)
project](http://projects.genivi.org/diagnostic-log-trace/) has been
added to Apertis, including the DLT server on target images, and the
graphical DLT viewer on the SDK. The DLT server has been integrated with
the systemd journal by default, so that all journal messages can be
exported to a DLT viewer for analysis.

## GIR support in Apertis libraries

A significant amount of work has been done on libgrassmoor, libclapton,
libseaton, liblightwood and libthornbury to ensure their APIs are
suitable for gobject-introspection (GIR). This is needed for the [hotdoc
documentation tool](#hotdoc:-new-api-documentation-tool) and
for exposing those SDK APIs in languages like JavaScript and Python. As
this work changes the APIs of these libraries in several ways, it will
land very early in the 16.03 cycle to give maximum time for stabilising
dependencies.

## Preliminary BlueZ 5 support

Several components have been updated for version 5 of the BlueZ
Bluetooth framework, following migration to that version by Ubuntu. This
new version has major API changes, which require synchronized
alterations to multiple Bluetooth-related packages and a rewrite of our
automated tests for this feature.

These new versions have not been integrated in 15.12 to avoid disruption
late in the cycle, and will be integrated during the 16.03 cycle.

## Infrastructure

### Updated packages

During Q4 cycle, several activities have been carried out to be able to
rebase Apertis against new upstream base (Ubuntu Wily Werewolf),
updating the platform with new features and updated for bug and security
fixes. A total of 200 packages have been modified out of 509 in target
component; 171 packages out of 1155 in development component; and 92 out
of 528 in SDK.

SDK component now supports a new architecture, 64 bit Intel, and a new
platform image is provided for that architecture.

In addition to base distribution, a new repository component for helper
libraries ("helper-libs") have been added to the repository, with the
purpose of allowing target applications to build on top of those. HMI
packages have also been updated to their latest stable version.

### OBS Build Projects

  - [OBS Apertis 15.12
    Target](https://build.collabora.co.uk/project/show?project=apertis%3A15.12%3Atarget)
  - [OBS Apertis 15.12 Helper
    Libraries](https://build.collabora.co.uk/project/show?project=apertis%3A15.12%3Ahelper-libs)
  - [OBS Apertis 15.12
    Development](https://build.collabora.co.uk/project/show?project=apertis%3A15.12%3Adevelopment)
  - [OBS Apertis 15.12
    SDK](https://build.collabora.co.uk/project/show?project=apertis%3A15.12%3Asdk)
  - [OBS Apertis 15.12
    HMI](https://build.collabora.co.uk/project/show?project=apertis%3A15.12%3Ahmi)
  - [OBS Apertis Infrastructure
    Tools](https://build.collabora.co.uk/project/show?project=apertis%3Ainfrastructure)

### Shared Repositories

Repositories are found at:

` deb `<https://repositories.apertis.org/apertis/>` 15.12 target helper-libs development sdk hmi`

### Images

Image daily builds, as well as release builds can be found at:

` `<https://images.apertis.org/>

Image build tools can be found in the Apertis tools repositories. Note
that a string is added to package version depending on the distribution
suite based on. For example, if trusty system is to be used expect to
install image-builder_7trusty1

|                                                                                                                           |                  |
| ------------------------------------------------------------------------------------------------------------------------- | ---------------- |
| **Package**                                                                                                               | **Version**      |
| [image-builder, image-tools](https://git.apertis.org/cgit/image-utils.git/)                                               | 7                |
| [apertis-image-configs, apertis-image-scripts (\*)](https://git.apertis.org/cgit/apertis-image-customization.git/)        | 15               |
| [linaro-image-tools, python-linaro-image-tools](https://git.collabora.com/cgit/singularity/tools/linaro-image-tools.git/) | 2012.09.1-1co38  |
| parted, libparted0debian1                                                                                                 | 2.3-11ubuntu1co3 |
| python-debian                                                                                                             | \>=0.1.25        |

### Test Framework

LAVA service at Collabora triggers test cases upon image builds, service
is found at:

` `<https://lava.collabora.co.uk/>

The list of available test cases, including those can be found
[here]( {{< ref "/qa/test_cases" >}} ).

LAVA service packages are available in the Apertis tools repository. To
be able to install it, please follow
[instructions](https://lava.collabora.co.uk/static/docs/installing_on_debian.html#debian-installation)

## Known issues

  - \- factory-reset-tool TC: flagcheck messages are hidden by Plymouth

  - \- folks-sync-only test fails: ofono persona store never becomes
    quiescent

  - \- tracker-indexing-local-storage: test-tracker AssertionError in
    tumbler_assert_thumbnailed

  - \- Crash observed in youtube site on GtkClutterLauncher

  - \- sdk images don't have GPL-3 tar and coreutils, can't build
    debhelper-based packages

  - \- Crash observed in www.cnn.com page on MxLauncher

  - \- sdk-performance-tools-sysprof-smoke-test: Test failed for AMD64
    SDK

  - \- Traffic-control-tcdemo:Video screen transparent partially

  - \- cgroups-resource-control: Some tests failed for ARM and i386

  - \- apparmor-folks: run-test-folks failed in all platforms

  - \- xoo-soft-keys-emulation: xterm: fatal IO error 11 (Resource
    temporarily unavailable)
