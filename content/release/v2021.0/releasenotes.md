+++
date = "2021-03-28"
weight = 100

title = "V2021.0 Release Notes"
+++

Apertis is a Debian derivative distribution geared towards the creation
of product-specific images for ARM (both the 32bit ARMv7 and 64-bit
ARMv8 versions using the hardfloat ABI) and Intel x86-64 (64-bit)
systems.

**Apertis v2021.0** is the first **stable** release of the Apertis v2021
stable [release flow]( {{< ref "release-flow.md#apertis-release-flow" >}} ).
Apertis is committed to maintaining the v2021 release stream up to the end
of 2022.

This Apertis release is built on top of Debian Buster with several
customisations and the Linux kernel 5.10.x LTS series.

Test results for the v2021.0 release are available in the following
test reports:

  - [APT images](https://lavaphabbridge.apertis.org/report/v2021/20210321.0115)
  - [OSTree images](https://lavaphabbridge.apertis.org/report/v2021/20210321.0115/ostree)
  - [NFS artifacts](https://lavaphabbridge.apertis.org/report/v2021/20210321.0115/nfs)
  - [LXC containers](https://lavaphabbridge.apertis.org/report/v2021/20210321.0115/lxc)

## Release flow

  - 2019 Q4: v2021dev0
  - 2020 Q1: v2021dev1
  - 2020 Q2: v2021dev2
  - 2020 Q3: v2021dev3
  - 2020 Q4: v2021pre
  - **2021 Q1: v2021.0**
  - 2021 Q2: v2021.1
  - 2021 Q3: v2021.2
  - 2021 Q4: v2021.3
  - 2022 Q1: v2021.4
  - 2022 Q2: v2021.5
  - 2022 Q3: v2021.6
  - 2022 Q4: v2021.7

### Release downloads

| [Apertis v2021 images](https://images.apertis.org/release/v2021/) | | | | |
| ------------------------------------------------------------------------- |-|-|-|-|
| Intel 64-bit		| [minimal](https://images.apertis.org/release/v2021/v2021.0/amd64/minimal/apertis_v2021-minimal-amd64-uefi_v2021.0.img.gz) | [target](https://images.apertis.org/release/v2021/v2021.0/amd64/target/apertis_v2021-target-amd64-uefi_v2021.0.img.gz) | [base SDK](https://images.apertis.org/release/v2021/v2021.0/amd64/basesdk/apertis_v2021-basesdk-amd64-sdk_v2021.0.vdi.gz) | [SDK](https://images.apertis.org/release/v2021/v2021.0/amd64/sdk/apertis_v2021-sdk-amd64-sdk_v2021.0.vdi.gz)
| ARM 32-bit (U-Boot)	| [minimal](https://images.apertis.org/release/v2021/v2021.0/armhf/minimal/apertis_v2021-minimal-armhf-uboot_v2021.0.img.gz) | [target](https://images.apertis.org/release/v2021/v2021.0/armhf/target/apertis_v2021-target-armhf-uboot_v2021.0.img.gz)
| ARM 64-bit (U-Boot)	| [minimal](https://images.apertis.org/release/v2021/v2021.0/arm64/minimal/apertis_v2021-minimal-arm64-uboot_v2021.0.img.gz)
| ARM 64-bit (Raspberry Pi)	| [minimal](https://images.apertis.org/release/v2021/v2021.0/arm64/minimal/apertis_v2021-minimal-arm64-rpi64_v2021.0.img.gz) | [target](https://images.apertis.org/release/v2021/v2021.0/arm64/target/apertis_v2021-target-arm64-rpi64_v2021.0.img.gz)

The Intel `minimal` and `target` images are tested on the
[reference hardware (MinnowBoard Turbot Dual-Core)]( {{< ref "/reference_hardware/amd64.md" >}} ),
but they can run on any UEFI-based x86-64 system. The `sdk` image is
[tested under VirtualBox]( {{< ref "/virtualbox.md" >}} ).

#### Apertis v2021 repositories

    deb https://repositories.apertis.org/apertis/ v2021 target development sdk hmi
    deb https://repositories.apertis.org/apertis/ v2021-security target development sdk hmi

## New features

### Resilient upgrades with atomic rollback on Renesas R-Car

The OSTree-based update manager is now fully supported on
[Renesas R-Car boards]({{< ref "reference_hardware/arm64.md" >}}) in addition
to the [i.MX6 SABRE Lite boards]({{< ref "reference_hardware/arm32.md" >}})
already supported in previous releases.

Introduced in [v2021pre]({{< ref "release/v2021pre/releasenotes.md" >}}).

### Enhanced offline updates signatures and encryption

During the v2021 development the Apertis team extended the protection offered
by the static delta bundle signature mechanism in OSTree used for offline
updates to also cover the bundle metadata, including the information which
defines whether the update is appropriate for the current device.

The feature has been [developed upstream first](https://github.com/ostreedev/ostree/pull/1985)
and is part of the latest OSTree releases.

To add another layer of protection, offline update bundles can now be encrypted
to make their contents inaccessible from unauthorized parties.

Keys need to be stored securely on devices using solutions like trusted
computing keyrings, but even when secure boot and trusted computing are not
available encryption can help to discourage attackers.

Introduced in [v2021dev2]({{< ref "release/v2021dev2/releasenotes.md" >}})
and [v2021dev3]({{< ref "release/v2021dev3/releasenotes.md" >}}).

### New Rhosydd data model and API based on the W3C VISS specification

[Rhosydd](https://gitlab.apertis.org/pkg/target/rhosydd/), the vehicle device
daemon implementing the
[sensors and actuators API]( {{< ref "sensors-and-actuators.md" >}} )
has been rebased to the data model from the latest W3C specification, as
[described in the design document]( {{< ref "designs/sensors-and-actuators.md#sensors-and-actuators-api" >}} ).

Check the [Vehicle Signal specification](https://github.com/GENIVI/vehicle_signal_specification)
for more information about the data model that replaces the
[old Vehicle Data specification](https://www.w3.org/2014/automotive/data_spec.html).

Introduced in [v2021dev3]({{< ref "release/v2021dev3/releasenotes.md" >}}).

### Technology preview: 64-bit Raspberry Pi boards support

Apertis now ships `rpi64` images for the widely available 64-bit Raspberry Pi
boards to make experimenting on real Arm hardware as accessible as possible.

Resilient updates and rollbacks via OSTree and U-Boot are supported on the
Raspberry Pi boards, joining the ranks of the other Apertis Arm boards.

Unlike the other fully supported Apertis platforms, the `rpi64` images are not
yet part of the daily automated test runs on LAVA.

Introduced in [v2021dev3]({{< ref "release/v2021dev3/releasenotes.md" >}}).

### Technology preview: Maynard and the AGL compositor

A modern, extensible Wayland compositor and a new reference shell are available
in this release, as described in the document about the new
[Application framework]({{< ref "application-framework.md#compositor-libweston" >}}).

{{< figure src="/images/agl-compositor-launcher2-screenshot.png" alt="A screenshot of the Maynard shell and launcher" width="75%" >}}

The AGL compositor and the Maynard shell on top of it will become the default
in the v2022 cycle, replacing the Mildenhall compositor.

Introduced in [v2021pre]({{< ref "release/v2021pre/releasenotes.md" >}}).

### Technology preview: Secure boot on i.MX6 SABRE Lite

A first iteration of trusted boot on the i.MX6 SABRE Lite boards is now part of
the minimal and target ARM 32-bit OSTree images.

Based on the [High Assurance Boot v4
(HABv4)](https://community.nxp.com/docs/DOC-105193) functionality embedded in
the boot ROM, Apertis can now verify the trust chain of the bootloader, the
kernel, the dtb and the initramfs.

Check the documentation about the
[implementation]( {{< ref "secure-boot.md#apertis-secure-boot-implementation-steps" >}} )
and the steps to
[prepare a device]( {{< ref "secure-boot.md#sabrelite-secure-boot-preparation" >}} )
for testing the secure boot functionality.

At the moment no checks are done after that to validate the root filesystem and
its contents, including kernel modules loaded on-demand.

Introduced in [v2021dev1]({{< ref "release/v2021dev1/releasenotes.md" >}}).

## Build and integration

### GitLab CI/CD image building pipelines

From this release Apertis has fully switched to GitLab CI/CD to build the
release artifacts, and Jenkins is no longer supported.

Among the advantages of GitLab CI/CD:

* flexible access controls
* gated merge requests on successful builds to avoid breakage on the main branch
* improved downstream customizability
* autoscaling cloud workers
* multi-project orchestration

Introduced in [v2021dev1]({{< ref "release/v2021dev1/releasenotes.md" >}}).

### Long term reproducibility of image builds

The GitLab CI/CD pipelines used to build the Apertis v2021 release make
reproducing past builds easily and reliably, as described in the
[Long term reproducibility]({{< ref "long-term-reproducibility.md" >}})
document.

Introduced in [v2021pre]({{< ref "release/v2021pre/releasenotes.md" >}}).

### Flatpak runtime and application build pipeline

As described in the
[Application framework]( {{< ref "application-framework.md#application-runtime-flatpak" >}} )
document, Flatpak fills a key role to decouple application deployment for the
base OS.

To make the adoption of Flatpak easier, Apertis provides a reference pipeline
to build runtimes and application based on packages in the Apertis repository.

A Flatpak runtime
[shipping the Mildenhall libraries](https://gitlab.apertis.org/infrastructure/apertis-flatdeb-mildenhall)
is also available to help the transition from the Canterbury application
framework and the
[Mildenhall sample applications](https://gitlab.apertis.org/sample-applications)
have been ported to it.

Introduced in [v2021dev2]({{< ref "release/v2021dev2/releasenotes.md" >}}).

## QA

### Automated OTA updates testcases (v2021dev2)

The previously manual testcase exercising the Over-the-Air update functionality
are now fully automated and run as part of the daily test rounds on LAVA.

Introduced in [v2021dev2]({{< ref "release/v2021dev2/releasenotes.md" >}}).

## Deprecations and ABI/API breaks

### Breaking changes

#### Jenkins pipelines are no longer supported

With the introduction of the GitLab CI/CD image building pipelines, building
Apertis images on Jenkins is no longer supported.

#### FIT support is mandatory for the ARM 32-bit OSTree images

With the introduction of secure boot for i.MX6 SABRE Lite boards in v2021dev1
the Linux kernel must now be provided in the FIT (Flattened Image Tree) format.

#### OSTree offline update bundle format change

The OSTree static bundle format used prior to v2021pre is not supported and
the format that got landed upstream in the `libostree` 2020.7 release should be
used instead.

### Breaks

#### Image builds now happen on GitLab CI/CD

The Jenkins based pipelines are no longer supported and GitLab CI/CD should be
used instead.

#### OSTree signing API changes

During the upstreaming process the new OSTree signing API that Apertis shipped
in the previous cycle as a preview some API changes have been requested.

The version of OSTree in v2021 now reflects the upstream API and users of
the new signing API will need to be adjusted.

### Rhosydd VISS API

The API and the data model of Rhosydd has changed to reflect the W3C
VISS specification.

## Infrastructure

### Apertis Docker images

The Apertis Docker images provide a unified and easily reproducible build
environment for developers and services.

As of today, this includes the
[`apertis-base`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2021-base),
[`apertis-image-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2021-image-builder),
[`apertis-package-source-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2021-package-source-builder),
[`apertis-flatdeb-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2021-flatdeb-builder),
[`apertis-documentation-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2021-documentation-builder),
and [`apertis-testcases-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2021-testcases-builder),
Docker images.

### Apertis infrastructure tools

The [Apertis v2021 infrastructure repository](https://build.collabora.co.uk/project/users/apertis:infrastructure:v2021)
provides packages for the required versions of `ostree-push` and
`ostree` for Debian Buster:

    deb https://repositories.apertis.org/infrastructure-v2021/ buster infrastructure

### Images

Image daily builds, as well as release builds can be found at https://images.apertis.org/

Image build tools can be found in the Apertis tools repositories.

### Infrastructure overview

The
[Image build infrastructure document]( {{< ref "image-build-infrastructure.md" >}} )
provides an overview of the image building process and the involved
services.

## Known issues

### High (9)
 - [T7014](https://phabricator.apertis.org/T7014)	bluez-phone: Message Access Profile test_profile_map_mse fails when there are many contacts
 - [T7623](https://phabricator.apertis.org/T7623)	apparmor-pulseaudio: test failed
 - [T7729](https://phabricator.apertis.org/T7729)	Failed to attach Bluetooth dongle on VirtualBox 6.1.16
 - [T7781](https://phabricator.apertis.org/T7781)	aum-ota-api: test failed due to new bootcount test needing a newer uboot
 - [T7783](https://phabricator.apertis.org/T7783)	ade-commands: test failed
 - [T7801](https://phabricator.apertis.org/T7801)	agl-compositor test fails
 - [T7802](https://phabricator.apertis.org/T7802)	flatpak install -y org.apertis.mildenhall.Platform org.apertis.mildenhall.Sdk
 - [T7803](https://phabricator.apertis.org/T7803)	sdk-debos-image-building: test failed with "Failed to register machine: Unit machine-root.scope already exists"
 - [T7816](https://phabricator.apertis.org/T7816)	Polling issues faced in apertis hawkbit agent

### Normal (63)
 - [T2896](https://phabricator.apertis.org/T2896)	Crash when initialising egl on ARM target
 - [T2930](https://phabricator.apertis.org/T2930)	Develop test case for out of screen events in Wayland images
 - [T3210](https://phabricator.apertis.org/T3210)	Fix Tracker testcase to not download media files from random HTTP user folders
 - [T3233](https://phabricator.apertis.org/T3233)	Ribchester: deadlock when calling RemoveApp() right after RollBack()
 - [T3321](https://phabricator.apertis.org/T3321)	libgles2-vivante-dev is not installable
 - [T3920](https://phabricator.apertis.org/T3920)	arm-linux-gnueabihf-pkg-config does not work with sysroots installed by `ade`
 - [T4092](https://phabricator.apertis.org/T4092)	Containers fail to load on Gen4 host
 - [T4293](https://phabricator.apertis.org/T4293)	Preseed action is needed for Debos
 - [T4307](https://phabricator.apertis.org/T4307)	ribchester-core causes apparmor denies on non-btrfs minimal image
 - [T4422](https://phabricator.apertis.org/T4422)	do-branching fails at a late stage cloning OBS binary repos
 - [T4444](https://phabricator.apertis.org/T4444)	A 2-3 second lag between the speakers is observed when a hfp connection is made over bluetooth
 - [T4660](https://phabricator.apertis.org/T4660)	Eclipse Build is not working for HelloWorld App
 - [T4693](https://phabricator.apertis.org/T4693)	Not able to create namespace for AppArmor container on the internal mx6qsabrelite images with proprietary kernel
 - [T5487](https://phabricator.apertis.org/T5487)	Wi-Fi search button is missing in wifi application
 - [T5748](https://phabricator.apertis.org/T5748)	System users are shipped in /usr/etc/passwd instead of /lib/passwd
 - [T5863](https://phabricator.apertis.org/T5863)	Songs/Videos don't play on i.MX6 with Frampton on internal images
 - [T5896](https://phabricator.apertis.org/T5896)	sdk-dbus-tools-bustle testcase is failing
 - [T5897](https://phabricator.apertis.org/T5897)	apparmor-ofono test fails
 - [T5900](https://phabricator.apertis.org/T5900)	evolution-sync-bluetooth test fails
 - [T5901](https://phabricator.apertis.org/T5901)	eclipse-plugins-apertis-management package is missing
 - [T5906](https://phabricator.apertis.org/T5906)	Video does not stream in WebKit on the i.MX6 internal images
 - [T5931](https://phabricator.apertis.org/T5931)	connman-usb-tethering test fails
 - [T6001](https://phabricator.apertis.org/T6001)	eclipse-plugins-remote-debugging test fails
 - [T6008](https://phabricator.apertis.org/T6008)	The pacrunner package used for proxy autoconfiguration is not available
 - [T6024](https://phabricator.apertis.org/T6024)	folks-inspect: command not found
 - [T6052](https://phabricator.apertis.org/T6052)	Multimedia playback is broken on the internal i.MX6 images (internal 3.14 ADIT kernel issue)
 - [T6077](https://phabricator.apertis.org/T6077)	youtube Videos are not playing on upstream webkit2GTK
 - [T6078](https://phabricator.apertis.org/T6078)	Page scroll is lagging in Minibrowser on upstream webkit2GTK
 - [T6111](https://phabricator.apertis.org/T6111)	traprain: 7_traprain test failed
 - [T6243](https://phabricator.apertis.org/T6243)	AppArmor ubercache support is no longer enabled after 18.12
 - [T6291](https://phabricator.apertis.org/T6291)	Generated lavaphabbridge error report email provides wrong link for full report link
 - [T6292](https://phabricator.apertis.org/T6292)	gettext-i18n: test failed
 - [T6349](https://phabricator.apertis.org/T6349)	sdk-code-analysis-tools-splint: 3_sdk-code-analysis-tools-splint test failed
 - [T6366](https://phabricator.apertis.org/T6366)	sdk-cross-compilation: 10_sdk-cross-compilation test failed
 - [T6444](https://phabricator.apertis.org/T6444)	aum-update-rollback-tests/arm64,amd64: Automatic power cut test should be reworked to reduce the speed of delta read
 - [T6446](https://phabricator.apertis.org/T6446)	aum-update-rollback-tests/amd64: DNS not available in LAVA tests after reboot
 - [T6620](https://phabricator.apertis.org/T6620)	Repeatedly plugging and unplugging a USB flash drive on i.MX6 (Sabrelite) results in USB failure
 - [T6662](https://phabricator.apertis.org/T6662)	SDK: command-not-found package is broken
 - [T6669](https://phabricator.apertis.org/T6669)	Stop building cross compilers tools and libraries for not supported platforms
 - [T6768](https://phabricator.apertis.org/T6768)	Fix the kernel command line generation in OSTRee for FIT image
 - [T6773](https://phabricator.apertis.org/T6773)	HAB testing: the unsigned image may pass validation in several circumstances
 - [T6783](https://phabricator.apertis.org/T6783)	Kernel  trace on armhf board with attached screen
 - [T6795](https://phabricator.apertis.org/T6795)	SabreLite failing to boot due to failing "to start udev Coldplug all Devices"
 - [T6806](https://phabricator.apertis.org/T6806)	HAB on SabreLite in open state accepts any signed kernel regardless of the signing key
 - [T6885](https://phabricator.apertis.org/T6885)	gitlab-rulez fails to set location of the gitlab-ci.yaml on first run
 - [T6887](https://phabricator.apertis.org/T6887)	ARM64 target does not reboot automatically
 - [T6961](https://phabricator.apertis.org/T6961)	audio-backhandling feature fails
 - [T7000](https://phabricator.apertis.org/T7000)	DNS resolution does not work in Debos on some setups
 - [T7012](https://phabricator.apertis.org/T7012)	Apparmor Denied session logs keep popping up on the terminal while executing tests
 - [T7016](https://phabricator.apertis.org/T7016)	network proxy for browser application is not resolving on mildenhall-compositor
 - [T7127](https://phabricator.apertis.org/T7127)	apparmor-functional-demo:  test failed
 - [T7128](https://phabricator.apertis.org/T7128)	apparmor-session-lockdown-no-deny
 - [T7129](https://phabricator.apertis.org/T7129)	apparmor-tumbler: test failed
 - [T7333](https://phabricator.apertis.org/T7333)	apparmor-geoclue: test failed
 - [T7503](https://phabricator.apertis.org/T7503)	Failed to unmount /usr on ostree-based images: Device or resource busy log is seen on rebooting
 - [T7512](https://phabricator.apertis.org/T7512)	debos sometimes fails to mount things
 - [T7530](https://phabricator.apertis.org/T7530)	ADE can't download amd64 sysroot.
 - [T7617](https://phabricator.apertis.org/T7617)	frome: test failed
 - [T7721](https://phabricator.apertis.org/T7721)	Fakemachine in debos immediately powers off and hangs in v2021 and v2022dev1 when using UML on the runners
 - [T7776](https://phabricator.apertis.org/T7776)	On executing system-update test on hawkbit-agent wrong delta is selected
 - [T7785](https://phabricator.apertis.org/T7785)	DNS over TLS does not work on systemd-resolve
 - [T7812](https://phabricator.apertis.org/T7812)	The dlt-daemon build in the devroot testcase fails due to lack of pandoc on Arm platforms
 - [T7815](https://phabricator.apertis.org/T7815)	scan-copyright fails to detect (L)GPL-3 code in util-linux
