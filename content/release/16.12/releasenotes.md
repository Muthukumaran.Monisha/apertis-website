+++
date = "2016-12-16"
weight = 100

title = "16.12 ReleaseNotes"

aliases = [
    "/old-wiki/16.12/ReleaseNotes"
]
+++

# Apertis 16.12 Release

**16.12** is the current stable development release of **Apertis**, a
Debian/Ubuntu derivative distribution geared towards the creation of
product-specific images for ARM (ARMv7 using the hardfloat ABI) and
Intel x86-64 (64-bit) systems.

This Apertis release is based on top of the Ubuntu 16.04 (Xenial) LTS
release with several customization. Test results of the 16.12 release
are available in the [16.12 test report]().

### Release downloads

| [Apertis 16.12 images](https://images.apertis.org/release/16.12/) |
| ----------------------------------------------------------------- |
| Intel 64-bit                                                      |

The `minimal`, `target` and `development` images are tested on the
[reference hardware (MinnowBoard MAX)]( {{< ref "/reference_hardware/_index.md" >}} ),
but they can run on any UEFI-based x86-64 system. The `sdk` image is
[tested under VirtualBox]( {{< ref "/virtualbox.md" >}} ).

#### Apertis 16.12 repositories

` deb `<https://repositories.apertis.org/apertis/>` 16.12 target helper-libs development sdk hmi`

## New features

### Stable application bundle layout

[Version 1.0.0 of the Application Bundle
Specification](https://appdev.apertis.org/documentation/bundle-spec.html)
has been published, which means that the layout of bundles is now stable
and that bundles that adhere to it are guaranteed to be installable on
any future release of Apertis with no changes.

The Canterbury application manager has been updated to match the
specification

### Web runtime

The first implementation of the [Web runtime
specification](https://docs.apertis.org/latest/web-runtime.html) has
landed in Apertis 16.12 as a technology preview. Application bundles can
use the `/usr/bin/bredon-0-launcher` shared launcher to exclusively rely
on HTML/JS/CSS for their needs while still being able to access all the
native GObject-based APIs provides by Apertis through the
GObject-Introspection bindings provided by Seed.

### Application store client

The old application store client implementation (Frome) has been
refreshed to take advantage of the recent development in the finalized
bundle specification, in the application manager (Canterbury) and in the
application mounter (Ribchester), while preserving the same D-Bus API
for applications.

The updated application store client can now create its own account on
the reference server, download packed application bundles using the
download manager service (Newport) and manage the installed state of
application bundles.

### Apertis Development Environment tools

To improve the developer experience for Apertis a lot of effort has been
focused on providing tools to ease the time-consuming parts of setting
up the appropriate development environment.

A command-line tool, `ade`, can now help developers in managing sysroot
environments to target different releases of Apertis for different
architectures, cross-building programs and libraries, packing
application bundles that can be deployed on the target system and
connect to it to run application bundles under an interactive debugger.

### Sample application bundles

The [source code for some simple application
bundles](https://git.apertis.org/cgit/sample-applications/) has been
released to demonstrate how to accomplish specific tasks, showcasing
different aspects of the SDK APIs provided in Apertis.

<dl>

<dt>

Basic application bundle skeleton (hello-world)

<dd>

The hello-world bundle demonstrates the most simple graphical
application, providing a base for all the other sample apps. It only
displays a non-interactive message on screen, but shows how to provide
entry points that can be shown in the application menu by using a
.desktop file, demonstrates how to integrate with the application
manager (`GApplication`), and provides the basic application bundle
project scaffolding.

<dt>

Hardkeys (special keys) handling

<dd>

Demonstrates how to listen for events triggered by standard and special
keys like arrows and back/forward keys and react to them.

<dt>

Preferences system (GSettings) usage

<dd>

A simple applications that switches its background color on click and it
is able to restore the last selected color when restarted by storing it
as an application-specific setting.

<dt>

Showing / dismissing notifications

<dd>

Triggers a notification when the button is clicked, automatically
dismissing it after two seconds.

<dt>

Persistent data handling

<dd>

Save the number of times the button gets pressed in a persistent data
file, using the `GKeyFile` API

<dt>

Shipping a graphical program with a separate background agent

<dd>

Demonstrate how to split an application in a graphical program
implementing the user-facing part of the bundle and a separate agent
running in the background which is controlled by the graphical program
using D-Bus methods and properties.

<dt>

Using C++ and embed existing external libraries

<dd>

This fairly advanced sample applications demonstrates how to build an
application using C++ instead of pure C using the same SDK API, and also
how to embed a private copy of an existing, complex, and
externally-maintained C++ library such as OpenCV which uses a different
build system (CMake instead of the GNU Autotools).

<dt>

Writing applications using HTML/JS/CSS with the web runtime

<dd>

An application written without any C or C++ code, relying exclusively on
the web runtime shipped in the latest version of Apertis to implement
applications using only the standard HTML/JS/CSS web technologies and
the GObject-Introspection bindings to access the native SDK APIs from
JavaScript code

</dl>

## Groundwork

### Web runtime JS bindings

Seed has been chosen to provide the GObject-Introspection bindings
needed to access the native SDK API provided by Apertis from the
JavaScript code of web applications running in the Apertis Web runtime,
which uses WebKitGTK+ to display the HTML/CSS contents and the JSC
JavaScript engine.

### WebKit2GTK+ performance on Wayland

The work on [hardware-accelerated
compositing](https://blog.kov.eti.br/2016/09/webkitgtk-2-14-and-the-web-engines-hackfest/)
that landed in the previous release has been further improved by
noticeably [speeding up how GTK+ paints its
windows](https://blog.kov.eti.br/2016/11/a-tale-of-cylinders-and-shadows/),
benefiting any application using GTK+ and not only WebKit2GTK+.

### Multiple mockable backends in geocode-glib

This [upstream
effort](https://bugzilla.gnome.org/show_bug.cgi?id=756311) which has not
landed yet in 16.12 aims at improving the
[testability](https://bugzilla.gnome.org/show_bug.cgi?id=772928) of
`geocode-glib` and also let it use different backend implementations.

### Lightwood2

A new version of the Lightwood abstract widget set API is currently
under review.

### Traprain route guidance API

Traprain gained a new API allowing navigation services to send
turn-by-turn notifications and progress information about the current
journey accordingly to the [Geolocation and navigation
document](https://docs.apertis.org/latest/geolocation-and-navigation.html#navigation-route-guidance-api).

## Design

### Audio management

A first version of the [Audio management concept
document](https://docs.apertis.org/latest/audio-management.html) has
been published, describing how streams and their priorities should be
managed in a Apertis system using PulseAudio.

### List widgets

The [list concept document](https://docs.apertis.org/latest/list.html)
has been further clarified and it is now available for public
consumption, providing the architecture for modern, model-based advanced
list widgets, based on the experience gathered working with the roller
widget.

### Speech-recognition

The Speech recognition document describes how an Apertis system can be
operated using speech commands, without physically interacting with
hardware buttons or touch screens.

It defines an uniform and consistent user interface, using three
different types of user interaction: command and control, data selection
and dictation.

### Routing preferences handling in `nav:` URIs

The [Geolocation and navigation
document](https://docs.apertis.org/latest/geolocation-and-navigation.html)
has seen the addition of routing-specific parameters in `nav:` URIs,
allowing applications to ask the navigation engine to plan routes for
specific kinds of vehicles, avoiding tolls and motorways, or trying to
pick the shortest or fastest route.

### Web runtime

New provisions have been added to the [Web runtime
specification](https://docs.apertis.org/latest/web-runtime.html) to
provide more details about the requirements that any compatible
implementation has to provide to be able to transparently run
applications that target the Apertis web runtime.

### Web portal caching

A [document about caching in WebKitGTk+ and its usage in specific
use-cases](https://docs.apertis.org/latest/web-portal-caching.html) has
been published, describing how a offline-first web portal could be
implemented using web technologies available in Apertis.

### Application layout

With the release of the [stable bundle
specification](https://appdev.apertis.org/documentation/bundle-spec.html)
the [Application layout
document](https://docs.apertis.org/latest/application-layout.html) has
been updated to point to it and to provide more clarifications.

## Deprecations and ABI/API breaks

<dl>

<dt>

T2789: Add namespace to SampleItem of mildenhall

<dd>

The `SampleItem` class and related symbols have been renamed to
`MildenhallSampleItem` to properly namespace them.

<dt>

T3031: mildenhall should keep the naming convention rules

<dd>

Applications should `#include <mildenhall/mildenhall.h>` as direct
inclusion of other Mildenhall headers is now deprecated.
`initialize_ui_toolkit()` has been deprecated, simply use
`clutter_init()` instead.
`create_mildenhall_window()` has been deprecated, use
`mildenhall_stage_new()` instead.

<dt>

T2640: Generate a single library for all the widgets

<dd>

The `SONAME` of the `liblightwood` widget library has changed since all
the widgets have been moved in a single library to reduce the linking
overhead. This means that applications linking to the
`liblightwoodbutton.so.0` and related libraries will need to be rebuilt
to link to the new `liblightwood-0.so.0`. No source code changes should
be required, the new setup should be automatically be picked up by just
rebuilding the existing code.

<dt>

T2707: Canterbury: remove support for GSettings-schema-based entry
points

<dd>

The old entry points discovery mechanism based on GSettings schemas is
still available but will be removed immediately after the release and
won't be available in 17.03. Use `.desktop` files to describe [entry
points in application
bundles](https://appdev.apertis.org/documentation/bundle-spec.html#entry-points).

</dl>

## Infrastructure

### Apertis infrastructure tools

For Debian Jessie based systems:

` deb `<https://repositories.apertis.org/debian/>` jessie tools`

For Ubuntu Trusty based systems:

` deb `<https://repositories.apertis.org/ubuntu/>` trusty tools`

### Images

Image daily builds, as well as release builds can be found at:

` `<https://images.apertis.org/>

Image build tools can be found in the Apertis tools repositories.

### Test Framework

The list of available test cases, including manual and automated, can be
found [here]( {{< ref "/qa/test_cases" >}} ).

LAVA service packages are available in the Apertis tools repository. To
install, please follow
[instructions](https://lava.collabora.co.uk/static/docs/installing_on_debian.html#debian-installation).

## Known issues

  - \- Mildenhall compositor crops window

  - \- Tracker issue when creating thumbnails on SDK and ARM

  - \- Issue with cgroup tests on SDK

  - \- Cannot update sysroot in Eclipse on SDK

  - \- Kinetic scrolling is not smooth

  - \- Bluetooth pairing fails on ARM

  - \- Apparmor lockdown session profile does not work as expected

  - \- PulseAudio AppArmor profile doesn't work as expected on ARM

  - \- Contact linking does not work on devel image

  - \- Issue with LibreOffice AppArmor profile

  - \- Issue with JavaScript engine

  - \- Factory reset does not work

  - \- Target doesn't reboot after system update
