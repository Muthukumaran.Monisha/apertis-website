+++
date = "2020-09-28"
weight = 100

title = "V2021dev3 Release Notes"
+++

Apertis is a Debian derivative distribution geared towards the creation
of product-specific images for ARM (both the 32bit ARMv7 and 64-bit
ARMv8 versions using the hardfloat ABI) and Intel x86-64 (64-bit)
systems.

**Apertis v2021dev3** is the fourth and last **development** release of the
Apertis v2021 stable release flow that will lead to the LTS **Apertis
v2021.0** release in March 2021.

This Apertis release is built on top of Debian Buster with several
customisations.

Test results for the v2021dev3 release are available in the following
test reports:

  - [APT images](https://lavaphabbridge.apertis.org/report/v2021dev3/20200624.0118)
  - [OSTree images](https://lavaphabbridge.apertis.org/report/v2021dev3/20200624.0118/ostree)
  - [NFS artifacts](https://lavaphabbridge.apertis.org/report/v2021dev3/20200624.0117/nfs)
  - [LXC containers](https://lavaphabbridge.apertis.org/report/v2021dev3/20200624.0/lxc)

## Release flow

  - 2019 Q4: v2021dev0
  - 2020 Q1: v2021dev1
  - 2020 Q2: v2021dev2
  - **2020 Q3: v2021dev3**
  - 2020 Q4: v2021pre
  - 2021 Q1: v2021.0
  - 2021 Q2: v2021.1
  - 2021 Q3: v2021.2
  - 2021 Q4: v2021.3
  - 2022 Q1: v2021.4
  - 2022 Q2: v2021.5
  - 2022 Q3: v2021.6
  - 2022 Q4: v2021.7

### Release downloads

| [Apertis v2021dev3 images](https://images.apertis.org/release/v2021dev3/) | | | | |
| ------------------------------------------------------------------------- |-|-|-|-|
| Intel 64-bit		| [minimal](https://images.apertis.org/release/v2021dev3/v2021dev3.0/amd64/minimal/apertis_v2021dev3-minimal-amd64-uefi_v2021dev3.0.img.gz) | [target](https://images.apertis.org/release/v2021dev3/v2021dev3.0/amd64/target/apertis_v2021dev3-target-amd64-uefi_v2021dev3.0.img.gz) | [base SDK](https://images.apertis.org/release/v2021dev3/v2021dev3.0/amd64/basesdk/apertis_v2021dev3-basesdk-amd64-sdk_v2021dev3.0.vdi.gz) | [SDK](https://images.apertis.org/release/v2021dev3/v2021dev3.0/amd64/sdk/apertis_v2021dev3-sdk-amd64-sdk_v2021dev3.0.vdi.gz)
| ARM 32-bit (U-Boot)	| [minimal](https://images.apertis.org/release/v2021dev3/v2021dev3.0/armhf/minimal/apertis_v2021dev3-minimal-armhf-uboot_v2021dev3.0.img.gz) | [target](https://images.apertis.org/release/v2021dev3/v2021dev3.0/armhf/target/apertis_v2021dev3-target-armhf-uboot_v2021dev3.0.img.gz)
| ARM 64-bit (U-Boot)	| [minimal](https://images.apertis.org/release/v2021dev3/v2021dev3.0/arm64/minimal/apertis_v2021dev3-minimal-arm64-uboot_v2021dev3.0.img.gz)

The Intel `minimal` and `target` images are tested on the
[reference hardware (MinnowBoard Turbot Dual-Core)]( {{< ref "/reference_hardware/amd64.md" >}} ),
but they can run on any UEFI-based x86-64 system. The `sdk` image is
[tested under VirtualBox]( {{< ref "/virtualbox.md" >}} ).

#### Apertis v2021dev3 repositories

    deb https://repositories.apertis.org/apertis/ v2021dev3 target development sdk hmi

## New features

### Technology preview: 64-bit Raspberry Pi boards support

Starting from v2021dev3 Apertis now ships
[64-bit ARM images for version 3 and 4 of the Raspberry Pi boards](https://gitlab.apertis.org/infrastructure/apertis-image-recipes/-/merge_requests/271).

This provides an inexpensive and globally available ARM platform that
developers around the world can use to test Apertis on real ARM hardware.

The new `rpi64` images support resilient updates and rollbacks via OSTree and
U-Boot like the other Apertis ARM platforms.

Unlike the other fully supported Apertis platforms, the `rpi64` images are not
yet part of the daily automated test runs on LAVA. Apertis plans to add the
needed devices to the LAVA lab in the coming months to ensure the images are
regularly tested and regressions are identified quickly.

### Upstreaming of signed metadata verification for offline updates

The version of OSTree in Apertis ships some downstream changes to protect
against malicious static bundle superblocks as used for offline updates.

Work is ongoing to
[land the feature upstream](https://github.com/ostreedev/ostree/pull/1985)
to be available to every OSTree user.

### LXC image recipes moved to GitLab CI/CD

Another pipeline moved from Jenkins to GitLab CI/CD: the `tiny` image recipes
for LXC are now
[built on GitLab](https://gitlab.apertis.org/infrastructure/tiny-image-recipes/-/pipelines).

Just like the main image recipes, the pipeline is integrated in the merge
requests workflow and it gets executed to catch issues before they get
committed to the main branch.

### Mildenhall flatpak apps

Based on the work done in the previous cycle, most
[Mildenhall sample applications](https://gitlab.apertis.org/sample-applications)
have been ported to the
[Mildenhall flatdeb pipeline](https://gitlab.apertis.org/infrastructure/apertis-flatdeb-mildenhall)
and now produce Flatpak bundles accordingly to the design document
describing the new [Application Framework]( {{< ref "application-framework.md" >}} ).

### Moved both Renesas R-Car and SABRE Lite to newer U-Boot

The Raspberry Pi support required the introduction of a
[new U-Boot version](https://gitlab.apertis.org/pkg/target/u-boot/-/merge_requests/25)
from Debian Bullseye, version `2020.04`.

The Apertis team took this chance to also streamline the support for the other
ARM boards and now all the Apertis ARM platforms
([i.MX6 SABRE Lite](https://www.apertis.org/reference_hardware/arm32/),
[Renesas R-Car](https://www.apertis.org/reference_hardware/arm64/) and the
newly introduced Raspberry Pi 3 and 4) now share the same U-Boot source package.

This is a fundamental step toward enabling transactional updates with rollbacks
with the
[Apertis update manager](https://gitlab.apertis.org/pkg/target/apertis-update-manager/)
on all the reference platforms.

### Rhosydd VISS

[Rhosydd](https://gitlab.apertis.org/pkg/target/rhosydd/), the vehicle device
daemon implementing the
[sensors and actuators API]( {{< ref "sensors-and-actuators.md" >}} )
has been
[rebased to the data model](https://gitlab.apertis.org/pkg/target/rhosydd/-/merge_requests/16)
from the latest W3C specification, as
[described in the design document]( {{< ref "designs/sensors-and-actuators.md#sensors-and-actuators-api" >}} ).

Check the [Vehicle Signal specification](https://github.com/GENIVI/vehicle_signal_specification)
for more information about the data model that replaces the
[old Vehicle Data specification](https://www.w3.org/2014/automotive/data_spec.html).

## Build and integration

### Production deployment of emulated OBS package builds on Intel x86-64

The plan described in the concept document to host the whole
[build infrastructure on Intel x86-64]( {{< ref "x86-build-infrastructure.md" >}} )
has moved from the
[proof-of-concept stage]( {{< ref "release/v2021dev2/releasenotes.md#emulated-obs-package-builds-on-intel-x86-64-proof-of-concept" >}} )
to the production stage, with ARM 32 and 64 bit OBS workers being hosted on
standard Intel x86-64 cloud machines.

### Dedicated flatdeb-builder Docker image

A new
[`flatdeb-builder` Docker image](https://gitlab.apertis.org/infrastructure/apertis-docker-images/-/merge_requests/124)
is now available to streamline the process of building Flatpak bundles with Flatdeb.

The new image is also able to run on places without KVM support, like cloud
virtual machines not supporting nested virtualization.

To use it, fetch it with:

    docker pull registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2021dev3-flatdeb-builder

## Documentation and designs

### Website reorganization

We continue to focus on improving the validity, accessibility and organisation of the Apertis documentation.

The contents of the designs.apertis.org website has been migrated to the main site and the required infrastructure put in place to redirect accesses to the new document locations. Work is underway to evaluate and categorise these documents into the main sites structure.

This change makes it possible to search more of the Apertis documents from one place.

## QA

### Full coverage of the packaging flow in the project-wide dashboard

The [dashboard](https://infrastructure.pages.apertis.org/dashboard/) has been
heavily extended to test the integrity of the whole packaging setup, and it now:

* ensures that updates are landed appropriately
* checks that versions on GitLab, OBS and in the APT repositories all match
* all the dependencies can be satisfied and package can be installed

### Multi-release support on qa.apertis.org

The [website hosting all the Apertis testcase definitions](https://qa.apertis.org)
now properly renders all the testcases for each release channel:

* [v2019](https://qa.apertis.org/v2019/)
* [v2020](https://qa.apertis.org/v2020/)
* [v2021dev3](https://qa.apertis.org/v2021dev3/)

It's now easier for testers to check the right test definition for each
specific image.

## Other enhancements

### Improved ubihealthd shutdown performance

Requested by a product team to improve the responsiveness of shutdown of
`ubihealthd` on NAND devices, a patch that was proposed upstream but didn't get
landed has been cleaned up and
[is now shipped by Apertis](https://gitlab.apertis.org/pkg/target/mtd-utils/-/merge_requests/2).

The patch splits up updating of PEB statistics and adds checks for the shutdown
status through the main loop, reducing the shutdown time noticeably.

### HTTP fixes in APT

After experiencing recurring corruption errors when downloading packages, a bunch of patches
[fixing some nasty HTTP issues](https://gitlab.apertis.org/pkg/target/apt/-/merge_requests/14)
have been discussed with the APT upstream maintainers and have been backported
to the version of APT shipped by Apertis.

### Clean shutdown with systemd/journald

An harmless but annoying message was printed in the logs during device shutdown:

    [FAILED] Failed unmounting /var

This was due to the need of keeping the journal daemon available for logging
until the end of shutdown.

Since shutting down the system without failures is a highly desirable trait in
embedded products using systemd, the
[upstream fix for the issue](https://github.com/systemd/systemd/issues/867) has been
[backported to the version shipped by Apertis](https://gitlab.apertis.org/pkg/target/systemd/-/merge_requests/26)
after a substantial rework to adapt to the older systemd release.

### Automated testing of the packaging CI pipeline

The GitLab CI/CD pipeline handling the packaging workflow is one of the most
used features in Apertis.

Unfortunately, it is also moderately complex as it needs to handle a few
different scenarios and testing it was a long and fragile process involving
multiple GitLab projects.

To make working on the pipeline faster and safer a
[new test infrastructure](https://gitlab.apertis.org/infrastructure/ci-package-builder/-/merge_requests/75)
has been implemented: every time a patch is submitted a separate pipeline is
instantiated on a throwaway packaging project, exercising the changed workflow by
emulating the actions of a developer and by monitoring the behaviour of the
packaging pipeline itself.

Developers can now feel much more confident when proposing new changes to the
packaging pipeline.

## Deprecations and ABI/API breaks

### Deprecations

During this release cycle we have continued to mark obsolete or
problematic APIs with the
[ABI break](https://phabricator.apertis.org/tag/abi_break/) tag as a way to
clear technical debt in future.

### Breaks

#### Image builds now happen on GitLab-CI

The
[GitLab-CI pipeline](https://gitlab.apertis.org/infrastructure/apertis-image-recipes/pipelines)
is now producing the reference images, and the old Jenkins pipeline has been
dismissed. The same applies for the
[LXC containers pipeline](https://gitlab.apertis.org/infrastructure/tiny-image-recipes/).

#### OSTree signing API changes

During the upstreaming process the new OSTree signing API that Apertis shipped
in the previous cycle as a preview some API changes have been requested.

The version of OSTree in v2021dev3 now reflects the upstream API and users of
the new signing API will need to be adjusted.

## Infrastructure

### Apertis Docker images

The Apertis Docker images provide a unified and easily reproducible build
environment for developers and services.

As of today, this includes the
[`apertis-base`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2021dev3-base),
[`apertis-image-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2021dev3-image-builder),
[`apertis-package-source-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-package-sources/v2021dev3-package-source-builder),
[`apertis-flatdeb-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-flatdebs/v2021dev3-flatdeb-builder),
[`apertis-documentation-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-documentations/v2021dev3-documentation-builder),
and [`apertis-testcases-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-testcasess/v2021dev3-testcases-builder),
Docker images.

### Apertis infrastructure tools

The [Apertis v2021 infrastructure
repository](https://build.collabora.co.uk/project/users/apertis:infrastructure:v2021)
provides packages for the required versions of `ostree-push` and
`ostree` for Debian Buster:

    deb https://repositories.apertis.org/infrastructure-v2021/ buster infrastructure

### Images

Image daily builds, as well as release builds can be found at https://images.apertis.org/

Image build tools can be found in the Apertis tools repositories.

### Infrastructure overview

The
[Image build infrastructure document]( {{< ref "image-build-infrastructure.md" >}} )
provides an overview of the image building process and the involved
services.

## Known issues

### High (10)
 - [T7056](https://phabricator.apertis.org/T7056)	FAILED: Error running command: ['ssh-copy-id', 'user@127.0.0.1' errors are shown when running the sdk-persistent testcases
 - [T7139](https://phabricator.apertis.org/T7139)	Incomplete description of the contribution process into Apertis
 - [T7237](https://phabricator.apertis.org/T7237)	sdk-persistent-disk-sysroot test fails
 - [T7238](https://phabricator.apertis.org/T7238)	sdk persistent memory tests fail
 - [T7258](https://phabricator.apertis.org/T7258)	Debos recipe fails for flat-deb in gitlab ci on BDC for v2021dev2
 - [T7296](https://phabricator.apertis.org/T7296)	Clear memory in MRC per MOR request Start, Please wait for some minutes... on bootup
 - [T7331](https://phabricator.apertis.org/T7331)	qa-report: The image version in task created due to failing LXC tests in LAVA is misreported
 - [T7405](https://phabricator.apertis.org/T7405)	AUM stuck with loop0 during apertis-update-manager-usb-unplug test on ARM32 image
 - [T7411](https://phabricator.apertis.org/T7411)	Full Screen does not come up when using the latest Guest Additions for the SDK
 - [T7414](https://phabricator.apertis.org/T7414)	glibc build fails on OBS

### Normal (106)
 - [T2853](https://phabricator.apertis.org/T2853)	GStreamer playbin prioritises imxeglvivsink over clutterautovideosink
 - [T2896](https://phabricator.apertis.org/T2896)	Crash when initialising egl on ARM target
 - [T2930](https://phabricator.apertis.org/T2930)	Develop test case for out of screen events in Wayland images
 - [T3121](https://phabricator.apertis.org/T3121)	Test apps are failing in Liblightwood with the use of GTest
 - [T3210](https://phabricator.apertis.org/T3210)	Fix Tracker testcase to not download media files from random HTTP user folders
 - [T3217](https://phabricator.apertis.org/T3217)	VirtualBox display freezes when creating multiple notifications at once and interacting (hover and click) with them
 - [T3233](https://phabricator.apertis.org/T3233)	Ribchester: deadlock when calling RemoveApp() right after RollBack()
 - [T3321](https://phabricator.apertis.org/T3321)	libgles2-vivante-dev is not installable
 - [T3920](https://phabricator.apertis.org/T3920)	arm-linux-gnueabihf-pkg-config does not work with sysroots installed by `ade`
 - [T3970](https://phabricator.apertis.org/T3970)	Ensure that the arch:all packages in the archive match the arch-specific versions
 - [T4092](https://phabricator.apertis.org/T4092)	Containers fail to load on Gen4 host
 - [T4293](https://phabricator.apertis.org/T4293)	Preseed action is needed for Debos
 - [T4307](https://phabricator.apertis.org/T4307)	ribchester-core causes apparmor denies on non-btrfs minimal image
 - [T4422](https://phabricator.apertis.org/T4422)	do-branching fails at a late stage cloning OBS binary repos
 - [T4444](https://phabricator.apertis.org/T4444)	A 2-3 second lag between the speakers is observed when a hfp connection is made over bluetooth
 - [T4568](https://phabricator.apertis.org/T4568)	Ribchester mount unit depends on Btrfs
 - [T4660](https://phabricator.apertis.org/T4660)	Eclipse Build is not working for HelloWorld App
 - [T4693](https://phabricator.apertis.org/T4693)	Not able to create namespace for AppArmor container on the internal mx6qsabrelite images with proprietary kernel
 - [T5468](https://phabricator.apertis.org/T5468)	build-snapshot: allow to build packages without `autogen.sh` script
 - [T5487](https://phabricator.apertis.org/T5487)	Wi-Fi search button is missing in wifi application
 - [T5611](https://phabricator.apertis.org/T5611)	`kbd` package has broken/problematic dependencies
 - [T5747](https://phabricator.apertis.org/T5747)	The /boot mountpoint is not empty
 - [T5748](https://phabricator.apertis.org/T5748)	System users are shipped in /usr/etc/passwd instead of /lib/passwd
 - [T5837](https://phabricator.apertis.org/T5837)	apparmor-utils: apparmor-utils test failed
 - [T5852](https://phabricator.apertis.org/T5852)	Terminal comes up inside the Launcher
 - [T5861](https://phabricator.apertis.org/T5861)	dbus-installed-tests: trying to overwrite mktemp.1.gz
 - [T5863](https://phabricator.apertis.org/T5863)	Songs/Videos don't play on i.MX6 with Frampton on internal images
 - [T5896](https://phabricator.apertis.org/T5896)	sdk-dbus-tools-bustle testcase is failing
 - [T5897](https://phabricator.apertis.org/T5897)	apparmor-ofono test fails
 - [T5900](https://phabricator.apertis.org/T5900)	evolution-sync-bluetooth test fails
 - [T5901](https://phabricator.apertis.org/T5901)	eclipse-plugins-apertis-management package is missing
 - [T5906](https://phabricator.apertis.org/T5906)	Video does not stream in WebKit on the i.MX6 internal images
 - [T5929](https://phabricator.apertis.org/T5929)	connman-pan-tethering test fail
 - [T5930](https://phabricator.apertis.org/T5930)	connman-pan-network-access test fails
 - [T5931](https://phabricator.apertis.org/T5931)	connman-usb-tethering test fails
 - [T5935](https://phabricator.apertis.org/T5935)	libfolks-ofono25 package not found
 - [T5993](https://phabricator.apertis.org/T5993)	rhosydd: 8_rhosydd test failed
 - [T6001](https://phabricator.apertis.org/T6001)	eclipse-plugins-remote-debugging test fails
 - [T6008](https://phabricator.apertis.org/T6008)	The pacrunner package used for proxy autoconfiguration is not available
 - [T6012](https://phabricator.apertis.org/T6012)	webkit2gtk-event-handling-redesign test fails on the amd64 ostree images
 - [T6024](https://phabricator.apertis.org/T6024)	folks-inspect: command not found
 - [T6052](https://phabricator.apertis.org/T6052)	Multimedia playback is broken on the internal i.MX6 images (internal 3.14 ADIT kernel issue)
 - [T6077](https://phabricator.apertis.org/T6077)	youtube Videos are not playing on upstream webkit2GTK
 - [T6078](https://phabricator.apertis.org/T6078)	Page scroll is lagging in Minibrowser on upstream webkit2GTK
 - [T6111](https://phabricator.apertis.org/T6111)	traprain: 7_traprain test failed
 - [T6231](https://phabricator.apertis.org/T6231)	gitlab-to-obs: Handle packages changing component across releases
 - [T6243](https://phabricator.apertis.org/T6243)	AppArmor ubercache support is no longer enabled after 18.12
 - [T6277](https://phabricator.apertis.org/T6277)	ldconfig: Warning comes up when we do an apt-get upgrade on the i.MX6
 - [T6291](https://phabricator.apertis.org/T6291)	Generated lavaphabbridge error report email provides wrong link for full report link
 - [T6292](https://phabricator.apertis.org/T6292)	gettext-i18n: test failed
 - [T6296](https://phabricator.apertis.org/T6296)	gupnp-services: 11_gupnp-services test failed
 - [T6349](https://phabricator.apertis.org/T6349)	sdk-code-analysis-tools-splint: 3_sdk-code-analysis-tools-splint test failed
 - [T6366](https://phabricator.apertis.org/T6366)	sdk-cross-compilation: 10_sdk-cross-compilation test failed
 - [T6369](https://phabricator.apertis.org/T6369)	apparmor-gstreamer1-0: test failed
 - [T6444](https://phabricator.apertis.org/T6444)	aum-update-rollback-tests/arm64,amd64: Automatic power cut test should be reworked to reduce the speed of delta read
 - [T6446](https://phabricator.apertis.org/T6446)	aum-update-rollback-tests/amd64: DNS not available in LAVA tests after reboot
 - [T6614](https://phabricator.apertis.org/T6614)	aum-update-rollback-tests/armhf: Rollback situation is not reproduced on public armhf target and internal images
 - [T6620](https://phabricator.apertis.org/T6620)	Repeatedly plugging and unplugging a USB flash drive on i.MX6 (Sabrelite) results in USB failure
 - [T6662](https://phabricator.apertis.org/T6662)	SDK: command-not-found package is broken
 - [T6669](https://phabricator.apertis.org/T6669)	Stop building cross compilers tools and libraries for not supported platforms
 - [T6670](https://phabricator.apertis.org/T6670)	Remove or move git-mediawiki package from the development repo
 - [T6680](https://phabricator.apertis.org/T6680)	Drop mkdocs package from development repository
 - [T6681](https://phabricator.apertis.org/T6681)	Fix btrfs packages in the development repository
 - [T6682](https://phabricator.apertis.org/T6682)	Drop or fix installation for git-all package in development repository
 - [T6683](https://phabricator.apertis.org/T6683)	Drop or fix installation for gccbrig package in development repository
 - [T6684](https://phabricator.apertis.org/T6684)	Move dh-python package from target to development repository
 - [T6685](https://phabricator.apertis.org/T6685)	Fix gstreamer1.0-gl package in target repository
 - [T6686](https://phabricator.apertis.org/T6686)	Move kernel-wedge package from target to development repository
 - [T6687](https://phabricator.apertis.org/T6687)	Move kdump-tools package from target to development repository
 - [T6688](https://phabricator.apertis.org/T6688)	Fix libblockdev-btrfs2 in target repository
 - [T6689](https://phabricator.apertis.org/T6689)	Move skales package from target to development repository
 - [T6690](https://phabricator.apertis.org/T6690)	Fix mesa-vdpau-drivers package in target repository
 - [T6691](https://phabricator.apertis.org/T6691)	Fix mesa-va-drivers package in target repository
 - [T6692](https://phabricator.apertis.org/T6692)	Move makedumpfile package from target to development repository
 - [T6693](https://phabricator.apertis.org/T6693)	Move lsb-release package from target to development repository
 - [T6694](https://phabricator.apertis.org/T6694)	Fix firmware-linux package in target
 - [T6695](https://phabricator.apertis.org/T6695)	Remove remaining hotdoc dbgsym package from development repository
 - [T6727](https://phabricator.apertis.org/T6727)	FTBFS: Apertis v2020pre package build failures
 - [T6768](https://phabricator.apertis.org/T6768)	Fix the kernel command line generation in OSTRee for FIT image
 - [T6773](https://phabricator.apertis.org/T6773)	HAB testing: the unsigned image may pass validation in several circumstances
 - [T6783](https://phabricator.apertis.org/T6783)	Kernel  trace on armhf board with attached screen
 - [T6795](https://phabricator.apertis.org/T6795)	SabreLite failing to boot due to failing "to start udev Coldplug all Devices"
 - [T6806](https://phabricator.apertis.org/T6806)	HAB on SabreLite in open state accepts any signed kernel regardless of the signing key
 - [T6885](https://phabricator.apertis.org/T6885)	gitlab-rulez fails to set location of the gitlab-ci.yaml on first run
 - [T6887](https://phabricator.apertis.org/T6887)	ARM64 target does not reboot automatically
 - [T6891](https://phabricator.apertis.org/T6891)	apparmor-pulseaudio: 13_apparmor-pulseaudio test failed
 - [T6903](https://phabricator.apertis.org/T6903)	U-Boot boot counter is used for AMD64 & ARM64
 - [T6961](https://phabricator.apertis.org/T6961)	audio-backhandling feature fails
 - [T7000](https://phabricator.apertis.org/T7000)	DNS resolution does not work in Debos on some setups
 - [T7012](https://phabricator.apertis.org/T7012)	Apparmor Denied session logs keep popping up on the terminal while executing tests
 - [T7016](https://phabricator.apertis.org/T7016)	network proxy for browser application is not resolving on mildenhall-compositor
 - [T7018](https://phabricator.apertis.org/T7018)	Chaiwala-logo doesn't come up on boot up
 - [T7127](https://phabricator.apertis.org/T7127)	apparmor-functional-demo:  test failed
 - [T7128](https://phabricator.apertis.org/T7128)	apparmor-session-lockdown-no-deny
 - [T7129](https://phabricator.apertis.org/T7129)	apparmor-tumbler: test failed
 - [T7201](https://phabricator.apertis.org/T7201)	gitlab CI reporting of OBS build status fails under modest/heavy load
 - [T7203](https://phabricator.apertis.org/T7203)	Sometimes Connman is unable to establish a Wi-Fi connection on minnowboard
 - [T7207](https://phabricator.apertis.org/T7207)	PBAP Phonebook Access PSE profile fails from time to time
 - [T7223](https://phabricator.apertis.org/T7223)	didcot: test failed
 - [T7224](https://phabricator.apertis.org/T7224)	OBS logging job fails in Gitlab CI pipeline
 - [T7247](https://phabricator.apertis.org/T7247)	connman-new-supplicant: test failed
 - [T7308](https://phabricator.apertis.org/T7308)	sdk-vb-fullscreen testcase link needs to be changed for v2019
 - [T7333](https://phabricator.apertis.org/T7333)	apparmor-geoclue: test failed
 - [T7340](https://phabricator.apertis.org/T7340)	newport: test failed
 - [T7387](https://phabricator.apertis.org/T7387)	Chaiwala test package needs to be installed when executing the Connman Pan Tethering and Connman Pan Network access test
 - [T7401](https://phabricator.apertis.org/T7401)	systemd-boot does not initiate rollback after repeated failed boots

### Low (18)
 - [T1809](https://phabricator.apertis.org/T1809)	Upstream: linux-tools-generic should depend on lsb-release
 - [T1924](https://phabricator.apertis.org/T1924)	telepathy-ring: Review and fix SMS test
 - [T1964](https://phabricator.apertis.org/T1964)	Mildenhall compositor crops windows
 - [T2142](https://phabricator.apertis.org/T2142)	Power button appers to be disabled on target
 - [T2226](https://phabricator.apertis.org/T2226)	Network search pop-up isn't coming up in wi-fi settings
 - [T2367](https://phabricator.apertis.org/T2367)	Videos are hidden when Eye is launched
 - [T2483](https://phabricator.apertis.org/T2483)	Video doesn't play when toggling from full screen to detail view
 - [T2498](https://phabricator.apertis.org/T2498)	Simulator screen is not in center but left aligned
 - [T2704](https://phabricator.apertis.org/T2704)	The video player window is split into 2 frames in default view
 - [T3161](https://phabricator.apertis.org/T3161)	If 2 drawers are activated, the most recent one hides behind the older one, instead of coming on top of older one.
 - [T3537](https://phabricator.apertis.org/T3537)	cgroups-resource-control: test network-cgroup-prio-class failed
 - [T3759](https://phabricator.apertis.org/T3759)	Status bar is not getting updated with the current song/video being played
 - [T4166](https://phabricator.apertis.org/T4166)	On multiple re-entries from settings to eye the compositor hangs
 - [T4296](https://phabricator.apertis.org/T4296)	Segmentation fault is observed on closing the mildenhall-compositor
 - [T4490](https://phabricator.apertis.org/T4490)	webkit2gtk-drag-and-drop doesn't work with touch
 - [T6065](https://phabricator.apertis.org/T6065)	apt-get dist-upgrade fails on SDK
 - [T7204](https://phabricator.apertis.org/T7204)	Bluez pairing operation is failing from time to time
 - [T7225](https://phabricator.apertis.org/T7225)	webkit2gtk-ac-3d-rendering test fails

### Lowest (73)
 - [T789](https://phabricator.apertis.org/T789)	Remove unnecessary folks package dependencies for automated tests
 - [T1556](https://phabricator.apertis.org/T1556)	No connectivity Popup is not seen when the internet is disconnected.
 - [T1960](https://phabricator.apertis.org/T1960)	remove INSTALL, aclocal.m4 files from langtoft
 - [T2028](https://phabricator.apertis.org/T2028)	Documentation is not available from the main folder
 - [T2299](https://phabricator.apertis.org/T2299)	Clutter_text_set_text API redraws entire clutterstage
 - [T2317](https://phabricator.apertis.org/T2317)	libgrassmoor: executes tracker-control binary
 - [T2318](https://phabricator.apertis.org/T2318)	mildenhall-settings: does not generate localization files from source
 - [T2475](https://phabricator.apertis.org/T2475)	Theme ,any F node which is a child of an E node is not working for Apertis widgets.
 - [T2781](https://phabricator.apertis.org/T2781)	Horizontal scroll is not shown on GtkClutterLauncher
 - [T2785](https://phabricator.apertis.org/T2785)	The background HMI is blank on clicking the button for Power OFF
 - [T2788](https://phabricator.apertis.org/T2788)	Share links to facebook, twitter are nbt working in browser (GtkClutterLauncher)
 - [T2790](https://phabricator.apertis.org/T2790)	Background video is not played in some website with GtkClutterLauncher
 - [T2833](https://phabricator.apertis.org/T2833)	Interaction with PulseAudio not allowed by its AppArmor profile
 - [T2858](https://phabricator.apertis.org/T2858)	shapwick reads /etc/nsswitch.conf and /etc/passwd, and writes /var/root/.cache/dconf/
 - [T2889](https://phabricator.apertis.org/T2889)	Cannot open/view pdf documents in browser (GtkClutterLauncher)
 - [T2890](https://phabricator.apertis.org/T2890)	Zoom in feature does not work on google maps
 - [T2917](https://phabricator.apertis.org/T2917)	Images for the video links are not shown in news.google.com on GtkClutterLauncher
 - [T2995](https://phabricator.apertis.org/T2995)	Focus in launcher rollers broken because of copy/paste errors
 - [T3008](https://phabricator.apertis.org/T3008)	beep audio decoder gives errors continously
 - [T3171](https://phabricator.apertis.org/T3171)	Unusable header in Traprain section in Devhelp
 - [T3174](https://phabricator.apertis.org/T3174)	Clang package fails to install appropriate egg-info needed by hotdoc
 - [T3219](https://phabricator.apertis.org/T3219)	Canterbury messes up kerning when .desktop uses unicode chars
 - [T3237](https://phabricator.apertis.org/T3237)	make check fails on libbredon package for wayland warnings
 - [T3280](https://phabricator.apertis.org/T3280)	Cannot open links within website like yahoo.com
 - [T3319](https://phabricator.apertis.org/T3319)	mx6qsabrelite: linking issue with libgstimxeglvivsink.so and libgstimxvpu.so gstreamer plugins
 - [T3332](https://phabricator.apertis.org/T3332)	Compositor seems to hide the bottom menu of a webpage
 - [T3430](https://phabricator.apertis.org/T3430)	Spacing issues between text and selection box in website like amazon
 - [T3431](https://phabricator.apertis.org/T3431)	Content on a webpage doesn't load in sync with the scroll bar
 - [T3433](https://phabricator.apertis.org/T3433)	Resizing the window causes page corruption
 - [T3506](https://phabricator.apertis.org/T3506)	Confirm dialog status updated before selecting the confirm option YES/NO
 - [T3517](https://phabricator.apertis.org/T3517)	webview Y offset not considered to place, full screen video on youtube webpage
 - [T3563](https://phabricator.apertis.org/T3563)	GObject Generator link throws 404 error
 - [T3564](https://phabricator.apertis.org/T3564)	GLib, GIO Reference Manual links are incorrectly swapped
 - [T3580](https://phabricator.apertis.org/T3580)	Canterbury entry-point launching hides global popups, but only sometimes
 - [T3588](https://phabricator.apertis.org/T3588)	<abstractions/chaiwala-base> gives privileges that not every app-bundle should have
 - [T3631](https://phabricator.apertis.org/T3631)	Segmentation fault when disposing test executable of mildenhall
 - [T3647](https://phabricator.apertis.org/T3647)	The web runtime doesn't set the related view when opening new windows
 - [T3729](https://phabricator.apertis.org/T3729)	ribchester: gnome-desktop-testing test times out
 - [T3730](https://phabricator.apertis.org/T3730)	canterbury: Most of the tests fail
 - [T3763](https://phabricator.apertis.org/T3763)	Compositor hides the other screens
 - [T3771](https://phabricator.apertis.org/T3771)	Roller problem in settings application
 - [T3797](https://phabricator.apertis.org/T3797)	Variable roller is not working
 - [T3798](https://phabricator.apertis.org/T3798)	In mildenhall, URL history speller implementation is incomplete.
 - [T3909](https://phabricator.apertis.org/T3909)	MildenhallSelectionPopupItem doesn't take ownership when set properties
 - [T3939](https://phabricator.apertis.org/T3939)	libshoreham packaging bugs
 - [T3940](https://phabricator.apertis.org/T3940)	libmildenhall-0-0 contains files that would conflict with a future libmildenhall-0-1
 - [T3969](https://phabricator.apertis.org/T3969)	MildenhallSelPopupItem model should be changed to accept only gchar * instead of MildenhallSelPopupItemIconDetail for icons
 - [T3971](https://phabricator.apertis.org/T3971)	libbredon/seed uninstallable on target as they depend on libraries in :development
 - [T3972](https://phabricator.apertis.org/T3972)	webview-test should be shipped in libbredon-0-tests instead of libbredon-0-1
 - [T3973](https://phabricator.apertis.org/T3973)	bredon-0-launcher should be shipped in its own package, not in libbredon-0-1
 - [T3991](https://phabricator.apertis.org/T3991)	virtual keyboard is not showing for password input field of any webpage
 - [T3992](https://phabricator.apertis.org/T3992)	Steps like pattern is seen in the background in songs application
 - [T3996](https://phabricator.apertis.org/T3996)	Avoid unconstrained dbus AppArmor rules in frome
 - [T4005](https://phabricator.apertis.org/T4005)	Newport test fails on minimal images
 - [T4009](https://phabricator.apertis.org/T4009)	connman: patch "Use ProtectSystem=true" rejected upstream
 - [T4010](https://phabricator.apertis.org/T4010)	connman: patch "device: Don't report EALREADY" not accepted upstream
 - [T4027](https://phabricator.apertis.org/T4027)	webkit2GTK crash observed flicking on webview from other widget
 - [T4031](https://phabricator.apertis.org/T4031)	Mildenhall should install themes in the standard xdg data dirs
 - [T4046](https://phabricator.apertis.org/T4046)	Page rendering is not smooth in sites like www.yahoo.com
 - [T4048](https://phabricator.apertis.org/T4048)	HTML5 demo video's appear flipped when played on webkit2 based browser app
 - [T4050](https://phabricator.apertis.org/T4050)	Render theme buttons are not updating with respect to different zoom levels
 - [T4052](https://phabricator.apertis.org/T4052)	Rendering issue observed on websites like http://www.moneycontrol.com
 - [T4089](https://phabricator.apertis.org/T4089)	Crash observed on webruntime framework
 - [T4110](https://phabricator.apertis.org/T4110)	Crash observed on seed module when we accesing the D-Bus call method
 - [T4142](https://phabricator.apertis.org/T4142)	introspectable support for GObject property, signal and methods
 - [T4348](https://phabricator.apertis.org/T4348)	Inital Roller mappings are misaligned on the HMI
 - [T4383](https://phabricator.apertis.org/T4383)	folks-metacontacts-antilinking: folks-metacontacts-antilinking_sh.service failed
 - [T4386](https://phabricator.apertis.org/T4386)	apparmor-tracker: AssertionError: False is not true
 - [T4419](https://phabricator.apertis.org/T4419)	traprain: sadt: error: cannot find debian/tests/control
 - [T4420](https://phabricator.apertis.org/T4420)	canterbury: core-as-root and full-as-root tests failed
 - [T4421](https://phabricator.apertis.org/T4421)	ribchester: Job for generated-test-case-ribchester.service canceled
 - [T5301](https://phabricator.apertis.org/T5301)	libsoup: /usr/lib/libsoup2.4/installed-tests/libsoup/ssl-test test failed
 - [T5989](https://phabricator.apertis.org/T5989)	frome: 6_frome test failed
