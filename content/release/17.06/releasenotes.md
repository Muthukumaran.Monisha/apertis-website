+++
date = "2017-06-29"
weight = 100

title = "17.06 ReleaseNotes"

aliases = [
    "/old-wiki/17.06/ReleaseNotes"
]
+++

# Apertis 17.06 Release

**17.06** is the current stable development release of **Apertis**, a
Debian/Ubuntu derivative distribution geared towards the creation of
product-specific images for ARM (both in the 32bit ARMv7 version using
the hardfloat ABI and in the 64-bit ARMv8 version) and Intel x86-64
(64-bit) systems.

This Apertis release is based on top of the Ubuntu 16.04 (Xenial) LTS
release with several customization. Test results of the 17.06 release
are available in the [17.06 test report]().

### Release downloads

| [Apertis 17.06 images](https://images.apertis.org/release/17.06/) |
| ----------------------------------------------------------------- |
| Intel 64-bit                                                      |

The `minimal`, `target` and `development` images are tested on the
[reference hardware (MinnowBoard MAX)]( {{< ref "/reference_hardware/_index.md" >}} ),
but they can run on any UEFI-based x86-64 system. The `sdk` image is
[tested under VirtualBox]( {{< ref "/virtualbox.md" >}} ).

#### Apertis 17.06 repositories

` $ deb `<https://repositories.apertis.org/apertis/>` 17.06 target helper-libs development sdk hmi`

## New features

### Application framework for headless setups

The current application framework provides several hooks for full
graphical environments that are not needed on systems not connected to a
display. In this cycle leaner, simpler versions of the Canterbury
session manager and of the Ribchester installation manager have been
factored out of their respective codebases, to address the headless
use-case in the most efficient and flexible way.

As a by-product of this effort, even the full versions of Canterbury and
Ribchester saw a great number of cleanups and fixes.

### Apertis running in a container

The work to be able to run Apertis in a Linux container has begun in
this cycle, albeit it has not been fully landed in the release. Apertis
can now be set up to [run under LXC with some simple
instructions]( ), using nested AppArmor profiles to
constrain app bundles and a global AppArmor profile to constrain the
whole container. The work to free the application framework from the
dependency on Btrfs subvolumes which make it not well suited to be run
in a container has been completed just after the freeze for the 17.06
release, so it will be part of the 17.09 release. Work will continue on
full system upgrades in container setups and the interactions of
AppArmor with user namespaces.

### Sample app-bundles

Three new application bundles have been added to demonstrate [how to use
simple web APIs over
HTTPS](https://git.apertis.org/cgit/sample-applications/helloworld-https-client.git/),
[how to use platform
services](https://git.apertis.org/cgit/sample-applications/helloworld-newport.git/)
like [Newport](https://git.apertis.org/cgit/newport.git/) and [how to
make app-bundles shipping an agent with no
UI](https://git.apertis.org/cgit/sample-applications/helloworld-simple-agent.git/).

The first example application connects to the
[ifconfig.co](https://ifconfig.co/) web service to show the current
public IP websites will see when you connect to them, plus some
information about the country and city associated with that IP.

The second example uses the [Newport D-Bus
API](https://docs.apertis.org/newport/dbus-index.html) to download two
images from the Internet and continuosly cycle between them.

The third example demonstrates how to define an agent that implements a
simple D-Bus API, plus a test client to exercise such API.

The SDK images now also ship repositories for all the sample app bundles
with all their code, to let you start hacking immediately.

## Groundwork

### AppArmor in full enforcing mode on headless images

AppArmor protection is now completely enforced on the [minimal images
for headless
setups]( {{< ref "/release/17.03/releasenotes.md#headless-images-with-the-application-framework" >}} ).
Several AppArmor profiles and abstractions have been documented, cleaned
up, and fixed to reach this goal.

Some of the older and less useful abstractions have been deprecated, see
the [section below](#deprecations-and-abi/api-breaks).

### ARM64 support in development tools

An ARM64 cross-compiler is now available in the Apertis 17.06 SDK
images, ready to be used with the [Apertis Development
Tool]( {{< ref "ade.md" >}} ) and
the pre-generated ARM64 sysroots published on the Apertis web server,
enabling full app-bundle development for ARM64 targets from the Apertis
SDK.

## Design

### Inter-domain communications

The focus of this release has been put on topics affecting the
[inter-domain communication
design]( {{< ref "inter-domain-communication.md" >}} ).
In particular the design has been expanded to cover container-based
setups in addition to setups based on separate boards and virtual
machines, to provide more explicit provisions for domains not running
Linux or not running a `dbus-daemon`, to clearly describe how domains
can dynamically discover where services are hosted on setups with
multiple Consumer Electronics domains, and how audio and video streams
can be shared across the domain boundaries, in a effort to isolate the
privileged domains from the attack surface of demuxers and decoders.

## Deprecations and ABI/API breaks

### Deprecations

During this release cycle we have continued to mark obsolete or
problematic APIs with the [ABI
break](https://phabricator.apertis.org/tag/abi_break/) tag as a way to
clear technical debt in future.

All features deprecated during previous release cycles remain
deprecated. Some of them are repeated here for better visibility.

<dl>

<dt>

: Prestwood D-Bus APIs and `libprestwoodiface` are deprecated in favour
of GVolumeMonitor

<dd>

The Prestwood daemon uses UPnP and GVolumeMonitor to monitor UPnP shares
and removable volumes, and offers a D-Bus API for the removable volumes,
with a thin library wrapper in `libprestwoodiface`. This API is not
really simpler for an API user than if the API user used UPnP and
GVolumeMonitor directly, so we recommend removing these APIs.

<dt>

: Canterbury's old audio manager implementation remains deprecated

<dd>

The current AudioMgr interface breaks the app-bundle confidentiality
security requirement and must be changed incompatibly or replaced to
resolve that. It was deprecated in 17.03, remains deprecated, and will
be removed in a future Canterbury version.  and  track related work.

<dt>

Syntactically invalid entry point IDs remain deprecated in favour of
syntactically valid entry point IDs

<dd>

Canterbury still accepts entry point IDs in built-in app-bundles that do
not match the syntax in the app-bundle specification. This was
deprecated in 17.03, remains deprecated, and will be removed in a future
Canterbury version. This will require changing the current special-cases
for an app-bundle named exactly `Launcher` so that instead they apply to
an app-bundle named exactly `org.apertis.Launcher`, or removing those
special cases altogether.  is related.

<dt>

Syntactically invalid bundle IDs for built-in app-bundles remain
deprecated in favour of syntactically valid bundle IDs

<dd>

When loading built-in app-bundles, Canterbury still accepts bundle IDs
that do not match the syntax in the app-bundle specification. This was
deprecated in 17.03, remains deprecated, and will be removed in a future
Canterbury version. The function `cby_is_bundle_id()` can be used to
validate bundle IDs. The syntax rules are identical to D-Bus interface
names: see the [Apertis Application Bundle
Specification]( {{< ref "bundle-spec.md" >}} )
for details.  is related.

<dt>

: `Launcher.CurrentActiveMenuEntry` is deprecated with no replacement

<dd>

The `Launcher.CurrentActiveMenuEntry` property was scheduled to be
removed in 17.06, but could not be removed because the Mildenhall
Launcher still uses it. It remains deprecated and will be removed in a
future Canterbury version.

<dt>

: Mildenhall widgets not listed here are subject to change and
unsuitable for third-party applications

<dd>

The ABI for the widgets listed below is considered stable, other widgets
are to be considered obsoleted or otherwise subject to ABI breaks:

  - MildenhallBottomBar - set of buttons intended to carry some actions,
    for example, shuffle, play, pause, next,previous, display time, etc.
  - MildenhallButtonDrawer - combobox button
  - MildenhallButtonSpeller - speller button
  - MildenhallCabinetItem - item for MildenhallCabinetRoller
  - MildenhallCabinetRoller - file browser roller
  - MildenhallContextDrawer - menu bar
  - MildenhallDetailRoller - list item for the roller widget
  - MildenhallDrawerBase - base for drawers like MildenhallContextDrawer
    and MildenhallViewsDrawer
  - MildenhallInfoRoller - used to display metadata information of any
    given file
  - MildenhallLbsBottomBar - bottom bar for location-based applications,
    displays distances in km and meters
  - MildenhallLoadingBar - loading bar
  - MildenhallMapWidget - geographical map
  - MildenhallMediaOverlay - semi-transparent play/pause icon to be used
    in multimedia applications
  - MildenhallMetaInfoFooter - similar to MildenhallMetaInfoHeader but
    placed at bottom
  - MildenhallMetaInfoHeader - header with icon and text
  - MildenhallNaviDrawer - combobox button to navigate from one view to
    another
  - MildenhallNaviDrawer - specialized combobox for navigation
  - MildenhallOverlay - widget displaying a .png with a custom alpha
    value over the stage
  - MildenhallPopup - popup intended for confirmation, alert,
    information dialogs
  - MildenhallProgressBar - progress bar
  - MildenhallPullupRoller - like info roller but with upward animation
  - MildenhallRadioButton - radio button
  - MildenhallRatingBottomBar - rating bar
  - MildenhallRollerContainer - Container for list and roller widgets
  - MildenhallScroller - scrolling widget used in the virtual keyboard
  - MildenhallSelectionPopupItem - item for MildenhallSelectionPopup
  - MildenhallSelectionPopup - popup widget which can have multiple
    selection options
  - MildenhallSortRoller - roller widget that keeps items sorted
  - MildenhallSpeller - virtual keyboard
  - MildenhallSpellerMultiLineEntry, MildenhallSpellerThreeToggleEntry -
    support widgets for the virtual keyboard
  - MildenhallTextBoxEntry - text box widget
  - MildenhallToggleButton - toggle button
  - MildenhallViewsDrawer - combobox button
  - MildenhallWidgetContainer - container with horizaontal placing and
    some padding value.

</dl>

### Breaks

#### Application framework

<dl>

<dt>

Ribchester support for GSettings-schema-based entry points removed in
favour of AppStream XML and CbyComponentIndex

<dd>

Ribchester had code for mounting app-bundles that are represented by
GSettings schemas, which was deprecated in 17.03. This was no longer
useful after Canterbury stopped reading those GSettings schemas in 17.03
(). Use AppStream XML to describe application bundles, as described in
the [Apertis Application Bundle
Specification]( {{< ref "bundle-spec.md" >}} ).

<dt>

`/Applications/$bundle/{app-data,users}` compat symlinks removed in
favour of `cby_get_persistence_path()`

<dd>

Canterbury no longer creates the
`/var/Applications/${bundle_id}/app-data` directory, and the `app-data`
and `users` symlinks in the bundle installation directory in
`/Applications`.

Applications should use the `cby_get_persistence_path()` function to get
the per-bundle, per-user writable data location, or rely on the
freedesktop.org environment variables `XDG_CACHE_HOME`,
`XDG_CONFIG_HOME` and `XDG_DATA_HOME` respectively for temporary data,
configuration data and general data storage.

<dt>

: `app-name` and `menu-entry` in command-line arguments are no longer
special

<dd>

Canterbury previously had special handling for the command-line
arguments of certain ways to launch entry points. If an argument was
named exactly `app-name`, the next argument is replaced by the entry
point ID; if an argument was named exactly `menu-entry`, the next
argument is replaced by the display name in the form used by the
Mildenhall reference UX (in all-caps with S P A C E S I N S E R T E D).

This feature was not compatible with D-Bus activation, which is not
argv-based (), and was unnecessary since 17.03, because entry points are
now always launched with the same entry point ID and display name
(having removed the special cases that would have used different values
for those arguments in ). This rewriting behaviour was deprecated in
17.03 and removed in 17.06. Command-line arguments with those names are
no longer considered special.

<dt>

: Entry point IDs in store app-bundles must be syntactically valid

<dd>

Canterbury previously accepted entry point IDs in store app-bundles that
do not match the syntax in the app-bundle specification. This was
deprecated in 17.03 and removed in 17.06. The function
`cby_is_entry_point_id()` can be used to validate entry point IDs. The
syntax rules are identical to D-Bus interface names: see the [Apertis
Application Bundle
Specification]( {{< ref "bundle-spec.md" >}} )
for details.

<dt>

: Bundle IDs in store app-bundles must be syntactically valid

<dd>

In some circumstances, Canterbury previously accepted bundle IDs in
store app-bundles that do not match the syntax in the app-bundle
specification. This was deprecated in 17.03 and removed in 17.06. The
function `cby_is_bundle_id()` can be used to validate bundle IDs. The
syntax rules are identical to D-Bus interface names: see the [Apertis
Application Bundle
Specification]( {{< ref "bundle-spec.md" >}} )
for details.

<dt>

: `Launcher.SetCurrentCategory`, `Launcher.InformAppLaunchStatus`,
`AppManager.GetGlobalSearchApps` removed

<dd>

These unused APIs were deprecated in 17.03, and removed from Canterbury
in 17.06. `Launcher.CurrentActiveMenuEntry` was also scheduled for
deletion, but unfortunately is still used by the Mildenhall Launcher: it
remains deprecated.

<dt>

: `AppDbHandler.LaunchNewApp()` removed

<dd>

This method is redundant with the higher-level API
`cby_entry_point_open_uri_async()` introduced in 17.03, and its only
known user (the Didcot content handover service) has been converted to
use that mechanism. It was deprecated in 17.03 and removed in 17.06.

<dt>

Canterbury SONAME change

<dd>

The ABI version suffix of `libcanterbury-gdbus` was changed to reflect
the fact that ABIs were removed. Applications that use the remaining
D-Bus APIs must be recompiled against Canterbury 0.1706.x. The
`canterbury-0` and `canterbury-platform-0` libraries are unaffected.

</dl>

#### AppArmor

<dl>

<dt>

: `tunables/chaiwala/chaiwala-user` AppArmor tunables removed

<dd>

The `tunables/chaiwala/chaiwala-user` AppArmor profile snippet described
a filesystem layout that has not existed for some time. It was
deprecated in 17.03 and removed in 17.06.

<dt>

: `abstractions/chaiwala-helpers` AppArmor abstraction removed

<dd>

The `abstractions/chaiwala-helpers` AppArmor abstraction had undesirable
security properties. It was deprecated in 17.03 and removed in 17.06.

<dt>

: `@{XDGRUNTIMEDIR}` AppArmor variable removed in favour of
`/run/user/[0-9]*`

<dd>

Apertis' modified AppArmor tunables previously defined a variable named
`@{XDGRUNTIMEDIR}` which is not present in upstream AppArmor, hindering
portability of AppArmor profiles between distributions. This was
deprecated in 17.03 and removed in 17.06. Please replace all instances
of `@{XDGRUNTIMEDIR}` in profiles with `/run/user/[0-9]*`.

<dt>

: `chaiwala-apparmor-session-lockdown` package obsoleted

<dd>

The purpose of the `chaiwala-apparmor-session-lockdown` package was not
clear. It was deprecated in 17.03 and converted into an empty
transitional package in 17.06, and should be removed altogether in
17.09.

<dt>

: `abstractions/dbus-daemon` AppArmor abstraction removed

<dd>

The `abstractions/dbus-daemon` AppArmor abstraction did not appear to be
useful, because every dbus-daemon on Apertis is part of the trusted
computing base and part of the platform infrastructure, so a dbus-daemon
is not a useful thing for third-party code to be launching. It was
deprecated in 17.03 and removed in 17.06.

<dt>

: `abstractions/chaiwala-user-write` AppArmor abstraction removed

<dd>

The `abstractions/chaiwala-user-write` AppArmor abstraction was too
broad to be useful, and granted the confined process enough privileges
to escape from its confinement by executing arbitrary unconfined code.
It also did not appear to be used in Apertis. It was deprecated in 17.03
and removed in 17.06.

<dt>

: `abstractions/chaiwala-user-read` AppArmor abstraction removed

<dd>

The `abstractions/chaiwala-user-read` AppArmor abstraction was too broad
to be useful, and granted the confined process read access to the entire
home directory. It was deprecated in 17.03 and removed in 17.06.

<dt>

: `abstractions/chaiwala-execution` is removed in favour of not doing
anything

<dd>

The `abstractions/chaiwala-execution` AppArmor abstraction was empty. It
was deprecated in 17.03 and removed in 17.06.

<dt>

: `abstractions/chaiwala-cameras` removed with no immediate replacement

<dd>

The `abstractions/chaiwala-cameras` AppArmor abstraction did not have
the effect that its documentation suggested. It was deprecated in 17.03
and removed in 17.06.  tracks design work on similar concepts.

</dl>

## Infrastructure

### Apertis infrastructure tools

For Debian Jessie based systems:

` $ deb `<https://repositories.apertis.org/debian/>` jessie tools`

For Ubuntu Trusty based systems:

` $ deb `<https://repositories.apertis.org/ubuntu/>` trusty tools`

### Images

Image daily builds, as well as release builds can be found at:

` `<https://images.apertis.org/>

Image build tools can be found in the Apertis tools repositories.

## Known issues

### High (5)

  - GApplication are not visible after they are relaunched.

  - The video player window is split into 2 frames in default view

  - ARM64 board fails to boot minimal Apertis images

  - Kernel panic when using ARM64 board as build server

  - Applications don't launch when clicked from quick launcher instead a
    Blank screen is seen

### Normal (127)

  - libjackson-json-java fails to build from source

  - libreoffice fails to build from source on aarch64

  - subversion fails to build from source on aarch64 and armv7hl

  - serf fails to build from source on some x86_64

  - Observing multiple service instances in all 17.06 SDK images

  - ade: Fails to install multiple sysroots

  - Frampton application doesn't load when we re-launch them after
    clicking the back button

  - Rendering issue observed on websites like
    <http://www.moneycontrol.com>

  - Render theme buttons are not updating with respect to different zoom
    levels

  - HTML5 demo video's appear flipped when played on webkit2 based
    browser app

  - Broken HTML generation in Backworth for the developers portal
    website

  - Page rendering is not smooth in sites like www.yahoo.com

  - Mildenhall should install themes in the standard xdg data dirs

  - webkit2GTK crash observed flicking on webview from other widget

  - Failed to load Captcha in Apertis developer portal

  - webkit-ac-3d-rendering test case fails

  - Newport test fails on minimal images

  - Avoid unconstrained dbus AppArmor rules in frome

  - Steps like pattern is seen in the background in songs application

  - virtual keyboard is not showing for password input field of any
    webpage

  - u-boot: Unable to find the device tree for R-car starter kit H3UCLB

  - Shutdown not working properly in virtualbox

  - bredon-0-launcher should be shipped in its own package, not in
    libbredon-0-1

  - webview-test should be shipped in libbredon-0-tests instead of
    libbredon-0-1

  - libbredon/seed uninstallable on target as they depend on libraries
    in :development

  - Mismatching gvfs/gvfs-common and libatk1.0-0/libatk1.0-data package
    versions in the archive

  - MildenhallSelPopupItem model should be changed to accept only gchar
    \* instead of MildenhallSelPopupItemIconDetail for icons

  - Rhosydd service crashes when client exits on some special usecases
    (Refer description for it)

  - rhosydd-client crashes when displaying vehicle properties for mock
    backend

  - libmildenhall-0-0 contains files that would conflict with a future
    libmildenhall-0-1

  - libshoreham packaging bugs

  - arm-linux-gnueabihf-pkg-config does not work with sysroots installed
    by \`ade\`

  - MildenhallSelectionPopupItem doesn't take ownership when set
    properties

  - Videos are hidden when Eye is launched

  - Roller problem in settings application

  - In mildenhall, URL history speller implementation is incomplete.

  - Variable roller is not working

  - Songs do not start playing from the beginning but instead start a
    little ahead

  - Blank screen seen in Songs application on re-entry

  - Compositor hides the other screens

  - Status bar is not getting updated with the current song/video being
    played

  - canterbury: Most of the tests fail

  - ribchester: gnome-desktop-testing test times out

  - rhosydd: integration test fails

  - didcot-client: autopkgtest fails with
    org.apertis.Didcot.Error.NoApplicationFound and "Unit didcot.service
    could not be found"

  - GtkClutterLauncher: Segfaults with mouse right-clicking

  - Cross debugging through GDB and GDBserver Not possible.

  - Voice/Audio is not heard for the Bluez-hfp profile in i.MX6

  - The web runtime doesn't set the related view when opening new
    windows

  - Segmentation fault when disposing test executable of mildenhall

  - <abstractions/chaiwala-base> gives privileges that not every
    app-bundle should have

  - Canterbury entry-point launching hides global popups, but only
    sometimes

  - Album art is missing in one of the rows of the songs application

  - folks: random tests fail

  - GLib, GIO Reference Manual links are incorrectly swapped

  - GObject Generator link throws 404 error

  - Search feature doesn't work correctly for appdev portal located at
    <https://appdev.apertis.org/documentation/index.html>

  - cgroups-resource-control: test network-cgroup-prio-class failed

  - webview Y offset not considered to place, full screen video on
    youtube webpage

  - Confirm dialog status updated before selecting the confirm option
    YES/NO

  - gupnp-services tests test_service_browsing and
    test_service_introspection fail on target-arm image

  - Resizing the window causes page corruption

  - Content on a webpage doesn't load in sync with the scroll bar

  - Compositor seems to hide the bottom menu of a webpage

  - telepathy-gabble-tests should depend on python-dbus

  - libgles2-vivante-dev is not installable

  - mx6qsabrelite: linking issue with libgstimxeglvivsink.so and
    libgstimxvpu.so gstreamer plugins

  - apparmor-folks: unable to link contacts to test unlinking

  - tracker tests: Error creating thumbnails: No poster key found in
    metadata

  - Cannot open links within website like yahoo.com

  - mildenhall_launcher process needs to be killed in order to view
    browser using webkit2 GtkClutterLauncher

  - Not enough space in /opt to download and install sysroot

  - polkit-parsing: TEST_RESULT:fail

  - make check fails on libbredon package for wayland warnings

  - Ribchester: deadlock when calling RemoveApp() right after RollBack()

  - Canterbury messes up kerning when .desktop uses unicode chars

  - VirtualBox freezes using 100% CPU when resuming the host from
    suspend

  - VirtualBox display freezes when creating multiple notifications at
    once and interacting (hover and click) with them

  - apparmor-tracker: underlying_tests failed

  - tracker-indexing-local-storage: Stopping tracker-store services

  - Clang package fails to install appropriate egg-info needed by hotdoc

  - Unusable header in Traprain section in Devhelp

  - Album Art Thumbnail missing in Thumb view in ARTIST Application

  - If 2 drawers are activated, the most recent one hides behind the
    older one, instead of coming on top of older one.

  - Test apps are failing in Liblightwood with the use of GTest

  - Focus in launcher rollers broken because of copy/paste errors

  - Mismatch between server version file and sysroot version

  - Pulse Audio volume control doesn't launch as a separate window on
    SDK

  - Images for the video links are not shown in news.google.com on
    GtkClutterLauncher

  - Crash when initialising egl on ARM target

  - Zoom in feature does not work on google maps

  - Cannot open/view pdf documents in browser (GtkClutterLauncher)

  - Drop down lists are not working on a site like facebook

  - gupnp-services: test service failed

  - shapwick reads /etc/nsswitch.conf and /etc/passwd, and writes
    /var/root/.cache/dconf/

  - factory reset with a different image's rootfs.tar.gz results in
    emergency mode

  - GStreamer playbin prioritises imxeglvivsink over
    clutterautovideosink

  - Fix folks EDS tests to not be racy

  - Interaction with PulseAudio not allowed by its AppArmor profile

  - Background video is not played in some website with
    GtkClutterLauncher

  - Share links to facebook, twitter are nbt working in browser
    (GtkClutterLauncher)

  - The background HMI is blank on clicking the button for Power OFF

  - libsoup-unit: ssl-test failed for ARM

  - Horizontal scroll is not shown on GtkClutterLauncher

  - telepathy-gabble: Several tests failed

  - Investigate why development ARM image fails to generate

  - minimal image: DISPLAY sanity check tests failed

  - Unsupported language text is not shown on the page in
    GtkClutterLauncher

  - Back option is missing as part of the tool tip

  - Target doesn't reboot after system update

  - Bluetooth pairing option not working as expected

  - Video doesn't play when toggling from full screen to detail view

  - Theme ,any F node which is a child of an E node is not working for
    Apertis widgets.

  - cgroups-resource-control: blkio-weights tests failed

  - mildenhall-settings: does not generate localization files from
    source

  - libgrassmoor: executes tracker-control binary

  - Clutter_text_set_text API redraws entire clutterstage

  - factory-reset-tool TC: flagcheck messages are hidden by Plymouth

  - mxkineticscrollview-smooth-pan:Free scroll doesn't work

  - Network search pop-up isn't coming up in wi-fi settings

  - apparmor-libreoffice: libreoffice.normal.expected fails:
    ods_to_pdf: fail \[Bugzilla bug \#331\]

  - apparmor-pulseaudio: ARM Failed to drain stream: Timeout

  - Power button appers to be disabled on target

  - Documentation is not available from the main folder

  - Only half of the hmi is covered when opening gnome.org

  - No connectivity Popup is not seen when the internet is disconnected.

  - Not able to load heavy sites on GtkClutterLauncher

### Low (10)

  - qemu-arm-static not found in the pre installed qemu-user-static
    package

  - Spacing issues between text and selection box in website like amazon

  - gstreamer1-0-decode: Failed to load plugin warning

  - beep audio decoder gives errors continously

  - Context drawer displced when switching to fullscreen in browser app

  - Simulator screen is not in center but left aligned

  - bluetooth device pair failing with "Invalid arguments in method
    call"

  - webkit-clutter-javascriptcore: run-javascriptcore-tests fails

  - Mildenhall compositor crops windows

  - Upstream: linux-tools-generic should depend on lsb-release
