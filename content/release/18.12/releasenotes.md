+++
date = "2019-10-25"
weight = 100

title = "18.12 ReleaseNotes"

aliases = [
    "/old-wiki/18.12/ReleaseNotes"
]
+++

# Apertis 18.12 Release

**18.12** is the December 2018 stable development release of
**Apertis**, a Debian/Ubuntu derivative distribution geared towards the
creation of product-specific images for ARM (both the 32bit ARMv7 and
64-bit ARMv8 versions using the hardfloat ABI) and Intel x86-64 (64-bit)
systems.

This Apertis release is based on top of the Ubuntu 16.04 (Xenial) LTS
release with several customizations. Test results of the 18.12 release
are available in the [18.12 test report]().

### Release downloads

| [Apertis 18.12 images](https://images.apertis.org/release/18.12/) |
| ----------------------------------------------------------------- |
| Intel 64-bit                                                      |
| ARM 32-bit (U-Boot)                                               |
| ARM 32-bit (MX6q Sabrelite)                                       |
| ARM 64-bit (U-Boot)                                               |

The Intel `minimal`, `target` and `development` images are tested on the
[reference hardware (MinnowBoard MAX)]( {{< ref "/reference_hardware/_index.md" >}} ),
but they can run on any UEFI-based x86-64 system. The `sdk` image is
[tested under VirtualBox]( {{< ref "/virtualbox.md" >}} ).

#### Apertis 18.12 repositories

` $ deb `<https://repositories.apertis.org/apertis/>` 18.12 target helper-libs development sdk hmi`

## New features

### SDK Improvements

To get developers started even more quickly, the Apertis SDK images now
provide cross-compilers and an **armhf** sysroot right out of the box.
Developers no longer need to run the **ade** command to download it
separately.

### Base SDK

New product teams adopting Apertis may need a customized experience on
their SDK, as the default Apertis SDK may contain tools that are not
relevant for them and may lack some tools that they would like to use.

To provide them a better foundation for their customizations a reduced
base SDK image recipe is now part of Apertis, shipping a somewhat
bare-bones SDK environment for cross-compilation.

### Devroots

Devroots are somewhat similar to sysroots but address a different
use-case: while sysroots are only meant for pure cross-compilation and
remote debugging from the host, devroots are meant to target foreign
architecture by emulating native compilation thanks to QEMU and
`systemd-nspawn`. This is the most reliable way to build packages for
foreign architecture, at the cost of a slight performance hit for larger
projects.

While devroots are meant to be used on the Apertis SDK, they work just
fine on any non-Apertis environment providing QEMU and `systemd-nspawn`.

See the [devroot short
guide]( {{< ref "tooling.md#development-containers-using-devrootenter" >}} )
for futher information about how to use them from the Apertis SDK.

## Build and integration

### Apertis NFS images for LAVA

In the past the LAVA first stage was using generic kernels and NFS roots
which had been built manually in a non reproducible fashion. This also
meant that hardware support could get out of sync with respect to what
Apertis supported, as fixes didn't automatically trickle to those
components.

To ensure that things stay synchronized, Apertis now produces kernels
and NFS roots based on Apertis itself for the LAVA first stage. Those
images can be found at <https://images.apertis.org/lava/nfs/>.

Note that while a release is being developed, Lava jobs will run with an
image that was generated at the previous release. Those NFS images are
not limited to LAVA, they can also be installed on a local NFS server to
boot any supported hardware via NFS.

## Quality Assurance

### Improvements to the test definitions

Until now, the tests definitions were hosted on this wiki, with the
problems below:

  - The test description and their implementation are stored in
    different places and over the years slowly got out of sync. This is
    particularly true for automated tests, in which the test procedures
    on the wiki are duplicating the information already provided in the
    LAVA YAML test definitions.
  - Definitions are stored in a lax format not suitable for use by
    tools, so all the QA processes have to involve a massive amount of
    manual work that doesn't scale with the Apertis growth.
  - Test cases only address the most current release, there is no way to
    have different test cases for different release branches
  - The wiki doesn't offer a good review workflow as developer are used
    when working on git repositories on [our GitLab
    instance](https://gitlab.apertis.org)

As the first step of a complete overhaul of the test handling process
all the test case definitions, both manual and automated, have been
converted to a stricter YAML format in the
[apertis-test-cases](https://gitlab.apertis.org/tests/apertis-test-cases/)
git repository. The YAML format is an extension of the LAVA format, so
the testcase are immediately usable by LAVA.

The new rendered testcases can be seen at
[qa.apertis.org](https://qa.apertis.org/).

### Testing on immutable rootfs, part 2

To continue the work started on Apertis 18.09, more tests were ported to
work on immutable rootfs.

Since the high priority tests were already ported, some medium priority
tests have been ported this quarter.

The focus was on testing minimal images and AppArmor features.

## Design and documentation

### Merging software update and OSTree documents

Two separate documents were covering the update and rollback system for
Apertis:

  - the [System updates and
    rollback]( {{< ref "system-updates-and-rollback.md" >}} )
    strived to be generic and agnostic about the actual update
    technology
  - the [OSTree resilient
    upgrades](https://designs.apertis.org/private/latest/ostree-resilient-upgrades.html)
    approach described how OSTree could be used to implement the
    features described in the first document. This document was private
    until now, and has been published.

Having these split in two had several issues:

  - readers may find the difference in purpose to be not obvious,
    wondering why we have two documents;
  - some readers may be only aware of the existence of one document but
    not the other;
  - the documents may get out of sync and not describe a coherent
    design.

Those documents are being merged into one for better readability and
easier maintenance.

## Technology Previews

### Offline updates over USB Mass Storage devices

Albeit not part of this release, work has started to implement fully
automatic system updates with rollback in case of errors on top of the
OSTree work done in the past releases.

The
[apertis-update-manager](https://gitlab.apertis.org/appfw/apertis-update-manager/)
component in the OSTree-based images to manage the update process and to
provide a high level D-Bus interface for user interaction.

In this cycle only offline updates over USB Mass Storage devices have
been implemented on the `armhf` images targeting the i.MX6 Sabrelite
platform.

In the coming cycles online OTA updates are planned and support will be
extended to all the other Apertis reference platforms.

## Deprecations and ABI/API breaks

### Deprecations

During this release cycle we have continued to mark obsolete or
problematic APIs with the [ABI
break](https://phabricator.apertis.org/tag/abi_break/) tag as a way to
clear technical debt in future.

#### Phabricator code contribution workflow

<dl>

<dt>

The use of Phabricator Differential and `git-phab` for code
contributions and reviews is deprecated in favour of the GitLab merge
request workflow

<dd>

For this release Apertis has fully switched to use the GitLab merge
request workflow for all contributions, which is more integrated and
smoother than the Phabricator Differential workflow. The documentation
on the wiki has been updated: refer to the [contribution
process]( {{< ref "contributions.md" >}} ).

</dl>

#### Test case definitions on the wiki

The definitions under [QA/Test_Cases]( {{< ref "/qa/test_cases/_index.md" >}} ) are
now deprecated in favour of the YAML-based definitions in the
[apertis-test-cases](https://gitlab.apertis.org/tests/apertis-test-cases/)
repository, see the [Improvements to the test
definitions](#improvements-to-the-test-definitions) section.

#### Mildenhall UI framework

<dl>

<dt>

The Mildenhall UI toolkit is deprecated in favour of other UI toolkits
such as GTK+, Qt, or HTML5-based solutions

<dd>

From this release Apertis may no longer ship the Mildenhall UI toolkit
nor any accompanying libraries, as the underlying Clutter library has
been deprecated upstream for some years now.

</dl>

#### Application framework

<dl>

<dt>

`ribchester` and `canterbury` are deprecated

<dd>

From this release Canterbury and Ribchester are deprecated as is the
[Application
Layout]( {{< ref "application-layout.md" >}} )
document in favour of Flatpak.

</dl>

### Breaks

#### Development images

<dl>

<dt>

The `development` image type has been removed in favour of extending the
`target` image type.

<dd>

From this release Apertis will no longer ship `development` images.
Users can pick the `target` images and manually install any needed
package with `apt`. The inclusion of any specific tool to be shipped
out-of-the-box on the `target` can be requested on Phabricator. The
`development` ospack is still available, and it is used to generate
sysroots for cross-compilation.

</dl>

## Infrastructure

### Apertis Docker registry

The Apertis Docker registry stores Docker images in order to provide a
unified and easily reproducible build environment for developers and
services.

As of today, this includes the `apertis-image-builder`,
`apertis-package-builder`, `apertis-testcases-builder` and
`apertis-documentation-builder` Docker images.

### Apertis infrastructure tools

The Apertis Debian Stretch tools repository provides packages for
`git-phab`, `lqa`, Debos and deps, LAVA v2, OBS 2.7, `ostree-push` and
`jenkins-job-builder`:

` $ deb `<https://repositories.apertis.org/debian/>` stretch tools`

### Images

Image daily builds, as well as release builds can be found at:

` `<https://images.apertis.org/>

Image build tools can be found in the Apertis tools repositories.

### Infrastructure overview

The [Image build infrastructure
document]( {{< ref "image-build-infrastructure.md" >}} )
provides an overview of the image building process and the involved
services.

## Known issues

### High (5)

  - The touchscreen displays connected to the i.MX6 Saberlite board
    isn't functioning on the rev 1.2 boards

  - boot-no-crashes: boot-no-crashes test failed

  - OSTree fails to build working ostree images in SDK

  - LAVA-Phab bridge should post a comment once tests stop failing

  - Issue with dbus limits when executing test-dbus-daemon from
    dbus-installed-tests

### Normal (95)

  - sanity-check: verify that a trivial PyGI and/or gjs GUI can run

  - connman shows incorrect Powered status for the bluetooth technology

  - telepathy-ring: Review and fix SMS test

  - property changed signal in org.bluez.Network1 is not emiting when
    PAN disconnects

  - apparmor-libreoffice: Several tests failed

  - Overhaul tests that require being run with AppArmor lockdown enabled
    to get started under Canterbury

  - cgroups-resource-control: blkio-weights tests failed

  - GStreamer playbin prioritises imxeglvivsink over
    clutterautovideosink

  - gupnp-services: test service failed

  - Crash when initialising egl on ARM target

  - Develop test case for out of screen events in Wayland images

  - Pulse Audio volume control doesn't launch as a separate window on
    SDK

  - Test apps are failing in Liblightwood with the use of GTest

  - apparmor-tracker: underlying_tests failed

  - Fix Tracker testcase to not download media files from random HTTP
    user folders

  - VirtualBox display freezes when creating multiple notifications at
    once and interacting (hover and click) with them

  - Ribchester: deadlock when calling RemoveApp() right after RollBack()

  - tracker tests: Error creating thumbnails: No poster key found in
    metadata

  - mx6qsabrelite: linking issue with libgstimxeglvivsink.so and
    libgstimxvpu.so gstreamer plugins

  - libgles2-vivante-dev is not installable

  - gupnp-services tests test_service_browsing and
    test_service_introspection fail on target-arm image

  - arm-linux-gnueabihf-pkg-config does not work with sysroots installed
    by \`ade\`

  - Mismatching gvfs/gvfs-common and libatk1.0-0/libatk1.0-data package
    versions in the archive

  - Observing multiple service instances in all 17.06 SDK images

  - Containers fail to load on Gen4 host

  - gnutls depends on old abandoned package gmp4 due to licensing
    reasons

  - Preseed action is needed for Debos

  - Debos: allow to use partition number for 'raw' action

  - Debos: mountpoints which are mounted only in build time

  - Debos: option for kernel command parameters for 'filesystem-deplay'
    action

  - ribchester-core causes apparmor denies on non-btrfs minimal image

  - Debos: changes 'resolv.conf' during commands execution chroot

  - Volume Control application hangs on opening it

  - gupnp-services: browsing and introspection tests fail

  - sqlite: Many tests fail for target amd64, armhf and sdk

  - dbus-installed-tests: service failed because a timeout was exceeded

  - boot-no-crashes: ANOM_ABEND found in journalctl logs

  - tracker-indexing-mass-storage test case fails

  - do-branching fails at a late stage cloning OBS binary repos

  - A 2-3 second lag between the speakers is observed when a hfp
    connection is made over bluetooth

  - SDK hangs when trying to execute bluez-hfp testcase

  - OSTree testsuite hangs the gouda Jenkins build slave

  - Couldn't install application: error seen when trying to install to
    target using ade

  - Ribchester mount unit depends on Btrfs

  - Eclipse Build is not working for HelloWorld App

  - Not able to create namespace for AppArmor container on
    \`mx6qsabrelite\` board

  - Seek/Pause option does not work correctly on YouTube

  - Failed unmounting /var bind mount error seen on shutting down an
    i.MX6/Minnowboard with Minimal ARM-32/AMD64 image

  - Apps like songs, artists etc are not offser correctly by the
    compositor window

  - Double tapping on any empty space in the songs app's screen results
    in the compositor window hinding the left ribbon

  - Back button is hidden by the compositor window in apps like songs,
    artists etc

  - Ensure that the uploaded linux package version uniquely identifies a
    single commit

  - Canterbury needs to explicitly killed to relauch the Mildenhall
    compositor

  - Mildenhall compositor gets offset when we open an app like songs

  - cgroups-resource-control: cpu-shares test failed

  - gupnp-services: run-test test failed

  - tracker-indexing-local-storage: run-test test failed

  - apparmor-gstreamer1-0: run-test-sh test failed

  - apparmor-pulseaudio: run-test-pulseaudio test failed

  - apparmor-pulseaudio: pulseaudio.malicious.expected test failed

  - apparmor-pulseaudio: pulseaudio.normal.expected_underlying_tests
    test failed

  - apparmor-tracker: run-test-tracker test failed

  - apparmor-tracker: tracker.malicious.expected test failed

  - apparmor-tracker: tracker.malicious.expected_underlying_tests test
    failed

  - apparmor-tracker: tracker.normal.expected test failed

  - apparmor-tracker: tracker.normal.expected_underlying_tests test
    failed

  - apparmor-utils: aa-enforce-test test failed

  - kernel panic occurs when trying to boot an image on i.mx6 revision
    1.2 board from LAVA

  - gupnp-services: test_service_introspection test failed

  - gupnp-services: test_service_browsing test failed

  - build failure in liblightwood-2 on 18.06 image

  - newport: newport test failed

  - lava: 12_apparmor-functional-demo test failed

  - apparmor-pulseaudio:
    pulseaudio.malicious.expected_underlying_tests test failed

  - didcot: didcot test failed

  - traprain: traprain test failed

  - "Setup Install to Target" option is not showing on eclipse

  - Ospacks ship a .gitignore file in the root directory

  - Ospacks ship an empty /script directory in the root directory

  - recipe for target 'install' failed error seen on executing the
    install to target using ade

  - rhosydd: rhosydd test failed

  - rhosydd: integration test failed

  - LAVA-Phab bridge should mark test failures as bugs

  - "su -" is not in working state

  - "pam-auth-update" generates wrong configuration with some additional
    packages

  - build-snapshot: allow to build packages without \`autogen.sh\`
    script

  - Wi-Fi search button is missing in wifi application

  - LAVA-Phab bridge does not report the release version or the board
    used of the tested image

  - traprain: gnome-desktop-testing test failed

  - apparmor-geoclue: run-test-geoclue test failed

  - create-abstract-sync-database: create-abstract-sync-database test
    failed

  - apparmor-pulseaudio: run-test-sh test failed

  - rfkill-toggle testcase fails

  - bluez-hfp testcase fails

  - ade tests failing on lava

### Low (129)

  - Remove unnecessary folks package dependencies for automated tests

  - Not able to load heavy sites on GtkClutterLauncher

  - No connectivity Popup is not seen when the internet is disconnected.

  - Upstream: linux-tools-generic should depend on lsb-release

  - remove INSTALL, aclocal.m4 files from langtoft

  - Mildenhall compositor crops windows

  - Documentation is not available from the main folder

  - Power button appers to be disabled on target

  - apparmor-libreoffice: libreoffice.normal.expected fails:
    ods_to_pdf: fail \[Bugzilla bug \#331\]

  - Network search pop-up isn't coming up in wi-fi settings

  - Clutter_text_set_text API redraws entire clutterstage

  - libgrassmoor: executes tracker-control binary

  - mildenhall-settings: does not generate localization files from
    source

  - Videos are hidden when Eye is launched

  - Theme ,any F node which is a child of an E node is not working for
    Apertis widgets.

  - Video doesn't play when toggling from full screen to detail view

  - Simulator screen is not in center but left aligned

  - Back option is missing as part of the tool tip

  - Unsupported launguage text is not shown on the page in
    GtkClutterLauncher

  - The video player window is split into 2 frames in default view

  - Horizontal scroll is not shown on GtkClutterLauncher

  - The background HMI is blank on clicking the button for Power OFF

  - Only half of the hmi is covered when opening gnome.org

  - Share links to facebook, twitter are nbt working in browser
    (GtkClutterLauncher)

  - Background video is not played in some website with
    GtkClutterLauncher

  - Interaction with PulseAudio not allowed by its AppArmor profile

  - Fix folks EDS tests to not be racy

  - shapwick reads /etc/nsswitch.conf and /etc/passwd, and writes
    /var/root/.cache/dconf/

  - Cannot open/view pdf documents in browser (GtkClutterLauncher)

  - Zoom in feature does not work on google maps

  - Printing hotdoc webpage directly results in misformatted document

  - Images for the video links are not shown in news.google.com on
    GtkClutterLauncher

  - Focus in launcher rollers broken because of copy/paste errors

  - beep audio decoder gives errors continously

  - If 2 drawers are activated, the most recent one hides behind the
    older one, instead of coming on top of older one.

  - Album Art Thumbnail missing in Thumb view in ARTIST Application

  - Unusable header in Traprain section in Devhelp

  - Clang package fails to install appropriate egg-info needed by hotdoc

  - gstreamer1-0-decode: Failed to load plugin warning

  - Canterbury messes up kerning when .desktop uses unicode chars

  - make check fails on libbredon package for wayland warnings

  - Cannot open links within website like yahoo.com

  - apparmor-folks: unable to link contacts to test unlinking

  - Compositor seems to hide the bottom menu of a webpage

  - Spacing issues between text and selection box in website like amazon

  - Content on a webpage doesn't load in sync with the scroll bar

  - Resizing the window causes page corruption

  - Confirm dialog status updated before selecting the confirm option
    YES/NO

  - webview Y offset not considered to place, full screen video on
    youtube webpage

  - cgroups-resource-control: test network-cgroup-prio-class failed

  - GObject Generator link throws 404 error

  - GLib, GIO Reference Manual links are incorrectly swapped

  - folks: random tests fail

  - Album art is missing in one of the rows of the songs application

  - Canterbury entry-point launching hides global popups, but only
    sometimes

  - <abstractions/chaiwala-base> gives privileges that not every
    app-bundle should have

  - Segmentation fault when disposing test executable of mildenhall

  - The web runtime doesn't set the related view when opening new
    windows

  - GtkClutterLauncher: Segfaults with mouse right-clicking

  - ribchester: gnome-desktop-testing test times out

  - canterbury: Most of the tests fail

  - Status bar is not getting updated with the current song/video being
    played

  - Compositor hides the other screens

  - Songs do not start playing from the beginning but instead start a
    little ahead

  - Roller problem in settings application

  - Variable roller is not working

  - In mildenhall, URL history speller implementation is incomplete.

  - MildenhallSelectionPopupItem doesn't take ownership when set
    properties

  - libshoreham packaging bugs

  - libmildenhall-0-0 contains files that would conflict with a future
    libmildenhall-0-1

  - rhosydd-client crashes when displaying vehicle properties for mock
    backend

  - Rhosydd service crashes when client exits on some special usecases
    (Refer description for it)

  - MildenhallSelPopupItem model should be changed to accept only gchar
    \* instead of MildenhallSelPopupItemIconDetail for icons

  - libbredon/seed uninstallable on target as they depend on libraries
    in :development

  - webview-test should be shipped in libbredon-0-tests instead of
    libbredon-0-1

  - bredon-0-launcher should be shipped in its own package, not in
    libbredon-0-1

  - virtual keyboard is not showing for password input field of any
    webpage

  - Steps like pattern is seen in the background in songs application

  - Avoid unconstrained dbus AppArmor rules in frome

  - Newport test fails on minimal images

  - connman: patch "Use ProtectSystem=true" rejected upstream

  - connman: patch "device: Don't report EALREADY" not accepted upstream

  - webkit2GTK crash observed flicking on webview from other widget

  - Mildenhall should install themes in the standard xdg data dirs

  - Page rendering is not smooth in sites like www.yahoo.com

  - HTML5 demo video's appear flipped when played on webkit2 based
    browser app

  - Render theme buttons are not updating with respect to different zoom
    levels

  - Rendering issue observed on websites like
    <http://www.moneycontrol.com>

  - Frampton application doesn't load when we re-launch them after
    clicking the back button

  - Crash observed on webruntime framework

  - Crash observed on seed module when we accesing the D-Bus call method

  - introspectable support for GObject property, signal and methods

  - On multiple re-entries from settings to eye the compositor hangs

  - Segmentation fault occurs while exectuing webkit2gtk-aligned-scroll
    test case

  - zoom feature is not working as expected

  - Segmentation fault is observed on closing the mildenhall-compositor

  - Building cargo and rustc in 17.12 for Firefox Quantum 57.0

  - Blank screen seen on executing last-resort-message testcase

  - Inital Roller mappings are misaligned on the HMI

  - folks-metacontacts: folks-metacontacts-linking_sh.service failed

  - folks-alias-persistence: folks-alias-persistence_sh.service failed

  - folks-metacontacts-antilinking:
    folks-metacontacts-antilinking_sh.service failed

  - folks-search-contacts: folks-search-contacts_sh.service failed

  - apparmor-tracker: AssertionError: False is not true

  - apparmor-folks: Several tests fail

  - apparmor-gstreamer1.0: gstreamer1.0-decode: assertion failed

  - tracker-indexing-local-storage: AssertionError: False is not true

  - didcot: test_open_uri: assertion failed

  - traprain: sadt: error: cannot find debian/tests/control

  - canterbury: core-as-root and full-as-root tests failed

  - ribchester: Job for generated-test-case-ribchester.service canceled

  - apparmor-pulseaudio: pulseaudio.normal.expected_underlying_tests
    fails

  - webkit2gtk-drag-and-drop doesn't work with touch

  - folks-alias-persistence: folks-alias-persistence test failed

  - folks-extended-info: folks-extended-info test failed

  - folks-metacontacts: folks-metacontacts-unlinking test failed

  - folks-metacontacts: folks-metacontacts-linking test failed

  - folks-metacontacts-antilinking: folks-metacontacts-antilinking test
    failed

  - folks-search: folks-search-contacts test failed

  - apparmor-folks: run-test-folks test failed

  - apparmor-folks: R1.13b_folks-metacontacts-unlinking test failed

  - apparmor-folks: R1.13b_folks-metacontacts-linking test failed

  - apparmor-folks: R1.13b_folks-alias-persistence test failed

  - apparmor-folks: R6.4.2_folks-metacontacts-antilinking test failed

  - frome: frome test failed

  - frome: gnome-desktop-testing test failed

  - libsoup: /usr/lib/libsoup2.4/installed-tests/libsoup/ssl-test test
    failed

  - folks-telepathy: folks-retrieve-contacts-telepathy test failed

  - apparmor-folks: R1.13b_folks-retrieve-contacts-telepathy test
    failed
