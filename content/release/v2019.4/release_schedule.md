+++
date = "2020-05-11"
weight = 100

title = "v2019.4 Release schedule"
+++

The v2019.4 release cycle started in July 2020.

| Milestone                                                                                                | Date              |
| -------------------------------------------------------------------------------------------------------- | ----------------- |
| Start of release cycle                                                                                   | 2020-07-01        |
| Soft feature freeze: end of feature proposal and review period                                           | 2020-08-13        |
| Soft code freeze/hard feature freeze: end of feature development for this release, only bugfixes allowed | 2020-08-20        |
| Release candidate 1 (RC1)/hard code freeze: no new code changes may be made after this date              | 2020-08-26        |
| RC testing                                                                                               | 2020-08-27..09-02 |
| v2019.4 release                                                                                          | 2020-09-03        |

If the release candidate 1 does not successfully pass all required
tests, then the issues will be fixed and a new release candidate will be
re-tested. This would delay the release, which would be reflected on
this page.

## See also

  - Previous [release schedules]( {{< ref "/policies/releases.md" >}} ) and
    more information about the timeline
