+++
date = "2018-03-19"
weight = 100

title = "18.03 ReleaseNotes"

aliases = [
    "/old-wiki/18.03/ReleaseNotes"
]
+++

# Apertis 18.03 Release

**18.03** is the current stable development release of **Apertis**, a
Debian/Ubuntu derivative distribution geared towards the creation of
product-specific images for ARM (both in the 32bit ARMv7 version using
the hardfloat ABI and in the 64-bit ARMv8 version) and Intel x86-64
(64-bit) systems.

This Apertis release is based on top of the Ubuntu 16.04 (Xenial) LTS
release with several customization. Test results of the 18.03 release
are available in the [18.03 test report]().

### Release downloads

| [Apertis 18.03 images](https://images.apertis.org/release/18.03/) |
| ----------------------------------------------------------------- |
| Intel 64-bit                                                      |
| ARM 32-bit                                                        |
| ARM 64-bit                                                        |

The Intel `minimal`, `target` and `development` images are tested on
x86-64 QEMU, but they can run on any UEFI-based x86-64 system. The `sdk`
image is [tested under VirtualBox]( {{< ref "/virtualbox.md" >}} ).

#### Apertis 18.03 repositories

` $ deb `<https://repositories.apertis.org/apertis/>` 18.03 target helper-libs development sdk hmi`

## New features

This release has focused on completing the migration to the new debos
building pipeline which was started during the 17.12 release cycle and
is now complete. The new pipeline is meant to be faster and more
flexible, and also allow developers to easily build custom images on
their machines.

Work also continues on implementing OSTree based images. OSTree based
images are now available for ARM (both 32bit ARMv7 and 64-bit ARMv8
versions) and Intel x86-64 (64-bit). These images are provided as a
technology preview in 18.03 in addition to the regular deb based images.
OSTree is to be used as the basis for a robust and efficient update
mechanism. Integration of the OSTree based images into the automated
LAVA test environment is underway.

## Design and documentation

### Image building infrastructure documentation

A design document has been added to cover the high-level concept and
infrastructure setup required for [debos based image
building]( {{< ref "image-build-infrastructure.md" >}} ).

## Deprecations and ABI/API breaks

### Deprecations

During this release cycle we have continued to mark obsolete or
problematic APIs with the [ABI
break](https://phabricator.apertis.org/tag/abi_break/) tag as a way to
clear technical debt in future.

#### Mildenhall UI framework

<dl>

<dt>

The Mildenhall UI toolkit is deprecated in favour of other UI toolkits
such as GTK+, Qt, or HTML5-based solutions

<dd>

From the next release Apertis may no longer ship the Mildenhall UI
toolkit nor any accompanying libraries, as the underlying Clutter
library has been deprecated upstream for some years now.

</dl>

#### Application framework

<dl>

<dt>

`ribchester-full` and `canterbury-full` are deprecated

<dd>

From the next release Canterbury and Ribchester will only offer the APIs
provided by the respective `-core` and the additional APIs offered by
the `-full` packages will be dropped

</dl>

### Breaks

No known breaking changes are part of the 18.03 release.

## Infrastructure

### Apertis infrastructure tools

Repositories for Debian Jessie based systems and Ubuntu Trusty based
systems are obsoleted:

` `~~`$``   ``deb``   `<https://repositories.apertis.org/debian/>`
 ``jessie``   ``tools`~~
` `~~`$``   ``deb``   `<https://repositories.apertis.org/ubuntu/>`
 ``trusty``   ``tools`~~

Packages ~~lava v1 and django deps, obs 2.3, etc~~ are obsoleted as
well.

For Debian Stretch based systems:

` $ deb `<https://repositories.apertis.org/debian/>` stretch tools`

New lqa, debos and deps, lava v2, obs 2.7, ostree-push are available as
replacement.

### Images

Image daily builds, as well as release builds can be found at:

` `<https://images.apertis.org/>

Image build tools can be found in the Apertis tools repositories.

### Jenkins jobs template

Jenkins jobs template are all ship centrally in apertis-jenkins-jobs Git
repo:

`  `<https://git.apertis.org/cgit/apertis-jenkins-jobs.git/>

instead of spread all over individual Git repos. (This only applies to
the new releases from 18.03 onwards.)

### Image recipes

We no longer use `apertis-customizations` and
`apertis-image-customization` repositories to hold files related to the
image building infrastructure. Instead of them, please refer to this new
repository for debos configuration and related files:

`   `<https://git.apertis.org/cgit/apertis-image-recipes.git/>

(This only applies to the new releases from 18.03 onwards.)

### LAVA

LAVA migration to run **Version 2** jobs have been completed during this
quarter. LAVA devices were properly configured to use the new version
and all Apertis tests jobs have been ported to use this new job format
(V2).

LAVA server has been upgraded to version **2018.2** dropping support for
V1 jobs and completing the V2 migration:
<https://lava.collabora.co.uk/static/docs/v2/>

## Known issues

### Normal (140)

  - Unusable header in Traprain section in Devhelp

  - mildenhall-settings: does not generate localization files from
    source

  - No connectivity Popup is not seen when the internet is disconnected.

  - Test apps are failing in Liblightwood with the use of GTest

  - apparmor-libreoffice: libreoffice.normal.expected fails:
    ods_to_pdf: fail \[Bugzilla bug \#331\]

  - tracker-indexing-local-storage: Stopping tracker-store services

  - apparmor-tracker: underlying_tests failed

  - VirtualBox display freezes when creating multiple notifications at
    once and interacting (hover and click) with them

  - VirtualBox freezes using 100% CPU when resuming the host from
    suspend

  - Canterbury messes up kerning when .desktop uses unicode chars

  - Mismatch between server version file and sysroot version

  - Crash when initialising egl on ARM target

  - libsoup-unit: ssl-test failed for ARM

  - Pulse Audio volume control doesn't launch as a separate window on
    SDK

  - polkit-parsing: TEST_RESULT:fail

  - Clang package fails to install appropriate egg-info needed by hotdoc

  - Focus in launcher rollers broken because of copy/paste errors

  - The web runtime doesn't set the related view when opening new
    windows

  - Segmentation fault when disposing test executable of mildenhall

  - <abstractions/chaiwala-base> gives privileges that not every
    app-bundle should have

  - Album art is missing in one of the rows of the songs application

  - folks: random tests fail

  - GLib, GIO Reference Manual links are incorrectly swapped

  - GObject Generator link throws 404 error

  - Search feature doesn't work correctly for appdev portal located at
    <https://appdev.apertis.org/documentation/index.html>

  - cgroups-resource-control: test network-cgroup-prio-class failed

  - webview Y offset not considered to place, full screen video on
    youtube webpage

  - Confirm dialog status updated before selecting the confirm option
    YES/NO

  - gupnp-services tests test_service_browsing and
    test_service_introspection fail on target-arm image

  - libgles2-vivante-dev is not installable

  - mx6qsabrelite: linking issue with libgstimxeglvivsink.so and
    libgstimxvpu.so gstreamer plugins

  - apparmor-folks: unable to link contacts to test unlinking

  - tracker tests: Error creating thumbnails: No poster key found in
    metadata

  - Cannot open links within website like yahoo.com

  - Observing multiple service instances in all 17.06 SDK images

  - Frampton application doesn't load when we re-launch them after
    clicking the back button

  - Rendering issue observed on websites like
    <http://www.moneycontrol.com>

  - Render theme buttons are not updating with respect to different zoom
    levels

  - HTML5 demo video's appear flipped when played on webkit2 based
    browser app

  - Page rendering is not smooth in sites like www.yahoo.com

  - Mildenhall should install themes in the standard xdg data dirs

  - webkit2GTK crash observed flicking on webview from other widget

  - Newport test fails on minimal images

  - Avoid unconstrained dbus AppArmor rules in frome

  - Steps like pattern is seen in the background in songs application

  - virtual keyboard is not showing for password input field of any
    webpage

  - Shutdown not working properly in the SDK VirtualBox VM

  - bredon-0-launcher should be shipped in its own package, not in
    libbredon-0-1

  - webview-test should be shipped in libbredon-0-tests instead of
    libbredon-0-1

  - libbredon/seed uninstallable on target as they depend on libraries
    in :development

  - Mismatching gvfs/gvfs-common and libatk1.0-0/libatk1.0-data package
    versions in the archive

  - MildenhallSelPopupItem model should be changed to accept only gchar
    \* instead of MildenhallSelPopupItemIconDetail for icons

  - Rhosydd service crashes when client exits on some special usecases
    (Refer description for it)

  - A 2-3 second lag between the speakers is observed when a hfp
    connection is made over bluetooth

  - rhosydd-client crashes when displaying vehicle properties for mock
    backend

  - Apps like Songs, Setttings are not launching on the target

  - Eclipse Build is not working for HelloWorld App

  - Generated OSTree repositories are incomplete

  - Mildenhall Launcher doesn't launch in SDK & target

  - Access denied message seen when we shutdown an i.MX6 or Minnowboard
    max target

  - Ribchester mount unit depends on Btrfs

  - telepathy-ring testcase fails

  - Couldn't install application: error seen when trying to install to
    target using ade

  - apparmor profile is not loaded on boot

  - traffic-control-tcmmd test fails

  - youtube content is not rendering on GtkClutterLauncher window

  - OSTree testsuite hangs the gouda Jenkins build slave

  - SDK hangs when trying to execute bluez-hfp testcase

  - webkit2gtk-drag-and-drop doesn't work with touch

  - VirtualBox 5.2 Guest Additions causing backtrace and stalling of
    boot

  - Full-screen Mode fails in Apertis SDK 17.12 in VirtualBox

  - check-dbus-services: Error:
    GDBus.Error:org.freedesktop.DBus.Error.AccessDenied

  - apparmor-pulseaudio: pulseaudio.normal.expected_underlying_tests
    fails

  - frome: \`/tmp/frome-\*': No such file or directory

  - do-branching fails at a late stage cloning OBS binary repos

  - ribchester: Job for generated-test-case-ribchester.service canceled

  - canterbury: core-as-root and full-as-root tests failed

  - traprain: sadt: error: cannot find debian/tests/control

  - tracker-indexing-mass-storage test case fails

  - boot-no-crashes: ANOM_ABEND found in journalctl logs

  - dbus-installed-tests: service failed because a timeout was exceeded

  - sqlite: Many tests fail for target amd64, armhf and sdk

  - didcot: test_open_uri: assertion failed

  - tracker-indexing-local-storage: AssertionError: False is not true

  - gupnp-services: browsing and introspection tests fail

  - apparmor-gstreamer1.0: gstreamer1.0-decode: assertion failed

  - apparmor-folks: Several tests fail

  - apparmor-tracker: AssertionError: False is not true

  - folks-search-contacts: folks-search-contacts_sh.service failed

  - folks-metacontacts-antilinking:
    folks-metacontacts-antilinking_sh.service failed

  - folks-alias-persistence: folks-alias-persistence_sh.service failed

  - folks-telepathy: folks-retrieve-contacts-telepathy_sh.service
    failed

  - folks-metacontacts: folks-metacontacts-linking_sh.service failed

  - Blank screen seen on executing last-resort-message testcase

  - Building cargo and rustc in 17.12 for Firefox Quantum 57.0

  - Volume Control application hangs on opening it

  - Debos: changes 'resolv.conf' during commands execution chroot

  - ribchester-core causes apparmor denies on non-btrfs minimal image

  - Debos: option for kernel command parameters for 'filesystem-deplay'
    action

  - Debos: mountpoints which are mounted only in build time

  - Debos: allow to use partition number for 'raw' action

  - Preseed action is needed for Debos

  - zoom feature is not working as expected

  - Segmentation fault occurs while exectuing webkit2gtk-aligned-scroll
    test case

  - gnutls depends on old abandoned package gmp4 due to licensing
    reasons

  - introspectable support for GObject property, signal and methods

  - Crash observed on seed module when we accesing the D-Bus call method

  - Containers fail to load on Gen4 host

  - Crash observed on webruntime framework

  - Incorrect network available state shown in GNetworkMonitor

  - libmildenhall-0-0 contains files that would conflict with a future
    libmildenhall-0-1

  - libshoreham packaging bugs

  - arm-linux-gnueabihf-pkg-config does not work with sysroots installed
    by \`ade\`

  - MildenhallSelectionPopupItem doesn't take ownership when set
    properties

  - In mildenhall, URL history speller implementation is incomplete.

  - Variable roller is not working

  - Compositor hides the other screens

  - Status bar is not getting updated with the current song/video being
    played

  - canterbury: Most of the tests fail

  - ribchester: gnome-desktop-testing test times out

  - rhosydd: integration test fails

  - didcot-client: autopkgtest fails with
    org.apertis.Didcot.Error.NoApplicationFound and "Unit didcot.service
    could not be found"

  - GtkClutterLauncher: Segfaults with mouse right-clicking

  - Cross debugging through GDB and GDBserver Not possible.

  - Canterbury entry-point launching hides global popups, but only
    sometimes

  - Unsupported launguage text is not shown on the page in
    GtkClutterLauncher

  - cgroups-resource-control: blkio-weights tests failed

  - make check fails on libbredon package for wayland warnings

  - Ribchester: deadlock when calling RemoveApp() right after RollBack()

  - libgrassmoor: executes tracker-control binary

  - gupnp-services: test service failed

  - shapwick reads /etc/nsswitch.conf and /etc/passwd, and writes
    /var/root/.cache/dconf/

  - factory reset with a different image's rootfs.tar.gz results in
    emergency mode

  - GStreamer playbin prioritises imxeglvivsink over
    clutterautovideosink

  - Fix folks EDS tests to not be racy

  - Interaction with PulseAudio not allowed by its AppArmor profile

  - property changed signal in org.bluez.Network1 is not emiting when
    PAN disconnects

  - connman shows incorrect Powered status for the bluetooth technology

  - remove INSTALL, aclocal.m4 files from langtoft

  - Not able to load heavy sites on GtkClutterLauncher

### Low (47)

  - Upstream: linux-tools-generic should depend on lsb-release

  - cross-compilation with sysroot fails without extra rpath for Mesa

  - If 2 drawers are activated, the most recent one hides behind the
    older one, instead of coming on top of older one.

  - The video player window is split into 2 frames in default view

  - Album Art Thumbnail missing in Thumb view in ARTIST Application

  - Network search pop-up isn't coming up in wi-fi settings

  - Bluetooth pairing option not working as expected

  - Documentation is not available from the main folder

  - beep audio decoder gives errors continously

  - Remove unnecessary folks package dependencies for automated tests

  - Zoom in feature does not work on google maps

  - Cannot open/view pdf documents in browser (GtkClutterLauncher)

  - Printing hotdoc webpage directly results in misformatted document

  - Drop down lists are not working on a site like facebook

  - Share links to facebook, twitter are nbt working in browser
    (GtkClutterLauncher)

  - Horizontal scroll is not shown on GtkClutterLauncher

  - Images for the video links are not shown in news.google.com on
    GtkClutterLauncher

  - Songs do not start playing from the beginning but instead start a
    little ahead

  - Compositor seems to hide the bottom menu of a webpage

  - ade process fails to start gdbserver if a first try has failed

  - Inital Roller mappings are misaligned on the HMI

  - Segmentation fault is observed on closing the mildenhall-compositor

  - On first flashing of the image any second application does not get
    displayed (eg. album art)

  - On multiple re-entries from settings to eye the compositor hangs

  - connman: patch "device: Don't report EALREADY" not accepted upstream

  - connman: patch "Use ProtectSystem=true" rejected upstream

  - qemu-arm-static not found in the pre installed qemu-user-static
    package

  - Spacing issues between text and selection box in website like amazon

  - Context drawer displced when switching to fullscreen in browser app

  - Roller problem in settings application

  - Blank screen seen in Songs application on re-entry

  - Back option is missing as part of the tool tip

  - Content on a webpage doesn't load in sync with the scroll bar

  - Resizing the window causes page corruption

  - Simulator screen is not in center but left aligned

  - Theme ,any F node which is a child of an E node is not working for
    Apertis widgets.

  - Video doesn't play when toggling from full screen to detail view

  - Videos are hidden when Eye is launched

  - Clutter_text_set_text API redraws entire clutterstage

  - The background HMI is blank on clicking the button for Power OFF

  - bluetooth device pair failing with "Invalid arguments in method
    call"

  - Background video is not played in some website with
    GtkClutterLauncher

  - mxkineticscrollview-smooth-pan:Free scroll doesn't work

  - Only half of the hmi is covered when opening gnome.org

  - Power button appers to be disabled on target

  - Mildenhall compositor crops windows

  - gstreamer1-0-decode: Failed to load plugin warning

### Lowest (2)

  - Broken HTML generation in Backworth for the developers portal
    website

  - Failed to load Captcha in Apertis developer portal
