+++
date = "2021-03-18"
weight = 100

title = "v2019.6 Release notes"
+++

Apertis is a Debian derivative distribution geared towards the creation
of product-specific images for ARM (both the 32bit ARMv7 and 64-bit
ARMv8 versions using the hardfloat ABI) and Intel x86-64 (64-bit)
systems.

**Apertis v2019.6** is the seventh **stable** release of the Apertis
v2019 [release flow]( {{< ref "release-flow.md#apertis-release-flow" >}} ).
Apertis is committed to maintaining the v2019 release stream until June
2021.

This Apertis release is built on top of Debian Buster with several
customisations and the Linux kernel 4.19.x LTS series.

Test results for the v2019.6 release are available in the following test
reports:

  - [APT     images](https://lavaphabbridge.apertis.org/report/v2019/20210223.0)
  - [OSTree  images](https://lavaphabbridge.apertis.org/report/v2019/20210223.0/ostree)
  - [NFS  artifacts](https://lavaphabbridge.apertis.org/report/v2019/20210223.0/nfs)
  - [LXC containers](https://lavaphabbridge.apertis.org/report/v2019/20210223.0/lxc)

Point releases including all the security fixes accumulated will be
published quarterly, up to v2019.7.

## Release flow

  - 2019 Q1: v2019dev0
  - 2019 Q2: v2019pre
  - 2019 Q3: v2019.0
  - 2019 Q4: v2019.1
  - 2020 Q1: v2019.2
  - 2020 Q2: v2019.3
  - 2020 Q3: v2019.4
  - 2020 Q4: v2019.5
  - 2021 Q1: **v2019.6**
  - 2021 Q2: v2019.7

### Release downloads

| [Apertis v2019.6 images](https://images.apertis.org/release/v2019/v2019.6/) | | | | |
| --------------------------------------------------------------------------- |-|-|-|-|
| Intel 64-bit        | [minimal](https://images.apertis.org/release/v2019/v2019.6/amd64/minimal/apertis_v2019-minimal-amd64-uefi_v2019.6.img.gz) | [target](https://images.apertis.org/release/v2019/v2019.6/amd64/target/apertis_v2019-target-amd64-uefi_v2019.6.img.gz) | [base SDK](https://images.apertis.org/release/v2019/v2019.6/amd64/basesdk/apertis_v2019-basesdk-amd64-sdk_v2019.6.vdi.gz) | [SDK](https://images.apertis.org/release/v2019/v2019.6/amd64/sdk/apertis_v2019-sdk-amd64-sdk_v2019.6.vdi.gz)
| ARM 32-bit (U-Boot) | [minimal](https://images.apertis.org/release/v2019/v2019.6/armhf/minimal/apertis_v2019-minimal-armhf-uboot_v2019.6.img.gz)
| ARM 64-bit (U-Boot) | [minimal](https://images.apertis.org/release/v2019/v2019.6/arm64/minimal/apertis_v2019-minimal-arm64-uboot_v2019.6.img.gz)

The Intel `minimal` and `target` images are tested on the
[reference hardware (MinnowBoard MAX)]( {{< ref "/reference_hardware/_index.md" >}} ),
but they can run on any UEFI-based x86-64 system. The `basesdk` and `sdk` images are
[tested under VirtualBox]( {{< ref "/virtualbox.md" >}} ).

#### Apertis v2019 repositories

    deb https://repositories.apertis.org/apertis/ v2019 target development sdk hmi
    deb https://repositories.apertis.org/apertis/ v2019-security target development sdk hmi

## Changes

This is a point release in the stable cycle, only security fixes and
small changes are appropriate for this release stream.

This release includes the security updates from Debian Buster.

## Deprecations and ABI/API breaks

Being a point release, no new deprecations or ABI breaks are part of
this release

## Infrastructure

### Apertis Docker registry

The Apertis Docker registry stores Docker images in order to provide a
unified and easily reproducible build environment for developers and
services.

As of today, this includes the `apertis-image-builder`,
`apertis-package-builder`, `apertis-package-source-builder`,
`apertis-testcases-builder` and `apertis-documentation-builder` Docker
images.

### Apertis infrastructure tools

The
[Apertis v2019 infrastructure repository](https://build.collabora.co.uk/project/users/apertis:infrastructure:v2019)
provides packages for the required versions of `ostree-push` and
`ostree` for Debian Buster:

    deb https://repositories.apertis.org/infrastructure-v2019/ buster infrastructure

### Images

Image daily builds, as well as release builds can be found at:

  https://images.apertis.org/

Image build tools can be found in the Apertis tools repositories.

### Infrastructure overview

The
[Image build infrastructure document]( {{< ref "image-build-infrastructure.md" >}} )
provides an overview of the image building process and the involved
services.

## Known issues

### High (5)
 - [T7014](https://phabricator.apertis.org/T7014)	bluez-phone: Message Access Profile test_profile_map_mse fails 
 - [T7623](https://phabricator.apertis.org/T7623)	apparmor-pulseaudio: test failed
 - [T7729](https://phabricator.apertis.org/T7729)	Failed to attach Bluetooth dongle to SDK and BaseSDK 
 - [T7767](https://phabricator.apertis.org/T7767)	DNS/name resolution issue with AMD64 OSTree based images
 - [T7783](https://phabricator.apertis.org/T7783)	ade-commands: test failed

### Normal (66)
 - [T2896](https://phabricator.apertis.org/T2896)	Crash when initialising egl on ARM target
 - [T2930](https://phabricator.apertis.org/T2930)	Develop test case for out of screen events in Wayland images
 - [T3210](https://phabricator.apertis.org/T3210)	Fix Tracker testcase to not download media files from random HTTP user folders
 - [T3233](https://phabricator.apertis.org/T3233)	Ribchester: deadlock when calling RemoveApp() right after RollBack()
 - [T3321](https://phabricator.apertis.org/T3321)	libgles2-vivante-dev is not installable
 - [T3920](https://phabricator.apertis.org/T3920)	arm-linux-gnueabihf-pkg-config does not work with sysroots installed by `ade`
 - [T4092](https://phabricator.apertis.org/T4092)	Containers fail to load on Gen4 host
 - [T4293](https://phabricator.apertis.org/T4293)	Preseed action is needed for Debos
 - [T4307](https://phabricator.apertis.org/T4307)	ribchester-core causes apparmor denies on non-btrfs minimal image
 - [T4422](https://phabricator.apertis.org/T4422)	do-branching fails at a late stage cloning OBS binary repos
 - [T4444](https://phabricator.apertis.org/T4444)	A 2-3 second lag between the speakers is observed when a hfp connection is made over bluetooth
 - [T4568](https://phabricator.apertis.org/T4568)	Ribchester mount unit depends on Btrfs
 - [T4660](https://phabricator.apertis.org/T4660)	Eclipse Build is not working for HelloWorld App
 - [T4693](https://phabricator.apertis.org/T4693)	Not able to create namespace for AppArmor container on the internal mx6qsabrelite images with proprietary kernel
 - [T5487](https://phabricator.apertis.org/T5487)	Wi-Fi search button is missing in wifi application
 - [T5748](https://phabricator.apertis.org/T5748)	System users are shipped in /usr/etc/passwd instead of /lib/passwd
 - [T5863](https://phabricator.apertis.org/T5863)	Songs/Videos don't play on i.MX6 with Frampton on internal images
 - [T5896](https://phabricator.apertis.org/T5896)	sdk-dbus-tools-bustle testcase is failing 
 - [T5897](https://phabricator.apertis.org/T5897)	apparmor-ofono test fails
 - [T5900](https://phabricator.apertis.org/T5900)	evolution-sync-bluetooth test fails
 - [T5901](https://phabricator.apertis.org/T5901)	eclipse-plugins-apertis-management package is missing
 - [T5906](https://phabricator.apertis.org/T5906)	Video does not stream in WebKit on the i.MX6 internal images 
 - [T5931](https://phabricator.apertis.org/T5931)	connman-usb-tethering test fails
 - [T5993](https://phabricator.apertis.org/T5993)	rhosydd: 8_rhosydd test failed
 - [T6001](https://phabricator.apertis.org/T6001)	eclipse-plugins-remote-debugging test fails
 - [T6008](https://phabricator.apertis.org/T6008)	The pacrunner package used for proxy autoconfiguration is not available
 - [T6024](https://phabricator.apertis.org/T6024)	folks-inspect: command not found 
 - [T6052](https://phabricator.apertis.org/T6052)	Multimedia playback is broken on the internal i.MX6 images (internal 3.14 ADIT kernel issue) 
 - [T6077](https://phabricator.apertis.org/T6077)	youtube Videos are not playing on upstream webkit2GTK
 - [T6078](https://phabricator.apertis.org/T6078)	Page scroll is lagging in Minibrowser on upstream webkit2GTK
 - [T6111](https://phabricator.apertis.org/T6111)	traprain: 7_traprain test failed
 - [T6243](https://phabricator.apertis.org/T6243)	AppArmor ubercache support is no longer enabled after 18.12
 - [T6291](https://phabricator.apertis.org/T6291)	Generated lavaphabbridge error report email provides wrong link for full report link 
 - [T6292](https://phabricator.apertis.org/T6292)	gettext-i18n: test failed
 - [T6349](https://phabricator.apertis.org/T6349)	sdk-code-analysis-tools-splint: 3_sdk-code-analysis-tools-splint test failed
 - [T6366](https://phabricator.apertis.org/T6366)	sdk-cross-compilation: 10_sdk-cross-compilation test failed
 - [T6369](https://phabricator.apertis.org/T6369)	apparmor-gstreamer1-0: test failed
 - [T6620](https://phabricator.apertis.org/T6620)	Repeatedly plugging and unplugging a USB flash drive on i.MX6 (Sabrelite) results in USB failure
 - [T6662](https://phabricator.apertis.org/T6662)	SDK: command-not-found package is broken
 - [T6669](https://phabricator.apertis.org/T6669)	Stop building cross compilers tools and libraries for not supported platforms 
 - [T6768](https://phabricator.apertis.org/T6768)	Fix the kernel command line generation in OSTRee for FIT image
 - [T6773](https://phabricator.apertis.org/T6773)	HAB testing: the unsigned image may pass validation in several circumstances
 - [T6783](https://phabricator.apertis.org/T6783)	Kernel  trace on armhf board with attached screen
 - [T6795](https://phabricator.apertis.org/T6795)	SabreLite failing to boot due to failing "to start udev Coldplug all Devices"
 - [T6806](https://phabricator.apertis.org/T6806)	HAB on SabreLite in open state accepts any signed kernel regardless of the signing key
 - [T6885](https://phabricator.apertis.org/T6885)	gitlab-rulez fails to set location of the gitlab-ci.yaml on first run
 - [T6887](https://phabricator.apertis.org/T6887)	ARM64 target does not reboot automatically
 - [T6961](https://phabricator.apertis.org/T6961)	audio-backhandling feature fails 
 - [T7000](https://phabricator.apertis.org/T7000)	DNS resolution does not work in Debos on some setups
 - [T7012](https://phabricator.apertis.org/T7012)	Apparmor Denied session logs keep popping up on the terminal while executing tests 
 - [T7016](https://phabricator.apertis.org/T7016)	network proxy for browser application is not resolving on mildenhall-compositor 
 - [T7127](https://phabricator.apertis.org/T7127)	apparmor-functional-demo:  test failed
 - [T7128](https://phabricator.apertis.org/T7128)	apparmor-session-lockdown-no-deny
 - [T7129](https://phabricator.apertis.org/T7129)	apparmor-tumbler: test failed
 - [T7308](https://phabricator.apertis.org/T7308)	sdk-vb-fullscreen testcase link needs to be changed for v2019 
 - [T7333](https://phabricator.apertis.org/T7333)	apparmor-geoclue: test failed
 - [T7340](https://phabricator.apertis.org/T7340)	newport: test failed
 - [T7503](https://phabricator.apertis.org/T7503)	Failed to unmount /usr on ostree-based images: Device or resource busy log is seen on rebooting
 - [T7512](https://phabricator.apertis.org/T7512)	debos sometimes fails to mount things
 - [T7530](https://phabricator.apertis.org/T7530)	ADE can't download amd64 sysroot.
 - [T7617](https://phabricator.apertis.org/T7617)	frome: test failed
 - [T7694](https://phabricator.apertis.org/T7694)	FOSSology re-use scan results betwenn pkg/dash and test/dash
 - [T7697](https://phabricator.apertis.org/T7697)	LAVA jobs running out of disk space when unpacking overlays for SDK images
 - [T7776](https://phabricator.apertis.org/T7776)	On executing system-update test on hawkbit-agent wrong delta is selected
 - [T7785](https://phabricator.apertis.org/T7785)	DNS over TLS does not work on systemd-resolve
