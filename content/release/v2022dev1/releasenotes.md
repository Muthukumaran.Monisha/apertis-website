+++
date = "2021-03-28"
weight = 100

title = "v2022dev1 Release Notes"
+++

Apertis is a Debian derivative distribution geared towards the creation
of product-specific images for ARM (both the 32bit ARMv7 and 64-bit
ARMv8 versions using the hardfloat ABI) and Intel x86-64 (64-bit)
systems.

**Apertis v2022dev1** is the second **development** release of the Apertis
v2022 stable release flow that will lead to the LTS **Apertis v2022.0**
release in March 2022.

This Apertis release is still built on top of Debian Buster with several
customisations and the Linux kernel 5.10.x LTS series. From the next
release the v2022 channel will be based on Debian Bullseye and will
track newer kernel versions up to the next LTS scheduled at the end of 2021.

Test results for the v2022dev1 release are available in the following
test reports:

  - [APT images](https://lavaphabbridge.apertis.org/report/v2022dev1/20210314.0016)
  - [OSTree images](https://lavaphabbridge.apertis.org/report/v2022dev1/20210314.0016/ostree)
  - [NFS artifacts](https://lavaphabbridge.apertis.org/report/v2022dev1/20210314.0016/nfs)
  - [LXC containers](https://lavaphabbridge.apertis.org/report/v2022dev1/20210314.0016/lxc)

## Release flow

  - 2020 Q4: v2022dev0
  - **2021 Q1: v2022dev1**
  - 2021 Q2: v2022dev2
  - 2021 Q3: v2022dev3
  - 2021 Q4: v2022pre
  - 2022 Q1: v2022.0
  - 2022 Q2: v2022.1
  - 2022 Q3: v2022.2
  - 2022 Q4: v2022.3
  - 2023 Q1: v2022.4
  - 2023 Q2: v2022.5
  - 2023 Q3: v2022.6
  - 2023 Q4: v2022.7

### Release downloads

| [Apertis v2022dev1 images](https://images.apertis.org/release/v2022dev1/) | | | | |
| ------------------------------------------------------------------------- |-|-|-|-|
| Intel 64-bit		| [minimal](https://images.apertis.org/release/v2022dev1/v2022dev1.0/amd64/minimal/apertis_v2022dev1-minimal-amd64-uefi_v2022dev1.0.img.gz) | [target](https://images.apertis.org/release/v2022dev1/v2022dev1.0/amd64/target/apertis_v2022dev1-target-amd64-uefi_v2022dev1.0.img.gz) | [base SDK](https://images.apertis.org/release/v2022dev1/v2022dev1.0/amd64/basesdk/apertis_v2022dev1-basesdk-amd64-sdk_v2022dev1.0.vdi.gz) | [SDK](https://images.apertis.org/release/v2022dev1/v2022dev1.0/amd64/sdk/apertis_v2022dev1-sdk-amd64-sdk_v2022dev1.0.vdi.gz)
| ARM 32-bit (U-Boot)	| [minimal](https://images.apertis.org/release/v2022dev1/v2022dev1.0/armhf/minimal/apertis_v2022dev1-minimal-armhf-uboot_v2022dev1.0.img.gz) | [target](https://images.apertis.org/release/v2022dev1/v2022dev1.0/armhf/target/apertis_v2022dev1-target-armhf-uboot_v2022dev1.0.img.gz)
| ARM 64-bit (U-Boot)	| [minimal](https://images.apertis.org/release/v2022dev1/v2022dev1.0/arm64/minimal/apertis_v2022dev1-minimal-arm64-uboot_v2022dev1.0.img.gz)
| ARM 64-bit (Raspberry Pi)	| [minimal](https://images.apertis.org/release/v2022dev1/v2022dev1.0/arm64/minimal/apertis_v2022dev1-minimal-arm64-rpi64_v2022dev1.0.img.gz) | [target](https://images.apertis.org/release/v2022dev1/v2022dev1.0/arm64/target/apertis_v2022dev1-target-arm64-rpi64_v2022dev1.0.img.gz)

The Intel `minimal` and `target` images are tested on the
[reference hardware (MinnowBoard Turbot Dual-Core)]( {{< ref "/reference_hardware/amd64.md" >}} ),
but they can run on any UEFI-based x86-64 system. The `sdk` image is
[tested under VirtualBox]( {{< ref "/virtualbox.md" >}} ).

#### Apertis v2022dev1 repositories

    deb https://repositories.apertis.org/apertis/ v2022dev1 target development sdk hmi

## Changes

### Replace GnuTLS/Nettle/GMP with GPL-3-free alternative on target images

Apertis has stricter [licensing expectations]({{< ref "license-expectations.md" >}})
than Debian, in particular around preventing GPL-3 code from being shipped on
target devices. One of the many challenging topics is the use of the GnuTLS
stack, as it depends on the GMP library which is released under a dual
GPL-2/LGPL-3 license. This means that on Apertis devices the GnuTLS stack can
only be used by GPL-2 programs, or the GPL-3 provisions would apply.

As a temporary solution to avoid that, previous releases shipped older
version of the GnuTLS/GMP stack, prior to the change of license.

The [License-compliant TLS stack for Apertis targets]({{< ref "tls-stack.md" >}})
document analized the available options to move forward and describes why
Apertis joins the ranks of Red Hat, Fedora and Debian in considering OpenSSL
matching the GPL platform library exceptions and thus suitable to be linked
from GPL programs.

For this reason from this releases a selection of packages has been switched
from their GnuTLS backend to the OpenSSL one, to be later able to upgrade the
GnuTLS stack without being restricted by the GPL-3 provisions.

### Technology preview: Licensing reports for images

As part of the image building pipeline Apertis now also produces detailed
reports listing the licenses covering the binaries shipped on each image.

The extracted information is stored in a per-image JSON file which documents
the licenses that affect each binary program or library shipped in the image
itself and that can be easily used to generate human-readable reports or to
add automated checks to ensure compliance to licensing policies.

This is a sample output:

```json
{
  "image_licenses": [
    "LGPL-2+", "BSD-3-clause", "NoLicenseReportFound", "GPL-2+",
    "BSD-3-clause or LGPL-2.1+", "Expat", "Sleepycat", "GPL-1+",
    "BSD-3-clause or LGPL-2+", "GPL-2", "OLDAP-2.8", "LGPL-2.1+",
    "GPL", "BSD-2-clause", "MPL-2.0", "public-domain",
    "BSD~unspecified", "LGPL", "BSD-4-clause", "LGPL-2"
  ],
  "packages": [
    {
      "package_name": "systemd-boot",
      "package_licenses": [ "LGPL-2.1+" ],
      "binaries": [
        {
          "binary_name": "usr/bin/bootctl",
          "binary_licenses": [ "LGPL-2.1+" ],
          "sources": []
        }
      ]
    },

```

### Infrastructure status page

Apertis has a complex infrastructure with many services. When hitting issues it
is helpful to check whether it is a local problem or if something is broken in
the infrastructure itself.

To help developers diagnose these issues and to help administrators to promptly
react on unscheduled downtimes some
[monitoring services have been reviewed]({{< ref "status-page-review.md" >}})
and a status page keeping track of the health of all the Apertis services is
now available at [status.apertis.org](https://status.apertis.org/).

### Improved SDK persistent disk tests

The manual tests excercising the
[persistent SDK workspaces]({{< ref "sdk-persistent-workspace.md" >}}) have
been substantially improved.

They are now much easier to run as many previously manual steps are now
automated, and they have also been made much more robust. This has been part of
the ongoing effort to make our testing activities as efficient as possible,

## Documentation

### Document strategies to deal with configuration files during updates and rollbacks

Dealing with configuration files during an update and during a rollback is a
common problem for product teams.

The [System updates and rollback]({{< ref "system-updates-and-rollback.md" >}}) document
[has been extended](https://gitlab.apertis.org/docs/apertis-website/-/merge_requests/153)
to discuss the known limitations and best practices of configuration management
for those scenarios.

### Uniform security using OP-TEE

OP-TEE is an open source project for a Trusted Execution Environment and, given
the growing interest in the technology in the context of virtualization, the
[OP-TEE integration document]({{< ref ""op-tee.md >}}) has
[been extended](https://gitlab.apertis.org/docs/apertis-website/-/merge_requests/167)
to describe how Apertis and OP-TEE can work together beyond the simplest
use cases.

The design document reflects the vision of uniform and consistent use across
various scenarios including running on bare metal and running inside a virtual
machine, with the technical and practical limitations to achieve this vision.

### Evaluate GPL-3-free replacements of GnuPG

After the documents covering replacements for
[the GnuTLS stack]({{< ref "tls-stack.md" >}}) and
[coreutils]({{< ref "coreutils-replacement.md" >}}) to match our
[licensing expectations]({{< ref "license-expectations.md" >}}), a new document
does the same to [replace GnuPG]({{}< ref "gnupg-replacement.md" >}).

### Plan for deployment management reference implementation

In addition to the documents covering the high level approach for
[software distribution and updates]({{< ref "software-distribution-and-updates.md" >}})
and [how to handle that wiht hawkBit]({{< ref "deployment-management.md" >}})
a new document describes the steps to move from the current experimental setup to a
[production-grade hawkBit deployment]({{< ref "preparing-hawkbit-for-production.md" >}}).

## Deprecations and ABI/API breaks

### Regressions

No known regressions.

### Deprecations

This is the last v2022 release based on Debian Buster: the next development
release will switch to Debian Bullseye.

### Breaks

No known breaking changes are part of this release.

## Infrastructure

### Apertis Docker images

The Apertis Docker images provide a unified and easily reproducible build
environment for developers and services.

As of today, this includes the
[`apertis-base`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022dev1-base),
[`apertis-image-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022dev1-image-builder),
[`apertis-package-source-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022dev1-package-source-builder),
[`apertis-flatdeb-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022dev1-flatdeb-builder),
[`apertis-documentation-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022dev1-documentation-builder),
and [`apertis-testcases-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022dev1-testcases-builder),
Docker images.

### Apertis infrastructure tools

The [Apertis v2022 infrastructure repository](https://build.collabora.co.uk/project/users/apertis:infrastructure:v2022)
provides packages for the required versions of `ostree-push` and
`ostree` for Debian Buster:

    deb https://repositories.apertis.org/infrastructure-v2022/ buster infrastructure

### Images

Image daily builds, as well as release builds can be found at https://images.apertis.org/

Image build tools can be found in the Apertis tools repositories.

### Infrastructure overview

The
[Image build infrastructure document]( {{< ref "image-build-infrastructure.md" >}} )
provides an overview of the image building process and the involved
services.

## Known issues
### High (9)
 - [T7014](https://phabricator.apertis.org/T7014)	bluez-phone: Message Access Profile test_profile_map_mse fails when there are many contacts
 - [T7623](https://phabricator.apertis.org/T7623)	apparmor-pulseaudio: test failed
 - [T7729](https://phabricator.apertis.org/T7729)	Failed to attach Bluetooth dongle on VirtualBox 6.1.16
 - [T7781](https://phabricator.apertis.org/T7781)	aum-ota-api: test failed due to new bootcount test needing a newer uboot
 - [T7783](https://phabricator.apertis.org/T7783)	ade-commands: test failed
 - [T7801](https://phabricator.apertis.org/T7801)	agl-compositor test fails
 - [T7802](https://phabricator.apertis.org/T7802)	flatpak install -y org.apertis.mildenhall.Platform org.apertis.mildenhall.Sdk
 - [T7803](https://phabricator.apertis.org/T7803)	sdk-debos-image-building: test failed with "Failed to register machine: Unit machine-root.scope already exists"
 - [T7816](https://phabricator.apertis.org/T7816)	Polling issues faced in apertis hawkbit agent

### Normal (62)
 - [T2896](https://phabricator.apertis.org/T2896)	Crash when initialising egl on ARM target
 - [T2930](https://phabricator.apertis.org/T2930)	Develop test case for out of screen events in Wayland images
 - [T3210](https://phabricator.apertis.org/T3210)	Fix Tracker testcase to not download media files from random HTTP user folders
 - [T3233](https://phabricator.apertis.org/T3233)	Ribchester: deadlock when calling RemoveApp() right after RollBack()
 - [T3321](https://phabricator.apertis.org/T3321)	libgles2-vivante-dev is not installable
 - [T3920](https://phabricator.apertis.org/T3920)	arm-linux-gnueabihf-pkg-config does not work with sysroots installed by `ade`
 - [T4092](https://phabricator.apertis.org/T4092)	Containers fail to load on Gen4 host
 - [T4293](https://phabricator.apertis.org/T4293)	Preseed action is needed for Debos
 - [T4307](https://phabricator.apertis.org/T4307)	ribchester-core causes apparmor denies on non-btrfs minimal image
 - [T4422](https://phabricator.apertis.org/T4422)	do-branching fails at a late stage cloning OBS binary repos
 - [T4444](https://phabricator.apertis.org/T4444)	A 2-3 second lag between the speakers is observed when a hfp connection is made over bluetooth
 - [T4660](https://phabricator.apertis.org/T4660)	Eclipse Build is not working for HelloWorld App
 - [T4693](https://phabricator.apertis.org/T4693)	Not able to create namespace for AppArmor container on the internal mx6qsabrelite images with proprietary kernel
 - [T5487](https://phabricator.apertis.org/T5487)	Wi-Fi search button is missing in wifi application
 - [T5748](https://phabricator.apertis.org/T5748)	System users are shipped in /usr/etc/passwd instead of /lib/passwd
 - [T5863](https://phabricator.apertis.org/T5863)	Songs/Videos don't play on i.MX6 with Frampton on internal images
 - [T5896](https://phabricator.apertis.org/T5896)	sdk-dbus-tools-bustle testcase is failing
 - [T5897](https://phabricator.apertis.org/T5897)	apparmor-ofono test fails
 - [T5900](https://phabricator.apertis.org/T5900)	evolution-sync-bluetooth test fails
 - [T5901](https://phabricator.apertis.org/T5901)	eclipse-plugins-apertis-management package is missing
 - [T5906](https://phabricator.apertis.org/T5906)	Video does not stream in WebKit on the i.MX6 internal images
 - [T5931](https://phabricator.apertis.org/T5931)	connman-usb-tethering test fails
 - [T6001](https://phabricator.apertis.org/T6001)	eclipse-plugins-remote-debugging test fails
 - [T6008](https://phabricator.apertis.org/T6008)	The pacrunner package used for proxy autoconfiguration is not available
 - [T6024](https://phabricator.apertis.org/T6024)	folks-inspect: command not found
 - [T6052](https://phabricator.apertis.org/T6052)	Multimedia playback is broken on the internal i.MX6 images (internal 3.14 ADIT kernel issue)
 - [T6077](https://phabricator.apertis.org/T6077)	youtube Videos are not playing on upstream webkit2GTK
 - [T6078](https://phabricator.apertis.org/T6078)	Page scroll is lagging in Minibrowser on upstream webkit2GTK
 - [T6111](https://phabricator.apertis.org/T6111)	traprain: 7_traprain test failed
 - [T6243](https://phabricator.apertis.org/T6243)	AppArmor ubercache support is no longer enabled after 18.12
 - [T6291](https://phabricator.apertis.org/T6291)	Generated lavaphabbridge error report email provides wrong link for full report link
 - [T6292](https://phabricator.apertis.org/T6292)	gettext-i18n: test failed
 - [T6349](https://phabricator.apertis.org/T6349)	sdk-code-analysis-tools-splint: 3_sdk-code-analysis-tools-splint test failed
 - [T6366](https://phabricator.apertis.org/T6366)	sdk-cross-compilation: 10_sdk-cross-compilation test failed
 - [T6444](https://phabricator.apertis.org/T6444)	aum-update-rollback-tests/arm64,amd64: Automatic power cut test should be reworked to reduce the speed of delta read
 - [T6446](https://phabricator.apertis.org/T6446)	aum-update-rollback-tests/amd64: DNS not available in LAVA tests after reboot
 - [T6620](https://phabricator.apertis.org/T6620)	Repeatedly plugging and unplugging a USB flash drive on i.MX6 (Sabrelite) results in USB failure
 - [T6662](https://phabricator.apertis.org/T6662)	SDK: command-not-found package is broken
 - [T6669](https://phabricator.apertis.org/T6669)	Stop building cross compilers tools and libraries for not supported platforms
 - [T6768](https://phabricator.apertis.org/T6768)	Fix the kernel command line generation in OSTRee for FIT image
 - [T6773](https://phabricator.apertis.org/T6773)	HAB testing: the unsigned image may pass validation in several circumstances
 - [T6783](https://phabricator.apertis.org/T6783)	Kernel  trace on armhf board with attached screen
 - [T6795](https://phabricator.apertis.org/T6795)	SabreLite failing to boot due to failing "to start udev Coldplug all Devices"
 - [T6806](https://phabricator.apertis.org/T6806)	HAB on SabreLite in open state accepts any signed kernel regardless of the signing key
 - [T6885](https://phabricator.apertis.org/T6885)	gitlab-rulez fails to set location of the gitlab-ci.yaml on first run
 - [T6887](https://phabricator.apertis.org/T6887)	ARM64 target does not reboot automatically
 - [T6961](https://phabricator.apertis.org/T6961)	audio-backhandling feature fails
 - [T7000](https://phabricator.apertis.org/T7000)	DNS resolution does not work in Debos on some setups
 - [T7012](https://phabricator.apertis.org/T7012)	Apparmor Denied session logs keep popping up on the terminal while executing tests
 - [T7016](https://phabricator.apertis.org/T7016)	network proxy for browser application is not resolving on mildenhall-compositor
 - [T7127](https://phabricator.apertis.org/T7127)	apparmor-functional-demo:  test failed
 - [T7128](https://phabricator.apertis.org/T7128)	apparmor-session-lockdown-no-deny
 - [T7129](https://phabricator.apertis.org/T7129)	apparmor-tumbler: test failed
 - [T7333](https://phabricator.apertis.org/T7333)	apparmor-geoclue: test failed
 - [T7503](https://phabricator.apertis.org/T7503)	Failed to unmount /usr on ostree-based images: Device or resource busy log is seen on rebooting
 - [T7512](https://phabricator.apertis.org/T7512)	debos sometimes fails to mount things
 - [T7617](https://phabricator.apertis.org/T7617)	frome: test failed
 - [T7721](https://phabricator.apertis.org/T7721)	Fakemachine in debos immediately powers off and hangs in v2021 and v2022dev1 when using UML on the runners
 - [T7776](https://phabricator.apertis.org/T7776)	On executing system-update test on hawkbit-agent wrong delta is selected
 - [T7785](https://phabricator.apertis.org/T7785)	DNS over TLS does not work on systemd-resolve
 - [T7812](https://phabricator.apertis.org/T7812)	The dlt-daemon build in the devroot testcase fails due to lack of pandoc on Arm platforms
 - [T7815](https://phabricator.apertis.org/T7815)	scan-copyright fails to detect (L)GPL-3 code in util-linux
