+++
date = "2016-07-07"
weight = 100

title = "webkit-clutter-javascriptcore"

aliases = [
    "/old-wiki/QA/Test_Cases/webkit-clutter-javascriptcore"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/
