+++
date = "2014-12-09"
weight = 100

title = "bluez-core"

aliases = [
    "/old-wiki/QA/Test_Cases/bluez-core"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/
