+++
date = "2017-10-27"
weight = 100

title = "roller_scrolling_vertical"

aliases = [
    "/old-wiki/QA/Test_Cases/roller_scrolling_vertical"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/
