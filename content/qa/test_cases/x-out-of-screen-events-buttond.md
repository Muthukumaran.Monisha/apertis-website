+++
date = "2016-10-20"
weight = 100

title = "x-out-of-screen-events-buttond"

aliases = [
    "/old-wiki/QA/Test_Cases/x-out-of-screen-events-buttond"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/
