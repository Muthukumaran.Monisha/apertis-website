+++
date = "2018-06-25"
weight = 100

title = "libsoup-unit"

aliases = [
    "/qa/test_cases/libsoup.md",
    "/old-wiki/QA/Test_Cases/libsoup",
    "/old-wiki/QA/Test_Cases/libsoup-unit"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/
