+++
date = "2017-10-25"
weight = 100

title = "sdk-nested-wm"

aliases = [
    "/old-wiki/QA/Test_Cases/sdk-nested-wm"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/
