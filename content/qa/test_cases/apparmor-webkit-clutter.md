+++
date = "2016-09-22"
weight = 100

title = "apparmor-webkit-clutter"

aliases = [
    "/old-wiki/QA/Test_Cases/apparmor-webkit-clutter"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/
