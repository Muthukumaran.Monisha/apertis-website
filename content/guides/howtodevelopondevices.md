+++
date = "2018-10-17"
weight = 100

title = "How to Develop on Devices"

aliases = [
    "/old-wiki/Guidelines/HowToDevelopOnDevices"
]
+++

This document aims to explain how to develop on devices running the
reference Apertis images.

# Which image to choose

The APT-based images like
[`apertis_18.09-minimal-amd64-uefi_18.09.0.img.gz`](https://images.apertis.org/release/18.09/18.09.0/amd64/minimal/apertis_18.09-minimal-amd64-uefi_18.09.0.img.gz)
and
[`apertis_18.09-target-amd64-uefi_18.09.0.img.gz`](https://images.apertis.org/release/18.09/18.09.0/amd64/target/apertis_18.09-target-amd64-uefi_18.09.0.img.gz)
are the ones providing all the flexibility needed for development,
letting developers install and remove packages, and otherwise customize
the rootfs.

The OSTree-based images are tailored as a reference for product
deployments: their constraints make installing and removing packages
impossible, and the more care must be taken to get write access to the
rootfs managed by OSTree.

# Making a device suitable for development

Apertis device images come with some restriction out-of-the-box when
trying to develop directly on them:

- only the `target` APT repository is enabled by default, while most of the
  useful development tools are in the \`development\` repository
- the rootfs is mounted read-only
- some utilities are kept to a older, less featureful versions since the newer
  ones switched to GPLv3
- target images deliberately don't have development/debugging tools installed

To easily overcome these issues, Apertis images ship the `apertis-dev` tool. To
get a basic set of development tools (somewhere between the official target and
development images) you can log in on the serial console or use the xterm, and
run:

    $ sudo apertis-dev --help
    usage: sudo apertis-dev [OPTIONS]
    Enable debugging and basic development.
    OPTIONS are:
        -b|--build: install basic build tools
        -d|--build-dep PACKAGE: install build-deps of PACKAGE
        -g|--debug: install basic debugging tools
        -3|--gpl3: install GPL3 versions of tar, coreutils
        --enable-debug: enable debug logging from applications
        --disable-debug: disable debug logging from applications
    For developer use only: do not do this in production.

For instance, to install `gdbserver`, run:

    $ sudo apertis-dev
    $ sudo apt install gdbserver

Please note that a machine where this has been done is not a "pure" target
image; if a bug was reported for target images, please confirm the fix on a
target image without these development tools.


# Using SSH

Accessing the device via SSH is generally more convenient than using a serial
console or xterm. To do so, add the following to your `~/.ssh/config` file:

```
Match host apertis*
    User user
    IdentityFile ~/.ssh/apertis.ssh
    IdentitiesOnly yes
    StrictHostKeyChecking no
    UserKnownHostsFile /dev/null
```

Generate a new key which you will use for connecting to all Apertis target
images:

    $ ssh-keygen -t rsa -N "" -f ~/.ssh/apertis.ssh

Then, for each new target image installation you have, set up the key,
replacing `$hostname` with the board’s hostname:

    $ ssh-copy-id -i ~/.ssh/apertis.ssh.pub user@$hostname

You should then be able to SSH in using:

    $ ssh $hostname


# Building software

The recommended environment to build platform packages and application bundles
is the
[Apertis SDK]( {{< ref "software-development-kit.md" >}} ).

However, it is also possible to build software on the devices directly with the
preparation steps below:

    $ sudo apertis-dev --build --gpl3
    $ sudo apt install apertis-dev-tools

