+++
date = "2020-06-09"
lastmod = "2021-02-03"
weight = 100

title = "VirtualBox"

aliases = [
    "/old-developer/latest/install.html",
    "/old-developer/latest/vm-config.html",
    "/old-developer/latest/vm-setup.html",
    "/old-developer/v2019/install.html",
    "/old-developer/v2019/vm-config.html",
    "/old-developer/v2019/vm-setup.html",
    "/old-developer/v2020/install.html",
    "/old-developer/v2020/vm-config.html",
    "/old-developer/v2020/vm-setup.html",
    "/old-developer/v2021pre/install.html",
    "/old-developer/v2021pre/vm-config.html",
    "/old-developer/v2021pre/vm-setup.html",
    "/old-developer/v2022dev0/install.html",
    "/old-developer/v2022dev0/vm-config.html",
    "/old-developer/v2022dev0/vm-setup.html",
    "/old-wiki/Docs/VirtualBox_Guest_Additions",
    "/old-wiki/Docs/VirtualBox_guest_additions",
    "/old-wiki/Virtual_Machines",
    "/old-wiki/VirtualBox"
]
+++

The recommended virtual machine platform for the AMD64 Apertis system images is
[VirtualBox](https://www.virtualbox.org/). It is typical for the
[Apertis SDK]( {{< ref "images.md" >}} )
to be run in a virtual machine, though other image types can also be used. This
enables development to be performed on computers running Windows, Mac OS, or
different Linux distributions.

![300px|thumb|Apertis SDK image running in a VM](/images/vm-sdk.png)

# System requirements

You will need a PC with the following configuration to install and run the SDK:

## Hardware

- Dual core CPU at 2GHz or higher
- 8 GB RAM or more
- 12 GB or more free space on the hard disk

{{% notice info %}}
If your PC supports Virtualization Technology, make sure that it is enabled. If
problems are seen when booting the SDK image, check if Virtualization
Technology is enabled in BIOS settings.
{{% /notice %}}

## Software

- Windows OS
- Oracle VirtualBox. See supported version and installation instructions below.

### VirtualBox supported version

{{% notice warning %}}
While you can use VirtualBox in other environments, and even use other
virtualization solutions, the supported setup is to run VirtualBox on 
Microsoft Windows.
{{% /notice %}}

The following table contains the supported version of VirtualBox and VirtualBox Guest additions for each release of Apertis:

| Apertis release | VirtualBox version | VirtualBox Guest Additions version |
| ------ | ------ | ----- |
| v2019 | [6.1.12 r139181 (Qt5.6.2)](https://download.virtualbox.org/virtualbox/6.1.12/VirtualBox-6.1.12-139181-Win.exe) | [6.1.12](https://download.virtualbox.org/virtualbox/6.1.12/VBoxGuestAdditions_6.1.12.iso) |
| v2020 | [6.1.12 r139181 (Qt5.6.2)](https://download.virtualbox.org/virtualbox/6.1.12/VirtualBox-6.1.12-139181-Win.exe) | [6.1.12](https://download.virtualbox.org/virtualbox/6.1.12/VBoxGuestAdditions_6.1.12.iso) |
| v2021 | [6.1.12 r139181 (Qt5.6.2)](https://download.virtualbox.org/virtualbox/6.1.12/VirtualBox-6.1.12-139181-Win.exe) | [6.1.12](https://download.virtualbox.org/virtualbox/6.1.12/VBoxGuestAdditions_6.1.12.iso) |
| v2022 | [6.1.12 r139181 (Qt5.6.2)](https://download.virtualbox.org/virtualbox/6.1.12/VirtualBox-6.1.12-139181-Win.exe) | [6.1.12](https://download.virtualbox.org/virtualbox/6.1.12/VBoxGuestAdditions_6.1.12.iso) |

# Installing VirtualBox

If you have not yet installed Oracle VM VirtualBox, to install the current version of this software, please follow these steps:

- [Download](https://www.virtualbox.org/wiki/Downloads) the required version of
  the VirtualBox installation file for your host platform. Check the table of
  supported versions above to determine which version of VirtualBox is supported
  for the Apertis Release you want to use.

- Follow the installation procedure provided in the
  [VirtualBox installation guide](https://www.virtualbox.org/manual/ch02.html)
  for your host plaform.

{{% notice info %}}
For instructions on uninstalling VirtualBox, are also provided in the [user manual](https://www.virtualbox.org/manual/ch02.html).
{{% /notice %}}

# VirtualBox Setup

VirtualBox can be configured both from its GUI or via the command line.

## From the VirtualBox GUI

- If you have not already downloaded an Apertis SDK image, the
  [images]({{< ref "images.md" >}}) page contains information regarding the
  options available. SDK images provided explicitly for use with VirtualBox
  have the file extension `.vdi.gz`.

- Extract the gzipped VDI image file to a local folder on your PC. The image
  for the virtual machine is a single file.

- Start the VirtualBox application ("Oracle VM VirtualBox" in the Start menu).

    ![](/images/fig-1.png)

- Go to `Machine` ▸ `New` or click the **New** Icon. This launches the **Create
  New Virtual Machine** screen.

- Enter a name and select the following from the menus:
  - Type: `Linux`
  - Version: `Debian (64 bit)`

    ![](/images/fig-2.png)

- Select the RAM size. Change the values manually according to your
  requirements. Assign at least 50% RAM for the virtual machine if your total
  RAM is more than 2 GB, with 2048MB recommended as the minimum for the SDK.

    ![](/images/fig-3.png)

{{% notice warning %}}
Not enough RAM might cause problems while you use the SDK (for example, Eclipse
might crash).
{{% /notice %}}

- Select **Use an existing virtual hard drive file** and browse to the location
  of your unzipped file (which should have the extension `.vdi`).

    ![](/images/fig-4.png)

- Click **Create** to create the virtual machine.

- A few more settings need to be modified to ensure that the Apertis images boot. Select your new virtual machine and select `Settings...`.

    ![](/images/fig-5.png)

- Ensure that the following settings are set as required:
  - Check the `System` ▸ `Motherboard` ▸  `Enable EFI (special OSes only)`
    option
  - Check the `System` ▸ `Processor` ▸ `Enable PAE/NX` option
  - Set video memory, `Display` ▸ `Screen` ▸ `Video Memory`, 64MB is
    recommended
  - Be sure 3D Acceleration is **disabled**. Ensure
    `Display` ▸ `Screen` ▸ `Enable 3D Acceleration` is unchecked

    ![](/images/fig-6.png)

- If you want to start your virtual machine from the desktop without having to
  open the VirtualBox every time, you can create a desktop icon. Right-click
  the entry of your virtual machine on the left and choose **Create Shortcut on
  Desktop** from the menu.

    ![](/images/fig-9.png)

{{% notice info %}}
 If you would like additional information about creating a new virtual machine you can find it in the [VirtualBox manual](https://www.virtualbox.org/manual/ch01.html#gui-createvm)
{{% /notice %}}

## From the Command Line

- Run the following commands:

<!-- end list -->

    $ RELEASE=v2020
    $ REVISION=0
    $ wget https://images.apertis.org/release/$RELEASE/$RELEASE.$REVISION/amd64/sdk/apertis_$RELEASE-sdk-amd64-sdk_$RELEASE.$REVISION.vdi.gz
    --2020-06-09 16:20:04--  https://images.apertis.org/release/v2020/v2020.0/amd64/sdk/apertis_v2020-sdk-amd64-sdk_v2020.0.vdi.gz
    Resolving images.apertis.org (images.apertis.org)... 2a00:1098:0:82:1000:25:2eeb:e3bc, 46.235.227.188
    Connecting to images.apertis.org (images.apertis.org)|2a00:1098:0:82:1000:25:2eeb:e3bc|:443... connected.
    HTTP request sent, awaiting response... 200 OK
    Length: 2363044547 (2.2G) [application/octet-stream]
    Saving to: ‘apertis_v2020-sdk-amd64-sdk_v2020.0.vdi.gz’
    
    apertis_v2020-sdk-a 100%[===================>]   2.20G  6.10MB/s    in 5m 57s  
    
    2020-06-09 16:26:01 (6.32 MB/s) - ‘apertis_v2020-sdk-amd64-sdk_v2020.0.vdi.gz’ saved [2363044547/2363044547]
    
    $ gunzip apertis_$RELEASE-sdk-amd64-sdk_$RELEASE.$REVISION.vdi.gz
    $ VDIFILE=./apertis_$RELEASE-sdk-amd64-sdk_$RELEASE.$REVISION.vdi
    $ VMNAME="Apertis $RELEASE.$REVISION SDK"
    $ vboxmanage createvm --register --name "$VMNAME" --ostype Debian_64
    Virtual machine 'Apertis v2020.0 SDK' is created and registered.
    UUID: 6370548c-2a11-4fb8-9411-5dc2ae686a8f
    Settings file: '/home/user/VirtualBox VMs/Apertis v2020.0 SDK/Apertis v2020.0 SDK.vbox'
    $ vboxmanage modifyvm "$VMNAME" --memory 2048 --apic on --pae on --largepages off --firmware efi --accelerate3d off --vram 64
    $ vboxmanage modifyvm "$VMNAME" --nictype1 virtio
    $ vboxmanage storagectl "$VMNAME" --name SATA --add sata
    $ vboxmanage storageattach "$VMNAME" --storagectl SATA --port 0 --type hdd --medium "$VDIFILE"
    $ vboxmanage storageattach "$VMNAME" --storagectl SATA --port 1 --type dvddrive --medium emptydrive
    $ vboxsdl --startvm "$VMNAME"
    Oracle VM VirtualBox SDL GUI version 6.0.22
    (C) 2005-2020 Oracle Corporation
    All rights reserved.

# Start the virtual machine for the first time

Use your just created desktop shortcut, or click **Start** in VirtualBox to
start the virtual machine. The boot-up-process might take a few seconds.

On starting the virtual machine, VirtualBox might display some popup windows
informing you about mouse, keyboard and color settings which might be different
on the VM. Please read through the messages and click **OK** for all of them.
If you check the **Do not show this message again** checkbox, you can
permanently disable these popup messages for this virtual machine.

{{% notice note %}}
The VirtualBox manual provides
[more information](https://www.virtualbox.org/manual/ch01.html#keyb_mouse_normal)
on the mouse and keyboard settings.
{{% /notice %}}

# Guest Additions under VirtualBox SDK images

Guest additions consist of device drivers and system applications that optimize
the guest operating system for better performance and usability. They are
designed to be installed inside a virtual machine after the guest operating
system has been installed. For more information on the features provided by
guest additions, see the
[VirtualBox manual](http://www.virtualbox.org/manual/ch04.html).

In the context of the Apertis project, guest additions allow developers to
enable full screen rendering within VirtualBox SDK images.  Full screen is not
the only reason to install guest additions, though. Shared folders are another
very handy feature.

## Installation

{{% notice info %}}
It is necessary to install guest additions corresponding to the virtual box
version installed.
{{% /notice %}}
{{% notice warning %}}
Make sure to backup any important data you may have in your virtual machine
before installing Guest Addtions. Some revisions of the Guest Additions have
been known to cause issues with the Apertis SDK images.
{{% /notice %}}

- Start your VirtualBox Apertis machine SDK image
- Go to `Devices` ▸ `Insert Guest Additions CD Image…` on the VirtualBox menu
  bar. A virtual device will appear on the desktop.

![](/images/fig-16.png)

![](/images/fig-17.png)

- Double-click on the `VBOXADDITIONS` CD icon which should appear on your
  guest desktop. This will launch a file browser.
- Open a terminal by **right click** ▸ `open terminal` in the guest additions folder
- Run the Linux Guest Additions installation script:

      $ sudo ./VBoxLinuxAdditions.run

![](/images/fig-18.png)

- Reboot your virtual machine
- Verify a new directory is created under `/opt` with guest additions or
  verify the `vboxguest` module is loaded

You can now enjoy guest additions' enhanced features.

Once the Guest Additions are installed successfully (the process might take a
few minutes), restart the virtual machine, see
[here]( {{< ref "sdk-usage.md#shutting-down--restarting-the-virtual-machine" >}} )
if you need help with that.

## Setting up shared folders

Go to **Settings** and click **Shared Folders** and select **Add Shared
Folders** (use the icon on the left hand side). Browse to the path you created
your share folder in (e.g. `C:\SHARE`). Click **OK** and close the Settings
window. The VirtualBox VM Settings can only be edited when the VM is closed. So
please close all VMs if any should be running, before setting up a shared
folder.

![](/images/fig-7.png)

![](/images/fig-8.png)

Once you start the virtual machine, go to **ApplicationsTerminal Emulator** and
run this command in the terminal:

    $ sudo mount –t vboxsf HOST_DIR_NAME GUEST_DIR_NAME

E.g:

    $ sudo mount –t vboxsf SHARE /mnt

This command will mount the share folder to the current `/mnt`.

## Adjusting virtual machine window size

Once the guest additions are installed, the window size can be changed. Select the option **Adjust Window Size**. Then resize the window to make it appear as a normal working size.

![](/images/fig-15.png)

## Put the virtual machine in Fullscreen mode

You can switch the display of the SDK to fullscreen mode by selecting **View** in the VirtualBox menu and choosing **Switch to Fullscreen**.

![](/images/fig-15.png)

You can still access the most important options of VirtualBox in the menu that appears at the bottom of the screen when you get close to it with your cursor.

# Uninstall the virtual machine

To uninstall the virtual machine, open VirtualBox and right click the machine
you want to remove. Chose **Remove** from the menu.

![](/images/fig-13.png)

In the following dialog you can decide if you want to remove the virtual
machine from VirtualBox or if you want to delete the files containing the
virtual machine from your hard drive as well. This will remove the hard drive
of the virtual machine and all files saved there. Please make sure to make
backups of the files you still need before deleting all files. **Remove only**
will just delete the virtual machine from VirtualBox but leave the files
containing the virtual machine intact.

# Non-SDK images

We recommend running minimal, target and development images on
[real hardware]( {{< ref "/reference_hardware/_index.md" >}} ), but VirtualBox
can run our `amd64` images.

- Download the `.img.gz` and `.img.bmap` files for the image you need from the
  [image repository]( {{< ref "/images.md" >}} )
- Expand the downloaded `.img.gz` file:
  - If you have `bmaptool` (recommended), use that to create a sparse file:

        $ bmaptool copy filename.img.gz apertis.img

  - If not, expand the downloaded `.img.gz` file (this will be slower):

        $ gunzip -c filename.img.gz > apertis.img

- Convert the image to VirtualBox format:

      $ vboxmanage convertfromraw apertis.img apertis.vdi --format VDI

- You can delete the temporary `.img` file now
- Create a new VM in the VirtualBox Manager
  - Select `Use an existing virtual hard disk file` and chose the `.vdi` file
  - Modify Settings, mostly the same as for the SDK (see above)
    - `Base Memory` can be smaller for these images: 1024M is recommended
