+++
date = "2019-10-25"
weight = 100

title = "Development Process"
+++

The process for contributing code to Apertis is the same as for many other open
source projects: check out the code, make changes locally, then submit them for
review and merging. Apertis uses its GitLab instance as a code repository and
review tool.

{{% notice note %}}
This guide assumes good working knowledge of git (see
[Version control]( {{< ref "version_control.md" >}} ) for
specific guidelines) and an account with commit rights on GitLab (see
[Getting commit rights]( {{< ref "contributions.md#getting-commit-rights" >}} )
for guidance).
{{% /notice %}}

## Creating an Apertis GitLab account

The Apertis GitLab instance is
[open for registration](https://gitlab.apertis.org/users/sign_up). An account
on the Apertis GitLab instance is required to follow the development process
detailed below.

{{% notice note %}}
By creating an account you signify that you accept the Apertis
[Privacy Policy]({{< ref "new_privacy_policy" >}}) and
[Terms of Use]({{% ref "terms_of_use.md" %}})
{{% /notice %}}

## Fork the repository to private namespace

Before commencing any work, the developer should fork the repository into their namespace
![](/images/gitlab-fork.png)

## Clone

Clone the forked repository

* Check out the git repository:

      git clone https://gitlab.apertis.org/[group name]/[repository name].git

* Commit the changes (see the
  [guidelines for making commits]( {{< ref "version_control.md#guidelines-for-making-commits" >}} )
  ), test them and check you are allowed to submit them under the project’s
  license, following the
  [sign-offs]( {{< ref "contributions.md#sign-offs" >}} ) documentation

      git commit -i -s

* Every commit must have an appropriate
  [`Signed-off-by:` tag]( {{< ref "contributions.md#sign-offs" >}} ) in
  the commit message.

## Merge Request

* Once the changes are ready to be reviewed, create a
  [merge request on GitLab](https://docs.gitlab.com/ce/user/project/merge_requests/).
* If changes are not ready and some (strongly encouraged) early feedback is
  required, the merge request should be
  [marked as Work In Progress (WIP)](https://docs.gitlab.com/ce/user/project/merge_requests/work_in_progress_merge_requests.html).


## Review

* Getting changes reviewed is extremely important because it is a great way to
  expand knowledge for both the submitter and the reviewer; some
  [good practices for code review](https://mtlynch.io/code-review-love/) help
  the process to go as smoothly as possible:
  1.  Review your own code first
  2.  Write a clear change list description
  3.  Automate the easy stuff
  4.  Answer questions with the code itself
  5.  Narrowly scope changes
  6.  Separate functional and non-functional changes
  7.  Break up large change lists
  8.  Respond graciously to critiques
  9.  Be patient when your reviewer is wrong
  10. Communicate your responses explicitly
  11. Artfully solicit missing information
  12. Award all ties to your reviewer
  13. Minimize lag between rounds of review
* Maintainers usually monitor pending merge requests, but it can be helpful
  to also mention them on the [`devel@` mailing list](https://lists.apertis.org/)
  when submitting or updating a merge request.
* The repository
  [maintainer]({{< ref "contributions.md#the-role-of-maintainers" >}}) or
  another developer will review the merge request.
* Reviews should happen on GitLab.
* Reviewers can propose concrete code snippets that the submitter can decide to
  integrate in their patch series or not:
  * Reviewers can use code blocks in comments, and the submitter can copy and
    paste them in their patches when updating the merge request
  * Reviewers can edit the submitted branch with the GitLab Web IDE or checking
    it out manually, stacking new commits on top of the submitted patches: it's
    up to the submitter to cherry-pick or reject the suggested changes and
    squash them appropriately with the original commits before the request can
    be merged
  * If submitters do not agree with the suggested snippets they need to explain
    why they are rejecting the proposed changes
* If changes are needed, a force-push with the changes is required to the
  submitted branch to update the commits that are part of the merge request
  (always remember to
  [use `--force-with-lease`](https://developer.atlassian.com/blog/2015/04/force-with-lease/)
  instead of `--force`).
* Questions, request to clarify and any other kind of discussion should be
  handled via comments on the merge request.
* Repeat these review steps until all the changes are accepted.


## Merge

* Merge request must have no thumbs down to be merged and all the
  [discussions should be resolved](https://docs.gitlab.com/ce/user/project/merge_requests/#resolve-discussion-comments-in-merge-requests-reviews).
* Reviewers that approve the general approach but still want to discuss some
  details can add a thumb up, so other reviewers know that once all the
  discussions are resolved the merge request can be approved
* Once all comments have been handled, the reviewer hits the merge button or
  otherwise pushes the submitted commits to the appropriate branch.


## Extra Actions

* If other actions need to be manually taken when commits are landed
  the developer accepting and merging the changes is responsible for ensuring
  that those actions are taken, either by doing it themselves or by asking
  someone else to do that.
* If the merged commits need to be backported to other branches through
  cherry-pick or merges, those should go through merge requests as well:
  however, only changes related to the backport process are allowed, unrelated
  changes should be applied with a separate merge request, possibly landing in
  master first.


## Release Commits

The process for releasing a package is documented in the
[packaging guide]( {{< ref "apertis_packaging_guide.md" >}} ).

