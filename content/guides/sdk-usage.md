+++
short-description = "Collection of guides for using the SDK"
license = "CC-BY-SAv4.0"
title = "SDK Basic Usage"
weight = 100
aliases = [
 "/old-developer/latest/sdk-usage.html", "/old-developer/v2019/sdk-usage.html", "/old-developer/v2020/sdk-usage.html", "/old-developer/v2021pre/sdk-usage.html", "/old-developer/v2022dev0/sdk-usage.html",
 "/old-developer/latest/vm-management.html", "/old-developer/v2019/vm-management.html", "/old-developer/v2020/vm-management.html", "/old-developer/v2021pre/vm-management.html", "/old-developer/v2022dev0/vm-management.html",
]
outputs = [ "html", "pdf-in",]
date = "2016-04-20"
+++

# Overview of SDK features
Take a moment to familiarize yourself with some of the more commonly used tools in the SDK.  These include the file browser, Eclipse, DevHelp and the simulator.  All of these tools have shortcut links placed on the desktop.

## File browser
![](/images/overview-home.png)
This icon opens the file manager so you can access the local folders and use the shared folder for exchanging documents between your host system and the virtual machine. You should be able to find your shared folder under **Places** on the left hand side of the file manager window.

## Eclipse IDE
![](/images/overview-eclipse.png)
Eclipse is the IDE used for application development in the SDK. Eclipse includes a custom Apertis plugin for updating the sysroot from within Eclipse.

## Apertis API documentation
![](/images/overview-docs.png)
DevHelp is the standard browser for offline API documentation across many Open Source projects. The DevHelp installation in the SDK provides a local, offline version of the all the documentation found in the Application Developer Portal, including the Apertis API references.

# Shutting down / restarting the virtual machine

If you want to shut the virtual machine down, you can use the icon in the top
right corner of the screen. If you want to restart it, go to **Applications
Menu** and click **Log Out**:

![](/images/fig-11.png)

In the dialog box, you can choose if you want to log out, restart the virtual
machine, or shut it down.

![](/images/fig-12.png)

# Audio settings

In order to make sure that the audio player output is heard from the simulator
make sure that the below settings are done:

- Unmute Audio in host machine.

- Go to `Applications menu ▸ Multimedia ▸ Pulse Audio Volume Control`

- Select **Output Devices** tab.

- Make sure that the Audio speaker is **Unmuted**.

With these settings, Audio player output should be heard over speakers.

# Change the keyboard layout

You might want to modify your keyboard layout for the Apertis operating system.
The default keyboard layout is **US**. To change it, please follow these steps:

- In the SDK, go to `Applications` Menu ▸ `Settings` and choose `Keyboard`.

- In the dialog box, select the tab `Layout`, uncheck `Use system defaults`
  and click `Edit` under Keyboard layout. Select your required keyboard
  layout from the list, click `OK` and close the window. You do not need to
  select a Keyboard model.

    ![](/images/fig-10.png)
