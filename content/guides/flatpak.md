+++
date = "2021-03-23"
weight = 100

title = "Application verification using Flatpak"
+++

Starting with Apertis v2022dev2, [Flatpak](https://flatpak.org) includes the
ability to distribute [application bundles]( {{< ref "glossary.md#application-bundle" >}} )
verified with [ed25519](https://ed25519.cr.yp.to/) signatures.

{{% notice warning %}}
The current implementation is a technology preview and it is expected to stabilize
during the release cycles leading to the v2022 stable release. The prototype is
already available in Apertis as documented here, but it may be subject to potentially
incompatible changes during the [upstream review process](https://github.com/flatpak/flatpak/pull/4170).
{{% /notice %}}

This signature system relies on OSTree's library functions. Therefore, the key
generation and storage process is identical to what is described in the
[System updates and rollback]( {{< ref "system-updates-and-rollback.md#verified-updates" >}} )
design document.

Flatpak application signatures occur on several levels:
  * single commits
  * whole repositories
  * single-file bundles

Please note, however, that GPG signatures are disabled on Apertis. It is still
possible to pull from GPG-signed repositories, but those signatures won't be
verified. Similarly, it is not possible to sign flatpak applications using GPG.

# Creating signed flatpak applications

The simplest way to create a signed flatpak is to use `flatpak-builder` with
the `--sign=<SECRETKEY>` command-line argument, where `<SECRETKEY>` is the
base64-encoded secret Ed25519 key. This ensures the OSTree commit and summary
are properly signed.

For more advanced usage, the same command-line option can also be used with the
following flatpak commands:
  * `flatpak build-bundle`
  * `flatpak build-commit-from`
  * `flatpak build-export`
  * `flatpak build-import-bundle`
  * `flatpak build-sign`
  * `flatpak build-update-repo`

These commands allow one to create Ed25519-signed commits from an unsigned
repository or bundle, or to create signed bundles.

Multiple occurrences of the `--sign` option are allowed in to order to permit
multiple signatures of each object.

More details about those commands are available in the
[Flatpak documentation](https://docs.flatpak.org/en/latest/building.html).

# Publishing signed flatpaks applications

## Publishing a single application

One convenient way to distribute single flatpak applications is to use
[.flatpakref](https://docs.flatpak.org/en/latest/repositories.html#flatpakref-files)
files. Those files include all necessary information for flatpak to be able to
install and update the application.

The only difference here is that the `GPGKey=...` line must be replaced with
`SignatureKey=<PUBLICKEY>`, where `<PUBLICKEY>` is the base64-encoded public
Ed25519 key.

This line will instruct flatpak to add the corresponding configuration keys to
the remote and perform signature verification when installing and/or updating
this application.

## Publishing a repository

When distributing several applications, it can be useful to publish the whole
repository using a [.flatpakrepo](https://docs.flatpak.org/en/latest/hosting-a-repository.html#flatpakrepo-files)
file.

Exactly as it is done with with `.flatpakref` files, using
`SignatureKey=<PUBLICKEY>` instead of `GPGKey=...` will instruct flatpak to
enable Ed25519 signature verification for this repository.

## Publishing a bundle

Flatpak applications can also be distributed as
[single-file bundles](https://docs.flatpak.org/en/latest/single-file-bundles.html),
which can be created using the `flatpak build-bundle` command. As previously
mentioned, these bundles can be signed by adding the `--sign=<SECRETKEY>` option
to the command invocation.

However, when publishing a signed flatpak bundle, the corresponding public key
has to be stored in a location easily accessible to the final user for signature
verification, as the bundle file itself is signed and doesn't provide any mean
to retrieve the associated public key.

# Installing a signed flatpak

## Configuring a remote repository

If the repository publisher provides a `.flatpakrepo` file including the public key,
then no action is needed other than running `flatpak remote-add <REPOFILE>`.

However, if such a file is not available, one must add the `--sign-verify`
command-line option to the `flatpak remote-add` command in order to provide
either the public key directly, or a file containing the public key:
  * `--sign-verify=ed25519=inline:<PUBLICKEY>` is used to directly specify the
    public key needed to verify this repository
  * `--sign-verify=ed25519=file:<PATH>` can be used to point flatpak to a file
    containing a list of public keys (base64-encoded, one key per line), among
    which at least one can be used to verify signatures for this repository

Multiple `--sign-verify` occurrences are allowed in order to specify as many
public keys as needed. This can be useful when a new signature key is being
deployed, while the old one is still in use: by specifying both the old and the
new key, users can make sure at least one of those will be able to verify the
signatures. That way, once the old key is revoked and only the new one is used
for signing the repository, the corresponding remote will keep working as
expected.

This option can also be added when using the `flatpak remote-modify` command.

## Installing a signed application

Similarly to the process of using `.flatpakrepo` files, when installing a single
application using a `.flatpakref` file including the public key, no additional
action is needed. Flatpak will automatically verify Ed25519 signatures using the
provided public key.

When the application is installed from a previously configured repository,
signature verification is also automated, as long as the corresponding public
key has been imported into the remote's configuration.

## Installing a signed bundle

Flatpak bundles are not installed from a repository like most flatpak
applications, but from a single, optionally signed, file. As there is no
repository configuration to import public keys from, the user needs to specify
the relevant public keys using the `--sign-verify` command-line option as stated
above.

This option works the same way with both `flatpak build-import-bundle` and
 `flatpak install` commands.

# References

Flatpak reference documentation: <https://docs.flatpak.org/>
