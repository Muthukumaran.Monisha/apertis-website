+++
date = "2019-10-24"
weight = 100

title = "GitLab-based Packaging Workflow"

aliases = [
    "/old-wiki/Guidelines/Git-based_packaging_workflow",
    "/old-wiki/Guidelines/Gitlab-based_packaging_workflow"
]
+++

Packaging in Apertis is handled through a set of git repositories hosted on
GitLab containing the sources for every packages. GitLab CI pipelines take care
of propagating updates to OBS to build and publish the resulting binaries.

# Repository contents

The packaging git repositories follow the
[DEP-14](http://dep.debian.net/deps/dep14/) Git layout specification with the
following conventions:

![DEP-14 in Apertis](/images/apertis-dep-14-gitlab-curves.svg)

- `upstream/${UPSTREAM_DISTRIBUTION}` branches with the unpacked upstream
  project code from the debian package (e.g. `upstream/buster`)
- `debian/${UPSTREAM_DISTRIBUTION}` branches with the Debian changes on top of
  the upstream project code (e.g. `debian/buster`)
- `debian/${UPSTREAM_VERSION}` tags (e.g. `debian/2.48.0-1`)
- `apertis/${APERTIS_RELEASE}` branches where the debian branches above get
  merged with the Apertis packaging (e.g.  `apertis/v2019pre`)
- `apertis/${APERTIS_VERSION}` tags (e.g. `apertis/2.48.0-1co1`)
- `pristine-lfs` branch containing the information to fetch the original
  tarballs stored on Git-LFS via
  [pristine-lfs](https://gitlab.apertis.org/infrastructure/pristine-lfs)

# Development environment

All the instructions below assume an Apertis development enviroment: either
boot the
[Apertis SDK]( {{< ref "software-development-kit.md" >}} )
or enter the `apertis-*-package-source-builder` Docker container:

    APERTIS_RELEASE=v2019pre
    docker pull docker-registry.apertis.org/apertis/apertis-${APERTIS_RELEASE}-package-source-builder
    docker run -it --rm --env HOME -w "$(pwd)" -v "$HOME:$HOME" -v "$HOME:/root" --security-opt label=disable docker-registry.apertis.org/apertis/apertis-${APERTIS_RELEASE}-package-source-builder bash

# How to manually sync an Apertis package with a new version

Upstream updates are usually handled automatically by the
[`ci-package-builder.yml`](https://gitlab.apertis.org/infrastructure/ci-package-builder/)
Continous Integration pipeline, which
[fetches upstream packages, merges them]({{< ref "apertis_packaging_guide.md#pulling-updates-or-security-fixes-from-upstream-distributions" >}})
with the Apertis contents and directly creates Merge Requests to be reviewed by
[maintainers]({{< ref "contributions.md#the-role-of-maintainers" >}}).

However, in some cases it is necessary to manually pull upstream contents:
1. the CI failed to merge the upstream update with the downstream changes, so a
   developer must reproduce the merge and fix the conflicts
1. for some reason it in necessary to manually pull updates from a new upstream
   distribution/suite

The steps below can guide you on manually pulling upstream updates:

{{% notice note %}}
This guide assumes the developer is a member of the Apertis development team,
contributors who are not a member of this team will need to fork the package to
their private workspace (as documented in the
[development process]({{< ref "development_process.md" >}}) before manually
updating the package and submit a merge request from there.
{{% /notice %}}

1. On GitLab,
   [fork the project](https://docs.gitlab.com/ce/user/project/repository/forking_workflow.html)
   to your personal namespace.

1. Check out the source repository

       GITLAB_USER=${USER}
       UPSTREAM_DISTRIBUTION=buster
       MIRROR=https://deb.debian.org/debian
       PACKAGE=glib2.0
       
       git clone git@gitlab.apertis.org:${GITLAB_USER}/${PACKAGE}
       cd ${PACKAGE}

1. Only if moving to a new upstream distribution, prepare the upstream base
   branches using the current upstream branches as a base:

       UPSTREAM_DISTRIBUTION=bullseye
       UPSTREAM_BASE=buster
       git push origin origin/upstream/${UPSTREAM_BASE}:refs/heads/upstream/${UPSTREAM_DISTRIBUTION}
       git push origin origin/debian/${UPSTREAM_BASE}:refs/heads/debian/${UPSTREAM_DISTRIBUTION}

1. Sync the upstream contents

       apertis-pkg-pull-updates --package=$PACKAGE --upstream=$UPSTREAM_DISTRIBUTION --mirror=$MIRROR
       git push origin --follow-tags pristine-lfs upstream/${UPSTREAM_DISTRIBUTION} debian/${UPSTREAM_DISTRIBUTION}

1. Merge the upstream changes

       PROPOSED_BRANCH=wip/$GITLAB_USER/update-from-${UPSTREAM_DISTRIBUTION}
       TARGET_BRANCH=apertis/v2021dev1
       git checkout -b ${PROPOSED_BRANCH} ${TARGET_BRANCH}
       apertis-pkg-merge-updates --upstream=debian/${UPSTREAM_DISTRIBUTION} --downstream=${PROPOSED_BRANCH}

1. Only if the merge fails, fix the conflicts and continue

       git status
       # fix conflicts
       git merge --continue
       dch --local co --no-auto-nmu

1. Check `debian/changelog` and update it if needed by listing all the remaining Apertis changes

       $EDITOR debian/changelog
       git commit --signoff --amend debian/changelog

1. Push your changes for review

       git push origin --follow-tags pristine-lfs upstream/${UPSTREAM_DISTRIBUTION} debian/${UPSTREAM_DISTRIBUTION}
       git push -o merge_request.create

# How to work with downstream packaging changes

The packaging repositories follow the
[Apertis contribution process]( {{< ref "contributions.md" >}} ).

However, the deb packaging format does not allow changes outside of the
`debian/` folder: changes to the upstream files should be carried as patches,
shipped in `debian/patches` folder.

To make working with those patches a bit easier for those used to `git rebase`,
[the `gbp pq` tool provides a way to convert patches to git branches and
back](https://honk.sigxcpu.org/projects/git-buildpackage/manual-html/gbp.patches.html).

# How to issue a release

The process for landing downstream changes is documented in the
[ci-package-builder documentation]({{< ref "apertis_packaging_guide.md#landing-downstream-changes-to-the-main-archive" >}}).

# How to add a new packaging repository

The process for adding new packages from Debian is documented in the
[ci-package-builder documentation]({{< ref "apertis_packaging_guide.md#adding-new-packages-from-debian" >}}).

