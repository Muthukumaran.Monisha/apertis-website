+++
date = "2015-11-19"
weight = 100

title = "Boot Process"

aliases = [
    "/old-wiki/Docs/System_startup"
]
+++

Apertis has a fairly typical Linux boot process:

- A platform specific bootloader performs initial low-level configuration
  of the system
- The Linux kernel performs higher level configuration and
  creates a standardised environment
- The kernel passes execution to the user space which enables services
  based on the functionality provided by the kernel and sets up the system
  to accept user input

# Critical sequences during startup

The phases during startup generally proceed from most- to
least-critical. For instance, an early deadlock in the kernel would mean
the system would never complete its boot process and thus would be
non-responsive to user input. On the other hand, if the networking system
(near the end of the boot process) fails to start, specific system
functionality (for example, in the case of a car infotainment unit,
driving directions) may be unavailable, but the user may still be able to
use other functionality (such as play music and other local media).

Any failure during boot should not result in a worsened state. Failures
such as filesystem corruption should be automatically repaired upon
subsequent boot. Due to Apertis' focus on making the core system storage
read-only for the majority of the system's uptime, there should be few
opportunities for defects to adversely affect the boot process. More
serious issues resulting from updates will result in the system returning
to the pre-update configuration. For more details, please refer to the
[System Updates and Rollback Proposal]( {{< ref "system-updates-and-rollback.md" >}} ).

# Systemd and its role in startup

[Systemd](http://www.freedesktop.org/wiki/Software/systemd/) starts
system services on-demand, serving as a replacement for the traditional
SysV init system. In this duty, systemd is the first process started by
the kernel. It starts all processes required to bring the system up to
the point where it is ready for user input.

Unlike the traditional init system, systemd can also handle automated
activation of services, after the system has settled, based upon
requirements of applications run due to user interaction. Further,
systemd can stop services when they are no longer needed.

As an implementation detail, systemd starts services in a
highly-parallel fashion, which allows them to make the most use of the
processor while they wait on their dependencies. This reduces total
start-up time by preventing services blocking input/output delays from
blocking their dependent services (which would otherwise need to wait
until the first service is done starting).

More information about systemd can be found on its [manual
pages](http://www.freedesktop.org/software/systemd/man/).

For guidance on improving systemd boot time, see
[Boot Optimisation and Profiling]( {{< ref "guides/boot_optimisation.md" >}} ).

# Boot splash screen

Apertis provides functionality to show a boot animation using
[Plymouth](http://www.freedesktop.org/wiki/Software/Plymouth/). Plymouth
can be themed to show different boot animations, see
[video animation on boot]( {{< ref "/guides/video-animation-on-boot.md" >}} )
for information on how to modify the boot animation theme.
