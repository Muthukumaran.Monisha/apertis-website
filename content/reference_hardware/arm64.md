+++
date = "2020-06-10"
weight = 100

title = "ARM 64-bit Reference Hardware"
+++

Starting with the 17.03 release, ARM64 images have been made available. While
the images in principle support various 64 bit ARM boards, Renesas Generation 3
SoCs are the main target.

The recommend reference boards for these images are the R-Car Starter Kit
boards, however the Salvator-x boards are also supported. For more information
about Renesas R-Car starter kit please see the
[Renesas product page](https://www.renesas.com/en-us/solutions/automotive/adas/solution-kits/r-car-starter-kit.html).
The following [optional extras]( {{< ref "/reference_hardware/extras.md" >}} )
may be of interest.

If you currently don't have access to any of the below supported hardware, the
`amd64` images can be run on a
[virtual machine]( {{< ref "/guides/virtualbox.md" >}} ).

| Reference                          | Hardware                                                                                        | Comments                                                                   |
| ---------------------------------- | ----------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------- |
| ARM64 reference device             | [Renesas R-Car M3 Starter Kit Pro (M3SK/m3ulcb)](http://elinux.org/R-Car/Boards/M3SK)           | [Setup guide]( {{< ref "/reference_hardware/rcar-gen3_setup.md" >}} )              |
| Alternative ARM64 reference device | [Renesas R-Car H3 Starter Kit Premier (H3SK/h3ulcb)](http://elinux.org/R-Car/Boards/H3SK)       | [Setup guide]( {{< ref "/reference_hardware/rcar-gen3_setup.md" >}} ) **Untested** |
|                                    |                                                                                                 |                                                                            |
| Alternative ARM64 reference device | [Renesas R-Car H3 Salvator-X (r8a7795-salvator-x)](http://elinux.org/R-Car/Boards/Salvator-X)   | [Setup guide]( {{< ref "/reference_hardware/rcar-gen3_setup.md" >}} )              |
| Alternative ARM64 reference device | [Renesas R-Car M3-W Salvator-X (r8a7796-salvator-x)](http://elinux.org/R-Car/Boards/Salvator-X) | [Setup guide]( {{< ref "/reference_hardware/rcar-gen3_setup.md" >}} ) **Untested** |
|                                    |                                                                                                 |                                                                            |

