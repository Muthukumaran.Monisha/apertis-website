+++
date = "2020-06-10"
weight = 100

title = "ARM 32-bit Reference Hardware"
+++

The recommended ARM 32-bit hardware is the i.MX6 Sabrelite.
Please see its
[setup guide]( {{< ref "/reference_hardware/imx6q_sabrelite_setup.md" >}} )
for first-time setup.
The following [optional extras]( {{< ref "/reference_hardware/extras.md" >}} )
may be of interest.

If you currently don't have access to any supported hardware, the `amd64`
images can be run on a
[virtual machine]( {{< ref "/guides/virtualbox.md" >}} ).

| Reference                                   | Hardware                                                                                                                                                                                         | Comments                                                                                                                      |
| ------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | ----------------------------------------------------------------------------------------------------------------------------- |
| ARM reference device                        | i.MX6 Sabrelite                                                                                                                                                                                  | [Setup guide]( {{< ref "/reference_hardware/imx6q_sabrelite_setup.md" >}} )                                                           |
| Power source                                | [The 5V+ power source unit](http://www.digikey.com/product-search/en?WT.z_header=search_go&lang=en&site=us&keywords=993-1019-ND&x=14&y=17) that should come with the i.MX6 Sabrelite             |                                                                                                                               |
| SD card                                     | Full-sized SD card (or microSD in full-size adapter), 16GB or larger                                                                                                                             | Speed class C10/U1 or better recommended                                                                                      |
| Recommended: 100 Mbit network switch or hub | Use a network connection that *does not* support gigabit                                                                                                                                         | Workaround for [reliability issues with this particular hardware when using gigabit](https://community.nxp.com/thread/381119) |
| Recommended: LVDS panel touchscreen         | [HannStar HSD100PXN1-A00-C11](https://www.mouser.com/ProductDetail/Boundary-Devices/NIT6X_101HANNSTAR/?qs=%2Fha2pyFaduhICqiA62NC348xLf7lkR37nWVKEUqzpogToe6HSSy1X%2FAMJ8uCmShD)                   |                                                                                                                               |
| Recommended: LVDS panel cable               | [Cable for LVDS panel](http://boundarydevices.com/products/lvds-cable-for-freescale-10-1-hannstar/)                                                                                              |                                                                                                                               |
| Recommended: USB to serial adapter          | [USB\<-\>serial adapter cable](http://www.amazon.com/TRENDnet-RS-232-Serial-Converter-TU-S9/dp/B0007T27H8/ref=sr_1_1?s=electronics&ie=UTF8&qid=1396528293&sr=1-1&keywords=usb+to+serial+adapter) | To look at the console                                                                                                        |
| Recommended: USB keyboard                   | USB keyboard                                                                                                                                                                                     |                                                                                                                               |
| Optional: Attached camera                   | [OV5640-based 5MP MIPI attached camera module](http://boundarydevices.com/products/nit6x_5mp_mipi/)                                                                                              |                                                                                                                               |
|                                             |                                                                                                                                                                                                  |                                                                                                                               |
