+++
date = "2015-11-09"
weight = 100

title = "App Store Approval"

outputs = ["html", "pdf-in"]

aliases = [
    "/old-wiki/App_Store_Approval"
]
+++

This page will eventually collect all the guidelines we recommend for
app-store curators.

The general principle should be that anything that crosses a trust
boundary should be checked.

For now, here are the design documents and wiki pages that are relevant
to this topic. This list is not exhaustive:

  - [Applications design document]( {{< ref "applications.md" >}} )
  - [Multi-user design document]( {{< ref "multiuser.md" >}} )
  - [Security design document]( {{< ref "security.md" >}} )
  - [Sensors and Actuators design document]( {{< ref "sensors-and-actuators.md" >}} )
  - [Geolocation and Navigation design document]( {{< ref "geolocation-and-navigation.md" >}} )
  - [Application Layout]( {{< ref "application-layout.md" >}} ) (the layout,
    bus names, etc. are constrained, and various system integration
    points should be checked)
  - [Content hand-over]( {{< ref "/concepts/content_hand-over.md" >}} ) (documents cross
    the boundary between bundles)
  - [Interface discovery]( {{< ref "/concepts/interface_discovery.md" >}} ) (the
    interfaces that can be advertised are constrained)
