+++
date = "2015-12-18"
weight = 100

title = "Attack detection"

aliases = [
    "/old-wiki/Attack_detection"
]
+++

{{% notice warning %}}
This concept is an old work in progress.
{{% /notice %}}

The platform should have a heuristic for detecting whether an app-bundle
has been compromised or is malicious. This design document, which has
not yet been written, will collect the various possible inputs to this
heuristic, and the various actions that might be taken as a result of
the heuristic deciding that an app-bundle's behaviour is potentially or
probably malicious.

  - [Egress filtering]( {{< ref "/egress_filtering.md" >}} ) is a potential
    input: if an application attempts to carry out Internet connections
    that are not allowed, this is suspicious behaviour.
  - [Egress filtering]( {{< ref "/egress_filtering.md" >}} ) is a potential
    action: if an application is detected to be malicious, its Internet
    connectivity could be limited or cut off.
