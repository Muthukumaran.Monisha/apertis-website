+++
date = "2015-12-03"
weight = 100

title = "Packages"

aliases = [
    "/old-wiki/Packages"
]
+++

{{% notice warning %}}
This concept is an old work in progress.
{{% /notice %}}

This page lists some packages found in Apertis images, not all of which
are present in typical Linux distributions. Standard Linux components
such as D-Bus and systemd are also listed here if they are used
extensively by Apertis.

Many Apertis packages are named after sites in the UK where significant
Roman archaeological hoards were found.

Each of these packages is classified according to these properties:

  - is it a system service (its scope is the whole system) or a user
    service (its scope is one specific user)?
  - is it part of the platform (like dbus-daemon), a built-in
    application bundle, or a store application bundle?
  - if it is part of the platform, is it available to store application
    bundles, or only to the platform and built-in applications?

See [Glossary]( {{< ref "/glossary.md" >}} ) for details of those terms.

## Platform libraries used by applications

### [libclapton](https://git.apertis.org/cgit/libclapton.git): system information and logging library

Clapton is a platform library, used by *FIXME: applications? the
platform?*.

### [libgrassmoor](https://git.apertis.org/cgit/libgrassmoor.git): media information and playback library

Grassmoor is a platform library, used by *FIXME: applications? the
platform?*.

### [liblightwood](https://git.apertis.org/cgit/liblightwood.git): widget library

Lightwood is a platform library, used by graphical applications.
*(FIXME: and the platform?)*

### [Mildenhall](https://git.apertis.org/cgit/mildenhall.git): user interface widget library

Mildenhall is a platform library providing the reference UI widgets,
used by graphical applications. *(FIXME: and the platform?)*

### [libseaton](https://git.apertis.org/cgit/libseaton.git): persistent data management library

Seaton is a platform library, used by applications. *(FIXME: and the
platform?)*

### [libthornbury](https://git.apertis.org/cgit/libthornbury.git): UI utility library

Thornbury is a platform library, used by graphical applications.
*(FIXME: and the platform?)*

## Platform libraries used internally

### chaiwala-bootflags: bootup flag manipulation library

chaiwala-bootflags manipulates the flags that determine how the system
boots up. It is used by chaiwala-btrfs and possibly other components.
Chaiwala was an old development codename for what has now become
Apertis. The name is kept for historical reasons.

## Platform system services used by applications

### dbus-daemon --system: D-Bus inter-process communication service

dbus-daemon is a platform component, used as both a user service and a
system service. The system bus, "dbus-daemon --system", connects
applications and platform components to system services, and mediates
their access to those system services. All components that are able to
connect to the system bus may listen for signals (normally broadcasts)
from authorized system services. All other accesses, including sending
signals, are prevented unless they are specifically allowed by
configuration.

## Platform system services used internally

### chaiwala-btrfs: upgrade/rollback service

chaiwala-btrfs manages btrfs subvolumes and snapshots for the platform,
such as platform upgrades, roll-back after a faulty upgrade, factory
reset and so on. It has some similarities with Ribchester, which does
similar things for application bundles.

Chaiwala was an old development codename for what has now become
Apertis. The name is kept for historical reasons.

### [Ribchester](https://git.apertis.org/cgit/.git): application mounter

Ribchester is a platform component containing a system service and a
library. It mounts btrfs subvolumes for application bundle management
(such as installation and rollback), and provides backup and related
services. It has some similarities with chaiwala-btrfs, which does
similar things for the platform.

### [Shapwick](https://git.apertis.org/cgit/shapwick.git): resource monitoring service

Shapwick is a platform component containing a system service and a
library for communication with the service. It monitors resource use.

### systemd: system startup and service management service

systemd is a service management service, used as both a system service
and a user service. The system instance is process 1 (the first process
launched by the kernel after leaving the early-boot environment), and is
responsible for starting and monitoring all system services and all user
sessions.

## Platform user-services used by applications

### [Barkway](https://git.apertis.org/cgit/barkway.git): network interface management service

Barkway is a platform component, consisting of a user service and
libraries that interface with it.

### [Canterbury](https://git.apertis.org/cgit/canterbury.git): application management and process control service

Canterbury is a platform component, consisting of a user service and
libraries that interface with it.

### [Chalgrove](https://git.apertis.org/cgit/chalgrove.git): preferences management service

Chalgrove is a platform component, consisting of a user service and
libraries that interface with it.

### [Corbridge](https://git.apertis.org/cgit/corbridge.git): Bluetooth management service

Corbridge is a platform component, consisting of a user service and
libraries that interface with it.

### dbus-daemon --session: D-Bus inter-process communication service

dbus-daemon is a platform component, used as both a user service and a
system service. The session bus, "dbus-daemon --session", is a user
service which connects applications and user services together and
mediates their communication.

### [Didcot](https://git.apertis.org/cgit/didcot.git): data sharing and file opening service

Didcot is a platform component, consisting of a user service and
libraries that interface with it. It mediates file opening by media type
(content type, MIME type), URL opening by URL scheme, and other data
sharing between user applications.

### [Newport](https://git.apertis.org/cgit/newport.git): download manager

Newport is a platform component containing a user service, and a library
for communication with the service. It manages large downloads for
applications and other services.

### [Prestwood](https://git.apertis.org/cgit/prestwood.git): disk mounting service

Prestwood is a platform component containing a user service and a
library for communication with the service. It mounts removable media.

### [Tinwell](https://git.apertis.org/cgit/tinwell.git): media playback service

Tinwell is a platform component containing a user service and a library
for communication with the service. It plays media.

## Platform user-services used internally

### systemd --user: session startup and service management service

systemd is a service management service, used as both a system service
and a user service. The per-user instance, systemd --user, manages user
services.

## Built-in application bundles

### [Eye](https://git.apertis.org/cgit/eye.git): video player

Eye is a built-in application bundle containing a graphical program.

### [Frampton](https://git.apertis.org/cgit/frampton.git): audio player

Frampton is a built-in application bundle containing a graphical program
and an agent.

### [Mildenhall launcher](https://git.apertis.org/cgit/mildenhall-launcher.git): application-launcher application

The Mildenhall launcher is a built-in application bundle containing a
graphical program. It lists applications that are available to be
launched.

### [Mildenhall popup layer](https://git.apertis.org/cgit/mildenhall-popup-layer.git): popup display application

The Mildenhall popup layer is a built-in application bundle containing a
graphical program. It displays popup notifications.

### [Mildenhall settings](https://git.apertis.org/cgit/mildenhall-settings.git): system and application configuration application

The Mildenhall settings application is a built-in application bundle
containing a graphical program. It provides access to system settings.

### [Mildenhall status bar](https://git.apertis.org/cgit/mildenhall-statusbar.git): status bar application

The Mildenhall status bar is a built-in application bundle containing a
graphical program. It provides the status bar for the reference user
interface.

### [Rhayader](https://git.apertis.org/cgit/rhayader.git): web browser application

Rhayader is a built-in application bundle containing a Webkit-based web
browser.
